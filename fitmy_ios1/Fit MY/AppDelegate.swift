//
//  AppDelegate.swift
//  Fit MY
//
//  Created by Azlan Shah on 24/06/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import Localize_Swift
//import LanguageManager_iOS

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    //let currentLanguage = "ms"
    let currentLanguage = "en"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        
//        let storyboard = UIStoryboard(name: "LoginScreen", bundle: nil)
//
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
//
//        self.window?.rootViewController = initialViewController
//        self.window?.makeKeyAndVisible()
        
//        if let language = UserDefaults.standard.value(forKey: "AppLanguage") as? String, !language.isEmpty, language.count > 0 {
//            Localize.setCurrentLanguage(language)
//        } else {
//            UserDefaults.standard.set(currentLanguage, forKey: "AppLanguage")
//            Localize.setCurrentLanguage(currentLanguage)
//        }
        
//        UIApplication.shared.statusBarStyle = .lightContent
        Localize.setCurrentLanguage("en")
        
        FirebaseApp.configure()
        let udid = UIDevice.current.identifierForVendor?.uuidString ?? "simulator"
        print("UDID: ", udid)
        let pushManager = PushNotificationManager(userID: udid)
        pushManager.registerForPushNotifications()
        
        IQKeyboardManager.shared.enable = true
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifySenpai), name: Notification.Name("notifySenpai"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notifySenpai2), name: Notification.Name("notifySenpai2"), object: nil)
    }
    
    @objc func notifySenpai() {
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Exercise in Progress!", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "New location receive", arguments: nil)
        content.sound = .default
        content.badge = 1
        
        // Fire in 1 second
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        // Create the request object.
        let request = UNNotificationRequest(identifier: "location_background_update", content: content, trigger: trigger)
        
        // Schedule the request.
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }

    
    @objc func notifySenpai2() {
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: "Exercise in Progress!", arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: "New location added to local DB", arguments: nil)
        content.sound = .default
        content.badge = 1
        
        // Fire in 1 second
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        
        // Create the request object.
        let request = UNNotificationRequest(identifier: "db_background_update", content: content, trigger: trigger)
        
        // Schedule the request.
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error : Error?) in
            if let theError = error {
                print(theError.localizedDescription)
            }
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notifySenpai"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("notifySenpai2"), object: nil)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        application.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        if let data = UserDefaults.standard.object(forKey: "current_activity") as? Data {
            if let exercise = try? JSONDecoder().decode([CurrentExercise].self, from: data), exercise.count > 0 {
                let content = UNMutableNotificationContent()
                content.title = NSString.localizedUserNotificationString(forKey: "Exercise in Progress!", arguments: nil)
                content.body = NSString.localizedUserNotificationString(forKey: "FitMY is terminated while exercise is in progress, please relaunch to continue tracking.", arguments: nil)
                content.sound = .default
                content.badge = 1
                
                // Fire in 1 second
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
                
                // Create the request object.
                let request = UNNotificationRequest(identifier: "exercise_indoor_ongoing", content: content, trigger: trigger)
                
                // Schedule the request.
                let center = UNUserNotificationCenter.current()
                center.add(request) { (error : Error?) in
                    if let theError = error {
                        print(theError.localizedDescription)
                    }
                }
            } else {
                UserDefaults.standard.set("stop", forKey: "current_status")
            }
        }
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Fit_MY")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}



//extension Bundle {
//    static func swizzleLocalization() {
//        let orginalSelector = #selector(localizedString(forKey:value:table:))
//        guard let orginalMethod = class_getInstanceMethod(self, orginalSelector) else { return }
//
//        DispatchQueue.main.async {
//            let mySelector = #selector(myLocaLizedString(forKey:value:table:))
//            guard let myMethod = class_getInstanceMethod(self, mySelector) else { return }
//            if class_addMethod(self, orginalSelector, method_getImplementation(myMethod), method_getTypeEncoding(myMethod)) {
//                class_replaceMethod(self, mySelector, method_getImplementation(orginalMethod), method_getTypeEncoding(orginalMethod))
//            } else {
//                method_exchangeImplementations(orginalMethod, myMethod)
//            }
//        }
//    }
//
//    @objc private func myLocaLizedString(forKey key: String,value: String?, table: String?) -> String {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
//            let bundlePath = Bundle.main.path(forResource: appDelegate.currentLanguage, ofType: "lproj"),
//            let bundle = Bundle(path: bundlePath) else {
//                return Bundle.main.myLocaLizedString(forKey: key, value: value, table: table)
//        }
//        return bundle.myLocaLizedString(forKey: key, value: value, table: table)
//    }
//}

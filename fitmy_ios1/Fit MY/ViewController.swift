//
//  ViewController.swift
//  Fit MY
//
//  Created by Azlan Shah on 24/06/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import SwiftSVG
import HealthKit

class ViewController: UIViewController {

    var healthStore = HKHealthStore()
    var timer = Timer()
    @IBOutlet weak var view1: UIViewX!
    @IBOutlet weak var steps: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkPermission()
        
        // Do any additional setup after loading the view.
        
        // Example
//        let triangle = UIView(pathString: "M75 0 l75 200 L0 200 Z")
//        view.addSubview(triangle)
        
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getData), userInfo: nil, repeats: true)
        
        let fistBump = UIView(SVGNamed: "menu")     // In the main bundle
        view1.addSubview(fistBump)
    }
    
    


}



extension ViewController {
    func checkPermission() {
        // Access Step Count
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!]
        // Check for Authorization
        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (bool, error) in
            if (bool) {
                // Authorization Successful
                
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getData), userInfo: nil, repeats: true)

                
            } // end if
        } // end of checking authorization
    }
    
    @objc func getData() {
        self.getSteps { (result) in
            DispatchQueue.main.async {
                let stepCount = String(Int(result))
                self.steps.text = String(stepCount)
                print("step updated at", Date())
                let distanceCount = String(Int(result))
                self.steps.text = String(stepCount)
                print("step updated at", Date())
            }
        }
//        timer.invalidate()
        
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getData), userInfo: nil, repeats: false)
    }
    
    func getSteps(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.count())
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func getDistance(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    resultCount = sum.doubleValue(for: HKUnit.count())
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            }
        }
        
        
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
}

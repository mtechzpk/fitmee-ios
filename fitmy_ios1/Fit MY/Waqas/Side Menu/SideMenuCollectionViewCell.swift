//
//  SideMenuCollectionViewCell.swift
//  Fit MY
//
//  Created by Waqas on 05/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class SideMenuCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var MenuImage: UIImageView!
    @IBOutlet var MenuLabel: UILabel!
    
}

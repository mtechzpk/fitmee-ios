//
//  SideMenuViewController.swift
//  Fit MY
//
//  Created by Waqas on 05/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

  @IBOutlet var HomeMenu: UIView!
           @IBOutlet var Activity: UIView!
           @IBOutlet var Booking: UIView!
           @IBOutlet var News: UIView!
           @IBOutlet var Gallery: UIView!
           @IBOutlet var Music: UIView!
           @IBOutlet var Video: UIView!
           @IBOutlet var Reward: UIView!
           @IBOutlet var Event: UIView!
           @IBOutlet var Profile: UIView!
           
    var viewController:UIViewController?
    
           override func viewDidLoad() {
               super.viewDidLoad()
               // Do any additional setup after loading the view.
               SetUpActionOnViews()
           }
           
    override func viewDidAppear(_ animated: Bool) {
        self.view.isUserInteractionEnabled = true
        SetUpActionOnViews()
    }
           
           
    @objc func HomeAction(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "FitnessDiaryController")
        
        self.dismiss(animated: false) {
            if(self.viewController != nil)
            {
                self.viewController!.navigationController?.pushViewController(initialViewController, animated: true)
            }
        }
    }
           @objc func ActivityAction(_ sender: UITapGestureRecognizer? = nil){
               // handling code
                let storyboard = UIStoryboard(name: "LoginScreen", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "ActivityViewController")
            
            self.dismiss(animated: false) {
                if(self.viewController != nil)
                {
                    self.viewController!.navigationController?.pushViewController(initialViewController, animated: true)
                }
            }
           }
           @objc func BookingAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
           }
           @objc func NewsAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
            
//            AlertHelper.showErrorAlert(WithTitle: "", Message: "Coming Soon!", Sender: self)
//            //NewsVC
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                       let initialViewController = storyboard.instantiateViewController(withIdentifier: "NewsVC")

                       self.dismiss(animated: false) {
                           if(self.viewController != nil)
                           {
                               self.viewController!.navigationController?.pushViewController(initialViewController, animated: true)
                           }
                       }
           }
           @objc func GalleryAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
            AlertHelper.showErrorAlert(WithTitle: "", Message: "Coming Soon!", Sender: self)
           }
           @objc func MusicAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
            AlertHelper.showErrorAlert(WithTitle: "", Message: "Coming Soon!", Sender: self)
           }
           @objc func VideoAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
            AlertHelper.showErrorAlert(WithTitle: "", Message: "Coming Soon!", Sender: self)
           }
           @objc func RewardAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
            
            //rewardVC
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "rewardVC")
            
            self.dismiss(animated: false) {
                if(self.viewController != nil)
                {
                    self.viewController!.navigationController?.pushViewController(initialViewController, animated: true)
                }
            }
           }
    @objc func EventAction(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        //EventAndProgramsVC
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "EventAndProgramsVC")
        
        self.dismiss(animated: false) {
            if(self.viewController != nil)
            {
                self.viewController!.navigationController?.pushViewController(initialViewController, animated: true)
            }
        }
    }
           @objc func ProfileAction(_ sender: UITapGestureRecognizer? = nil) {
               // handling code
            
//            SettingController
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "SettingController")
            
            self.dismiss(animated: false) {
                if(self.viewController != nil)
                {
                    self.viewController!.navigationController?.pushViewController(initialViewController, animated: true)
                }
            }
            
//            AlertHelper.showErrorAlert(WithTitle: "", Message: "Coming Soon!", Sender: self)
           }
          
    @objc func BackGroundAction(sender: UITapGestureRecognizer) {
        // handling code
        self.view.removeFromSuperview()
    }
    
    @IBAction func BackGroundBtn_Click(_ sender: Any) {
//        self.view.removeFromSuperview()
//        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    
    
       }



       extension SideMenuViewController{
           
           func SetUpActionOnViews() {
               
            HomeMenu.isUserInteractionEnabled = true
               let Hometap = UITapGestureRecognizer(target: self, action: #selector(self.HomeAction(_:)))
               HomeMenu.addGestureRecognizer(Hometap)
            
            Activity.isUserInteractionEnabled = true
               let ActivityTab = UITapGestureRecognizer(target: self, action: #selector(self.ActivityAction(_:)))
               Activity.addGestureRecognizer(ActivityTab)
            
               let BookingTab = UITapGestureRecognizer(target: self, action: #selector(self.BookingAction(_:)))
               Booking.addGestureRecognizer(BookingTab)
               let NewsTab = UITapGestureRecognizer(target: self, action: #selector(self.NewsAction(_:)))
               News.addGestureRecognizer(NewsTab)
               let GalleryTab = UITapGestureRecognizer(target: self, action: #selector(self.GalleryAction(_:)))
               Gallery.addGestureRecognizer(GalleryTab)
               let MusicTab = UITapGestureRecognizer(target: self, action: #selector(self.MusicAction(_:)))
               Music.addGestureRecognizer(MusicTab)
               let VideoTab = UITapGestureRecognizer(target: self, action: #selector(self.VideoAction(_:)))
               Video.addGestureRecognizer(VideoTab)
               let RewardTab = UITapGestureRecognizer(target: self, action: #selector(self.RewardAction(_:)))
               Reward.addGestureRecognizer(RewardTab)
               let EventTab = UITapGestureRecognizer(target: self, action: #selector(self.EventAction(_:)))
               Event.addGestureRecognizer(EventTab)
               let ProfileTab = UITapGestureRecognizer(target: self, action: #selector(self.ProfileAction(_:)))
               Profile.addGestureRecognizer(ProfileTab)
               
//            let BackGroundTap = UITapGestureRecognizer(target: self, action: #selector(self.BackGroundAction(sender:)))
//            BackGroundTap.numberOfTapsRequired = 1
//            backgroundView.isUserInteractionEnabled = true
//            backgroundView.addGestureRecognizer(BackGroundTap)
//            self.view.addSubview(backgroundView)
            
           }
       }

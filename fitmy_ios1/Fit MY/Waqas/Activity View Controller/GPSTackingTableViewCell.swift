//
//  GPSTackingTableViewCell.swift
//  Fit MY
//
//  Created by Waqas on 04/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class GPSTackingTableViewCell: UITableViewCell {

    @IBOutlet var GPSTracking: UILabel!
    @IBOutlet var FitnessActivity: UILabel!
    @IBOutlet var BackView: UIView!
    @IBOutlet var ImageBackView: CircleButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        BackView.layer.cornerRadius = 10
        ImageBackView.layer.cornerRadius = ImageBackView.bounds.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}

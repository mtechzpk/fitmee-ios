//
//  ActivityTableviewCell.swift
//  Fit MY
//
//  Created by Waqas on 04/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class ActivityTableviewCell: UITableViewCell {

    
    @IBOutlet var ActivityNameLabel: UILabel!
    @IBOutlet var BackView: UIView!
    @IBOutlet var ImageName: CircleButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        BackView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ActivityViewController.swift
//  Fit MY
//
//  Created by Waqas on 04/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var ActivityTableView: UITableView!
    
    let ActivityArray = ["","Walking","Running","Stairs Climbing","Cycling","Hiking","Swimming","Aerobic","Indoor Running","Sleep"]
    
    let ImagesName = ["","walking_icon","running_icon","stairs_icon","cycling_icon","hiking_icon","swimming_icon","aerobic_icon","indoor_running_activityicon","sleep_activity_icon"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityTableView.rowHeight = 80
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ActivityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! GPSTackingTableViewCell
            cell.GPSTracking.text = "GPS Tracking"
            cell.FitnessActivity.text = "Fitness Activity"
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! ActivityTableviewCell
            cell.ActivityNameLabel.text = ActivityArray[indexPath.row]
            cell.ImageName.image = UIImage(named: ImagesName[indexPath.row])
        
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row > 0)
        {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Fitness_History_Two") as? FitnessHistoryTwoController {
                viewController.fill(title: ActivityArray[indexPath.row])
                self.navigationController?.pushViewController(viewController, animated: true)
//                present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    @IBAction func startBtn_Click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "ExerciseTwoController")
        
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
     @IBAction func menuBtn_Click(_ sender: Any) {
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            var frm = initialViewController.view.frame
            frm = self.view.frame
            initialViewController.view.frame = frm
            
            initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
    //        self.view.addSubview(initialViewController.view)
            initialViewController.viewController = self
            self.present(initialViewController, animated: false, completion: nil)
    //        self.navigationController?.pushViewController(initialViewController, animated: false)
        }


}

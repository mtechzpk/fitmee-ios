//
//  RoundedCorners.swift
//  Fit MY
//
//  Created by Waqas on 04/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit
@IBDesignable
class RoundedCorners: UITextField {

    
    @IBInspectable
    var cornerRadius: CGFloat {
      get {
        return layer.cornerRadius
      }
      set {
        layer.cornerRadius = newValue
      }
    }
    
    
//    required public init?(coder aDecoder: NSCoder) {
//
//        super.init(coder: aDecoder)
//
//        self.layer.cornerRadius = 15
//    }

}

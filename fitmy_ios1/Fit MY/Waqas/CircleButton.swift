//
//  CircleButton.swift
//  Fit MY
//
//  Created by Waqas on 04/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit
@IBDesignable
class CircleButton: UIImageView {

    @IBInspectable
    var circlebutton:CGFloat{
        get{
            return layer.cornerRadius
        }
        set{
            return layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
      get {
        return layer.borderWidth
      }
      set {
        layer.borderWidth = newValue
      }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
      get {
        if let color = layer.borderColor {
          return UIColor(cgColor: color)
        }
        return nil
      }
      set {
        if let color = newValue {
          layer.borderColor = color.cgColor
        } else {
          layer.borderColor = nil
        }
      }
    }

}

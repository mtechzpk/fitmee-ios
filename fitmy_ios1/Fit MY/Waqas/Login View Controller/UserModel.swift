// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let userModel = try? newJSONDecoder().decode(UserModel.self, from: jsonData)

import Foundation

// MARK: - UserModel
class UserModel {
    var id: Int = 0
    var user_type:String = "", category_type:String = "", full_name:String = "", email: String = ""
    var email_verified_at: String = ""
    var image: String = ""
    var phone:String = "", gender:String = "", height:String = "", weight: String = ""
    var nation:String = "", citizenship:String = "", office_tel_no:String = "", office_fax_no: String = ""
    var address:String = "", country:String = "", state:String = "", city: String = ""
    var zip_code:String = "", type:String = "", fitness_points:String = "0.0", created_at: String = ""
    var updated_at: String? = ""
}

//
//  LoginViewController.swift
//  Fit MY
//
//  Created by Waqas on 05/03/2020.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet var EmailTextField: RoundedCorners!
    @IBOutlet var PasswordTextFile: RoundedCorners!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func LoginButton(_ sender: Any) {
        if EmailTextField.text == "" || PasswordTextFile.text == ""{
            AlertHelper.showErrorAlert(WithTitle: "Alert", Message: "All fields are necessary to fill", Sender: self)
        } else {
            submitRegistration()
        }
    }
    @IBAction func BackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension LoginViewController {
    func submitRegistration() {
        let url = "http://172.104.217.178/fitmy/public/api/login"
        var header = [String:String]()
        header["Accept"] = "application/json"
        var body = [String:String]()
        body["email"] = EmailTextField.text!
        body["password"] = PasswordTextFile.text!
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { (JSON) in
            DispatchQueue.main.async {
                let data = JSON.dictionaryValue
                //print(data)
                if data.count > 0 {
                    if let user = data["user"]?.dictionaryValue
                    {
                        userModel.id = user["id"]!.intValue
                        userModel.user_type = user["user_type"]!.stringValue
                        userModel.category_type = user["category_type"]!.stringValue
                        userModel.full_name = user["full_name"]!.stringValue
                        userModel.email = user["email"]!.stringValue
                        userModel.email_verified_at = user["email_verified_at"]!.stringValue
                        userModel.image = user["image"]!.stringValue
                        userModel.phone = user["phone"]!.stringValue
                        userModel.gender = user["gender"]!.stringValue
                        userModel.height = user["height"]!.stringValue
                        userModel.weight = user["weight"]!.stringValue
                        userModel.nation = user["nation"]!.stringValue
                        userModel.citizenship = user["citizenship"]!.stringValue
                        userModel.office_tel_no = user["office_tel_no"]!.stringValue
                        userModel.office_fax_no = user["office_fax_no"]!.stringValue
                        userModel.address = user["address"]!.stringValue
                        userModel.country = user["country"]!.stringValue
                        userModel.state = user["state"]!.stringValue
                        userModel.city = user["city"]!.stringValue
                        userModel.zip_code = user["zip_code"]!.stringValue
                        userModel.type = user["type"]!.stringValue
                        userModel.fitness_points = user["fitness_points"]!.stringValue
                        userModel.created_at = user["created_at"]!.stringValue
                        userModel.updated_at = user["updated_at"]!.stringValue
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "FitnessDiaryController")
                        self.navigationController?.pushViewController(initialViewController, animated: true)
                    } else {
                        AlertHelper.showErrorAlert(WithTitle: "Alert", Message: "Invalid credentials", Sender: self)
                    }
                } else {
                    print("Invalid response")
                }
            }
        },
         failure: { (error) in
            print(error)
        })
    }
}

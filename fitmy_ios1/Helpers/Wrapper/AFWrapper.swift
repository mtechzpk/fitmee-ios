
//
//  AFWrapper.swift
//  AFSwiftDemo
//
//  Created by Ashish on 10/4/16.
//  Copyright © 2016 Ashish Kakkad. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON

class AFWrapper: NSObject {
    
    class func requestGETURL(_ strURL: String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        
        Alamofire.request(strURL, method: .get , encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
//            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            
//            print(responseObject)
            print(responseObject.response?.statusCode)
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
//                print("Response", resJson)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
}

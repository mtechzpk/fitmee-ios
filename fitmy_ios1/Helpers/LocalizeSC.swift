//
//  LocalizeSC.swift
//  Fit MY
//
//  Created by Azlan Shah on 28/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import Localize_Swift

class LocalizeSC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateLocalizedStrings()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateLocalizedStrings), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: -
    
    @objc func updateLocalizedStrings() {
        // Abstract method
    }
}

//
//  customizeTF.swift
//  Fitme
//
//  Created by apple on 3/4/20.
//  Copyright © 2020 apple. All rights reserved.
//

import UIKit
@IBDesignable
class customizeTF: UITextField {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet{
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet{
            layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
    }
    
    @IBInspectable var rightImage: UIImage?{
        didSet{
            updateView()
        }
    }
    func updateView(){
        if let image = rightImage{
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 15))
            imageView.image = image
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            view.addSubview(imageView)
            rightView = view
        } else{
            leftViewMode = .never
        }
    }
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
}

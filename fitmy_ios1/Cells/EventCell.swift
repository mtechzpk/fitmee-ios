//
//  EventCell.swift
//  Fit MY
//
//  Created by Azlan Shah on 25/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import Reusable
import Kingfisher

class EventCell: UITableViewCell, NibReusable  {
    
    @IBOutlet weak var promoImg: UIImageViewX!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    private var info: String = "<Info?>"
    private var details: String = "<Details?>"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fill(title: String, detail: String, price: String, image: String) {
        self.titleLbl.text = title
        self.detailLbl.text = detail
        self.priceLbl.text = price
        self.promoImg.kf.setImage(with: URL(string: image), options: [.transition(.fade(0.3))])
    }
    
    @IBAction func infoAction(_ sender: UIButton) {
        //        let infoVC = InfoViewController.instantiate()
        //        infoVC.setInfo(self.info)
        //        self.window?.rootViewController?.present(infoVC, animated: true, completion: nil)
    }
}

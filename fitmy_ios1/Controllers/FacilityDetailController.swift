//
//  EventDetalController.swift
//  Fit MY
//
//  Created by Azlan Shah on 25/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class FacilityDetailController: UIViewController {
    
    @IBOutlet weak var promoImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    
    var thisImg = String()
    var thisTitle = String()
    var thisLocation = String()
    var thisDuration = String()
    var thisDetail = String()
    var thisTerms = String()
    var thisID = String()
    var thisRewardImg = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        fill()
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func fill() {
        promoImg.kf.setImage(with: URL(string: thisImg), options: [.transition(.fade(0.3))])
        titleLbl.text = thisTitle
        locationLbl.text = thisLocation
        
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

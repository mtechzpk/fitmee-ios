//
//  RewardModel.swift
//  Fit MY
//
//  Created by Mahad on 3/9/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class RewardModel: NSObject {
    var id:Int = 0
    var reward_name:String = ""
    var reward_description:String = ""
    var filename:String = ""
    var term_and_conditions:String = ""
    var required_fitness_points:String = ""
    var reward_category_id:Int = 0
}

//
//  RewardTVC.swift
//  Fit MY
//
//  Created by Mahad on 3/9/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class RewardTVC: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var promoImg: UIImageViewX!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(title: String, detail: String, price: String, image: String) {
        self.titleLbl.text = title
        self.detailLbl.text = detail
        self.priceLbl.text = price
        if image.isEmpty {
            self.promoImg.image = nil
        } else {
            self.promoImg.kf.setImage(with: URL(string: image))
        }
    }
}

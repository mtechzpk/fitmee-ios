//
//  RewardDetailVC.swift
//  Fit MY
//
//  Created by Mahad on 3/11/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class RewardDetailVC: UIViewController {

    
    // MARK: - Properties
    
    var rewardDetail:RewardModel?
    
    
    // MARK: - IBOutlets
    @IBOutlet var backimgView: UIImageView!
    @IBOutlet var frontimgView: UIImageView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    @IBOutlet var termsLbl: UILabel!
    
    @IBOutlet var fitpointsLbl: UILabel!
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissView(gesture:)))
        slideDown.direction = .right
        view.addGestureRecognizer(slideDown)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if rewardDetail != nil{
            backimgView.kf.setImage(with: URL(string: rewardDetail!.filename))
                       frontimgView.kf.setImage(with: URL(string: rewardDetail!.filename))
                       
                        titleLbl.text = rewardDetail!.reward_name
                       descLbl.text = rewardDetail!.reward_description
                       termsLbl.text = rewardDetail!.term_and_conditions
            fitpointsLbl.text = userModel.fitness_points + "ƒ"
        }
    }
    
    // MARK: - Set up
    // MARK: - IBActions
    
    @IBAction func redeemBtn_Click(_ sender: Any) {
        redeemReward()
    }
    
    
    // MARK: - Navigation
    
    @objc func dismissView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Network Manager calls
    
    func redeemReward() {
        let header:[String:String] = ["Accept":"application/json"]
        let url = "http://172.104.217.178/fitmy/public/api/buy_reward"
        var body = [String:Any]()
        body["user_id"] = userModel.id//UserDefaults.standard.string(forKey: "user_id")
        body["reward_id"] = rewardDetail!.id
        
        
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { response in
            let responseData = response.dictionaryValue
            //print(json)
            
            if(responseData["status"] == 200)
                       {
                        
                        userModel.fitness_points = responseData["remaining_fitness_points"]!.stringValue
                        
                        AlertHelper.showSuccessAlert(WithTitle: "Success", Message: responseData["message"]?.stringValue ?? "Congratulation!, You Purchased That Reward!", Sender: self)
//                           let data = responseData["data"]?.arrayValue
//                           
//                           for obj in data ?? []
//                           {
//                               let id = obj["id"].intValue
//                                                  let name = obj["name"].stringValue
//                                                  let filename = obj["filename"].stringValue
//                                                  
//                                                  let obj = allCategories(id: id, name: name, filename: filename)
////                                                  self.arrayCategories.append(obj)
//                           }
//                           self.catCollectionView.reloadData()
                           
                       }
                       else
                       {
                           AlertHelper.showErrorAlert(WithTitle: "Error", Message: responseData["message"]?.stringValue ?? "Registration Failed!", Sender: self)
                       }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    // MARK: - Extensions
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

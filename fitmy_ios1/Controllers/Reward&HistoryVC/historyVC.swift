//
//  HistoryVC.swift
//  Fit MY
//
//  Created by Mahad on 3/11/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {

    // MARK: - Properties
    var rewardHistoryArray = [RewardModel]()
    // MARK: - IBOutlets
    
    @IBOutlet var historyTV: UITableView!
    @IBOutlet var fitpointsLbl: UILabel!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyTV.delegate = self
        historyTV.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        GetPurchasedRewards()
        fitpointsLbl.text = userModel.fitness_points + "ƒ"
    }
    
    // MARK: - Set up
    // MARK: - IBActions
   
    
    @IBAction func menuBtn_Click(_ sender: Any) {
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            var frm = initialViewController.view.frame
            frm = self.view.frame
            initialViewController.view.frame = frm
            
            initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
    //        self.view.addSubview(initialViewController.view)
            initialViewController.viewController = self
            self.present(initialViewController, animated: false, completion: nil)
    //        self.navigationController?.pushViewController(initialViewController, animated: false)
        }
    
    @IBAction func allRewardBtn_Click(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    func GetPurchasedRewards() {
        let header:[String:String] = ["Accept":"application/json"]
        let url = "http://172.104.217.178/fitmy/public/api/purchased_rewards"
        var body = [String:Any]()
        body["user_id"] = userModel.id//UserDefaults.standard.string(forKey: "user_id")
        
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { (response) in
            let json = response.dictionaryValue
            //print(json)
            self.rewardHistoryArray.removeAll()
            if(json["status"] == 200)
            {
                let data = json["data"]?.arrayValue
                
                if let responseData = data{
                    var reward:RewardModel
                    for obj in responseData{
                        reward = RewardModel()
                        
                        let i = obj["reward"].dictionaryValue
                        
                        reward.id = i["id"]!.intValue
                        reward.reward_name = i["reward_name"]!.stringValue
                        reward.reward_description = i["reward_description"]!.stringValue
                        
                        
                        reward.filename = i["filename"]!.stringValue
                        reward.term_and_conditions = i["term_and_conditions"]!.stringValue
                        reward.reward_category_id = i["reward_category_id"]!.intValue
                        reward.required_fitness_points = i["required_fitness_points"]!.stringValue
                        
                        self.rewardHistoryArray.append(reward)
                    }
                    self.historyTV.reloadData()
                }
            }
            else
            {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: json["message"]?.stringValue ?? "Registration Failed!", Sender: self)
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    // MARK: - Extensions
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Extensions
extension HistoryVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rewardHistoryArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RewardHistoryTVC", for: indexPath) as! RewardTVC
        
        var reward = rewardHistoryArray[indexPath.row]
        
        cell.fill(title: reward.reward_name, detail: reward.reward_description, price: reward.required_fitness_points, image: reward.filename)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rewardDetailVC:RewardDetailVC = storyboard.instantiateViewController(withIdentifier: "RewardDetailVC") as! RewardDetailVC
        rewardDetailVC.rewardDetail = rewardHistoryArray[indexPath.row]
        self.navigationController?.pushViewController(rewardDetailVC, animated: true)
    }
}

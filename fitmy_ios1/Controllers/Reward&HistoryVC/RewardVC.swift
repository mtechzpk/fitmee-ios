//
//  RewardVC.swift
//  Fit MY
//
//  Created by Mahad on 3/6/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class RewardVC: UIViewController {

    
    
    // MARK: - Properties
    var arrayCategories = [allCategories]()
    var arrayRewards = [RewardModel]()
    
    var isCategoryFilter:Bool = false
    var filterarrayRewards = [RewardModel]()
    
    // MARK: - IBOutlets
    
    @IBOutlet var fitpointsLbl: UILabel!
    @IBOutlet var catCollectionView: UICollectionView!
    
    @IBOutlet var rewardTV: UITableView!
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        catCollectionView.delegate = self
        catCollectionView.dataSource = self
        
        rewardTV.delegate = self
        rewardTV.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetRewardCategories()
        fitpointsLbl.text = userModel.fitness_points + "ƒ" //(UserDefaults.standard.string(forKey: "fitness_points") ?? "0.0")+"ƒ"
        GetAllRewards()
    }
    
    // MARK: - Set up
    // MARK: - IBActions
    
    @IBAction func HistoryBtn_Click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let historyVC:HistoryVC = storyboard.instantiateViewController(withIdentifier: "HistoryVC") as! HistoryVC
        self.navigationController?.pushViewController(historyVC, animated: true)
    }
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    
    func GetRewardCategories() {
        
        let url = "http://172.104.217.178/fitmy/public/api/get_reward_categories"
        var header = [String:String]()
        header["Accept"] = "application/json"
        
               var body = [String:Any]()
        body["user_id"] = userModel.id//UserDefaults.standard.string(forKey: "user_id") // UserID
        
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { (Response) in
            let responseData = Response.dictionaryValue
            if(responseData["status"] == 200)
            {
                let data = responseData["data"]?.arrayValue
                
                for obj in data ?? []
                {
                    let id = obj["id"].intValue
                                       let name = obj["name"].stringValue
                                       let filename = obj["filename"].stringValue
                                       
                                       let obj = allCategories(id: id, name: name, filename: filename)
                                       self.arrayCategories.append(obj)
                }
                self.catCollectionView.reloadData()
                
            }
            else
            {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: responseData["message"]?.stringValue ?? "Registration Failed!", Sender: self)
            }
            
            
        }) { (error) in
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: error.localizedDescription, Sender: self)
            return
        }
        
        
    }
    
    
    func GetAllRewards() {
        let header:[String:String] = ["Accept":"application/json"]
        let url = "http://172.104.217.178/fitmy/public/api/get_all_rewards"
        AFWrapper.requestGETURL(url, params: nil, headers: header, success: { response in
            let json = response.dictionaryValue
            //print(json)
            let data = json["data"]?.arrayValue
            self.arrayRewards.removeAll()
            if let responseData = data{
                var reward:RewardModel
                for i in responseData{
                    reward = RewardModel()
                    reward.id = i["id"].intValue
                    reward.reward_name = i["reward_name"].stringValue
                    reward.reward_description = i["reward_description"].stringValue
                

                    reward.filename = i["filename"].stringValue
                    reward.term_and_conditions = i["term_and_conditions"].stringValue
                    reward.reward_category_id = i["reward_category_id"].intValue
                    reward.required_fitness_points = i["required_fitness_points"].stringValue
                    
                    self.arrayRewards.append(reward)
                }
                self.rewardTV.reloadData()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
     @IBAction func menuBtn_Click(_ sender: Any) {
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            var frm = initialViewController.view.frame
            frm = self.view.frame
            initialViewController.view.frame = frm
            
            initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
    //        self.view.addSubview(initialViewController.view)
            initialViewController.viewController = self
            self.present(initialViewController, animated: false, completion: nil)
    //        self.navigationController?.pushViewController(initialViewController, animated: false)
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - Extensions
extension RewardVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCategories.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventsCVCell", for: indexPath) as! EventsCVCell
        cell.bgView.layer.cornerRadius = 7

        if indexPath.row == 0{
            cell.programLabel.text = "All"
            cell.programImage.image = UIImage(named: "music_menu_icon")
        } else {
            cell.programImage.kf.setImage(with: URL(string: arrayCategories[indexPath.row - 1].filename))
            cell.programLabel.text = arrayCategories[indexPath.row - 1].name
        }

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            isCategoryFilter = false
            rewardTV.reloadData()
        }
        else
        {
            let catid = arrayCategories[indexPath.row-1].id
            isCategoryFilter = true
            filterarrayRewards = arrayRewards.filter{$0.reward_category_id == catid}
            rewardTV.reloadData()
        }
    }
    
}

extension RewardVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isCategoryFilter
        {
            return filterarrayRewards.count
        }
        return arrayRewards.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RewardTVC", for: indexPath) as! RewardTVC
        
        var reward = RewardModel()
        
        if isCategoryFilter
        {
            reward = filterarrayRewards[indexPath.row]
        }
        else
        {
            reward = arrayRewards[indexPath.row]
        }
        
        cell.fill(title: reward.reward_name, detail: reward.reward_description, price: reward.required_fitness_points, image: reward.filename)
        
//        cell.eventImage.kf.setImage(with: URL(string: arrayEvents[indexPath.row].filename))
//        cell.eventName.text = arrayEvents[indexPath.row].event_name
//        cell.eventDescription.text = arrayEvents[indexPath.row].description
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rewardDetailVC:RewardDetailVC = storyboard.instantiateViewController(withIdentifier: "RewardDetailVC") as! RewardDetailVC
        rewardDetailVC.rewardDetail = isCategoryFilter == true ? filterarrayRewards[indexPath.row]: arrayRewards[indexPath.row]
        self.navigationController?.pushViewController(rewardDetailVC, animated: true)
    }
}

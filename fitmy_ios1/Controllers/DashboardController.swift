//
//  DashboardController.swift
//  Fit MY
//
//  Created by Azlan Shah on 28/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import Kingfisher
import Localize_Swift

struct Profile: Codable {
    var total_points = Int()
    var birth_year = Int()
    var full_name = String()
    var nick_name = String()
    var height_cm = Double()
    var weight_kg = Double()
    var nationality = String()
    var race = String()
    var gender = String()
    var email = String()
    var total_steps = Int()
    var birth_date = String()
    var photo = String()
    var id = String()
    var used_points = Int()
    var phone_num = String()
    var user_password = String()
    var app_password = String()
    var date_created = String()
}

struct News {
    var title = String()
    var body = String()
    var date = String()
    var img = String()
    var source = String()
}

class DashboardController: UIViewController {

    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userLocationLbl: UILabel!
    @IBOutlet weak var profileImg: UIImageViewX!
    @IBOutlet weak var availablePointLbl: UILabel!
    
    @IBOutlet weak var activityScrollView: UIView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityStackViewChildView: UIView!
    
    @IBOutlet weak var newsScrollView: UIView!
    @IBOutlet weak var newsStackView: UIStackView!
    @IBOutlet weak var newsStackViewChildView: UIView!
    
    @IBOutlet weak var communityScrollView: UIView!
    @IBOutlet weak var communityStackView: UIStackView!
    @IBOutlet weak var communityStackViewChildView: UIView!
    
    
    
    // Static label for translation only
    @IBOutlet weak var pointLbl: UILabel!
    @IBOutlet weak var settingLbl: UILabel!
    @IBOutlet weak var shortcut1Lbl: UILabel!
    @IBOutlet weak var shortcut2Lbl: UILabel!
    @IBOutlet weak var shortcut3Lbl: UILabel!
    @IBOutlet weak var shortcut4Lbl: UILabel!
    @IBOutlet weak var newsLbl: UILabel!
    @IBOutlet weak var eventLbl: UILabel!
    @IBOutlet weak var communityLbl: UILabel!
    
    
    var allNews = [News]()
    var communityNews = [News]()
    
    var currentNews = [News]()
    var currentCommunityNews = [News]()
    
    var events = [Event]()
    var currentEvent = [Event]()
    
    let encoder = JSONEncoder()
    let defaults = UserDefaults.standard
    
    var profile = Profile()
    
    var newsView = UIView()
    var communityView = UIView()
    var activityView = UIView()
    
    var firstTime = false //check if user is logged in for first time
    
    @objc func localizeLabel() {
        pointLbl.text = "Fit Point".localized()
        settingLbl.text = "Setting".localized()
        shortcut1Lbl.text = "Record Exercise".localized()
        shortcut2Lbl.text = "Join Event".localized()
        shortcut3Lbl.text = "Book Facility".localized()
        shortcut4Lbl.text = "Redeem Reward".localized()
        newsLbl.text = "News & Promotions".localized()
        eventLbl.text = "Nearby Events & Programs".localized()
        communityLbl.text = "Community News".localized()
        
        print("Language Changed: Home")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newsView = self.newsStackViewChildView.copyView()
        
        communityView = self.communityStackViewChildView.copyView()
        
        activityView = self.activityStackViewChildView.copyView()
        
        addNews()
        addCommunityNews()
        
        if !isAppAlreadyLaunchedOnce() {
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "onboarding_one") as! OnboardingController
            let navController = UINavigationController(rootViewController: VC1)
            self.present(navController, animated: true, completion: nil)
            firstTime = true //check if user is first time
        } else {
            self.updateProfileDetail()
            updateToken()
            setProfile()
            firstTime = false
        }
        
        if let current = UserDefaults.standard.value(forKey: "current_status") as? String, current == "running" {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Exercise2") as? ExerciseTwoController {
                present(viewController, animated: true, completion: nil)
            }
        }
        
        
        activityScrollView.layoutIfNeeded()
        activityStackView.layoutIfNeeded()
        newsScrollView.layoutIfNeeded()
        newsStackView.layoutIfNeeded()
        communityScrollView.layoutIfNeeded()
        communityStackView.layoutIfNeeded()
        
        localizeLabel()
        
    }
    
    func addNews() {
        allNews.append(News(title: "MAJLIS PELANCARAN PROGRAM REBEN KUNING, PEKA@MRCB",
                            body: "Program PEKA@MRCB adalah sebahagian daripada inisiatif Reben Kuning yang diterajui oleh Kementerian Belia dan Sukan dengan kerjasama Jabatan Penjara Malaysia dan Malaysian Resources Corporation Berhad (MRCB). \n \n PEKA yang bermaksud 'Peluang Kedua Anda' menyediakan peluang pekerjaan untuk pesalah - pesalah yang dipilih yang akan mengakhiri tempoh hukuman mereka. Pesalah - pesalah ini yang disebut sebagai 'Orang Diselia' (ODS) dipilih oleh Jabatan Penjara Malaysia setelah menjalani proses pemilihan yang menyeluruh. \n \n MRCB adalah syarikat pembinaan pertama di Malaysia yang memulakan program ini yang telah memberikan peluang pekerjaan kepada bekas pesalah dalam industri pembinaan dengan mendapat sokongan dari KBS serta Jabatan Penjara Malaysia. \n \n MRCB telah menawarkan faedah pekerjaan kepada ODS yang menyertai PEKA@MRCB termasuk gaji bulanan, sumbangan KWSP, pengangkutan, perubatan dan faedah lain. 30 ODS telah memulakan kerja pada 1 Ogos 2019 di 'casting yard' untuk projek LRT 3 yang diuruskan oleh Reaplite Industry Sdn Bhd sebagai projek perintis. Dua lagi kumpulan ODS akan memulakan PEKA@MRCB yang akan diuruskan oleh Trans Resources Corporation Sdn Bhd, MRCB Builders Sdn Bhd, BBR Construction System Sdn Bhd dan PSL Concrete Sdn Bhd pada 2 Disember 2019. \n \n PEKA@MRCB telah dirasmikan oleh Menteri Belia dan Sukan, YB Syed Saddiq Syed Abdul Rahman, turut hadir Ketua Setiausaha Kementerian Belia dan Sukan, YBrs Dr. Waitchalla R.R.V Suppiah, Ketua Pengarah Jabatan Penjara, YBhg Dato' Sri Hj. Zulkifli Omar dan Naib Presiden Eksekutif MRCB, YBhg Datuk Dell Akhbar Khan.",
                            date: "26/11/2019",
                            img: "",
                            source: "KBS")
        )
        allNews.append(News(title: "PELAN STRATEGIK PEMBANGUNAN ESPORTS 2020 - 2025 KEMENTERIAN BELIA DAN SUKAN.",
                            body: "Pelan Strategik Pembangunan Esports 2020 - 2025 telah dirasmikan oleh Menteri Belia dan Sukan, YB Syed Saddiq Syed Abdul Rahman buat julung kalinya bagi tujuan untuk memberi pendedahan kepada seluruh rakyat Malaysia tentang hala tuju pembangunan esports negara. \n \n Pelan strategik pembangunan esports ini merupakan pelan yang pertama kali dilaksanakan Kementerian Belia dan Sukan yang dirancang sejak Januari 2019, pelan ini menekankan 5 Strategi Utama yang meliputi pemegang taruh dalam industri esports seperti pemain profesional, penggerak industri, pelabur dan agensi-agensi kerajaan. \n \n Dengan adanya pelan strategik pembangunan esports, kini esports terbukti memberi banyak faedah kepada para penggiat industri digital. Sejak tahun 2018 lagi, Malaysia mula diiktiraf antara negara yang mampu menarik dan membuka peluang untuk mengembangkan industri esports dengan lebih meluas dan untuk itu KBS memainkan peranan yang penting bagi mencapai hasrat tersebut.",
                            date: "22/11/2019",
                            img: "",
                            source: "KBS")
        )
        allNews.append(News(title: "Majlis Penyerahan Jalur Gemilang Kontinjen Sukan Sea 2019",
                            body: "Kontinjen Malaysia meletakkan sasaran meraih 70 pingat emas pada Sukan SEA 2019 di Filipina yang akan membuka tirai secara rasmi pada 30 November ini. \n \n Berdasarkan kepada bengkel penetapan sasaran diadakan pada 5 dan 6 November lalu bersama semua pemegang taruh, kontinjen negara dijangka memenangi 70 emas, 51 perak dan 105 gangsa. Sasaran ini dijangka meletakkan kontinjen negara di kedudukan keempat terbaik keseluruhan di kalangan 11 negara Asia Tenggara yang bertanding. \n \n Bersama YB Syed Saddiq Bin Syed Abdul Rahman, Menteri Belia dan Sukan turut hadir, Timbalan Menteri Belia dan Sukan, YB Steven Sim, Presiden Majlis Olimpik Malaysia (MOM), Datuk Seri Mohamad Norza Zakaria, Ketua Pengarah Majlis Sukan Negara, Datuk Ahmad Shapawi Ismail dan ketua kontinjen Malaysia, Datuk Megat Zulkarnain Omardin dan YBhg. Dr. Waitchalla R.R.V, KSU KBS.",
                            date: "21/11/2019",
                            img: "",
                            source: "KBS")
        )
        allNews.append(News(title: "Majlis Menandatangani Perjanjian Penajaan Korporat Sukan Para Antara FWD Takaful Berhad Dengan Bersama MSN",
                            body: "FWD Takaful Berhad menandatangani Perjanjian Penajaan bersama MSN untuk merasmikan penajaan syarikat sebanyak RM5 juta untuk menyokong pembangunan atlet-atlet para negara. \n \n Penajaan itu meliputi kos peralatan sukan, latihan, pakaian, perjalanan dan perbelanjaan lain yang berkaitan pasukan Paralimpik Malaysia menjelang Sukan Paralimpik Tokyo 2020. FWD Takaful juga akan menyediakan perlindungan Takaful untuk seluruh atlet Paralimpik semasa tempoh masa penajaan. \n \n Dato' Ahmad Shapawi Ismail, Ketua Pengarah MSN menandatangani perjanjian bagi pihak MSN dengan Tuan Salim Majid Zain, Ketua Pegawai Eksekutif di FWD Takaful dan disaksikan oleh YB @syedsaddiq , Menteri Belia dan Sukan bagi majlis menandatangani perjanjian penajaan yang diadakan di Pusat Kecemerlangan Sukan Paralimpik Kampung Pandan.",
                            date: "14/11/2019",
                            img: "",
                            source: "KBS")
        )
        currentNews = allNews
    }
    
    func addCommunityNews() {
        communityNews.append(News(title: "PESTA SUKAN MUAR 2019",
                                  body: "Pesta Sukan Muar 2019 semalam telah dihadiri oleh YB Syes Saddiq Syed Abdul Rahman, Menteri Belia dan Sukan dan Atlet Para Negara bertempat di Stadium Sultan Ibrahim Muar, Johor. \n \n Pesta Sukan Muar 2019 ini mempertandingkan 10 jenis sukan antaranya ragbi sentuh, futsal padang, esports, bola jaring, badminton dan sepak takraw. \n \n Seramai 1,200 peserta Unity Fun Run 5km dan 800 peserta Unity Fun Ride 35km telah mengambil bahagian dan turut berlangsung acara zumba di sebelah pagi. Secara keseluruhannya, lebih 5,000 peserta telh turut serta dalam Pesta Sukan ini. \n \n Tahniah diucapkan kepada mereka yang berjaya mendapat tempat di setiap acara dan tahniah juga kepada semua yang turut serta dalam meraikan aktiviti riadah ini.",
                                  date: "24/11/2019",
                                  img: "",
                                  source: "KBS")
        )
        communityNews.append(News(title: "RAKAN MUDA KREATIF GRAVITI",
                                  body: "Rakan Muda Kreatif \"Grafiti\" Peringkat Negeri Wilayah Persekutuan Kuala Lumpur 2019 telah diadakan pada 22 dan 23 November 2019 bertempat di Skate Park Kompleks Rakan Muda Bukit Kiara, KL. Seramai 62 orang peserta telah menyertai program tersebut yang dikelola oleh tenaga pengajar profesional dari EightyFourCube iaitu En. Abdul Rashade. \n \n Melalui elemen gaya hidup Rakan Inovasi ini, peserta diajar mengenai asas seni grafiti dan diberi platform berkarya mengikut kreativiti dan idea masing-masing. Melalui program ini, minat dan potensi belia dapat dikenalpasti, dilatih dan digilap.",
                                  date: "24/11/2019",
                                  img: "",
                                  source: "JABATAN BELIA & SUKAN NEGARA")
        )
        communityNews.append(News(title: "Kursus Kejurulatihan Akar Umbi FAM | 23 & 24 November 2019",
                                  body: "Kursus Kejurulatihan Akar Umbi FAM telah berlangsung di Wisma Persatuan Bolasepak Melaka, Persiaran Kompleks Sukan Hang Jebat, Melaka selama dua hari. \n \n Seramai 30 peserta telah menyertai kursus tersebut anjuran Persatuan Bolasepak Melaka (MUSA) yang dikendalikan oleh Jurulatih Developer Akar Umbi Tahap 1 FAM, Mohd Fauzi bin Mohtar dan dibantu oleh Navasellan A/L Shunmugam. \n \n Turut hadir adalah Setiausaha Agung MUSA, Farhan bin Ibrahim, Pegawai Khas Presiden MUSA, Syahrul Nizam bin Shamsudin dan Pengarah Teknikal MUSA, Richard Sinnappan.",
                                  date: "26/11/2019",
                                  img: "",
                                  source: "FAM")
        )
        communityNews.append(News(title: "SUKAN SEA FILIPINA 2019",
                                  body: "Komen Datuk Ong Kim Swee selepas tamat perlawanan pertama menentang Myanmar 🇲🇲 tadi. Beliau nampak positif walaupun pasukan terikat 1-1 dan mahu para pemain fokus untuk 3 perlawanan seterusnya yang sukar. \n \n Beliau juga berpuas hati dengan persembahan pemain #HarimauMalaya B-19 Luqman Hakim dan Umar Hakeem, namun yakin mereka mampu menunjukkan aksi lebih baik. #TeamMsiaSukanSEA2019 #KamiTeamMalaysia",
                                  date: "25/11/2019",
                                  img: "",
                                  source: "TEAM MALAYSIA")
        )
        currentCommunityNews = communityNews
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateProfileDetail()
        localizeLabel()
        
        if !isAppAlreadyLaunchedOnce() {
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "onboarding_one") as! OnboardingController
            let navController = UINavigationController(rootViewController: VC1)
            self.present(navController, animated:true, completion: nil)
        } else {
            //if user just login/register, update token and event list, else just update profile
            if firstTime {
                updateToken()
                firstTime = false
            } else {
                updateProfile()
                setProfile()
            }
            
            getAllNews()
            getAllCommunityNews()
            getAllEvent()
        }
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            print("Bearer \(token)")
        }
    }
    
    func updateNews() {
        for subview in newsStackView.subviews {
            subview.removeFromSuperview()
        }
        for (index, news) in self.currentNews.enumerated() {
            let activityView2 = newsView.copyView()
            
            // 0:image, 1:label, 2:button
            let img = activityView2.subviews[0] as! UIImageViewX
            img.kf.setImage(with: URL(string: news.img), options: [.transition(.fade(0.3))])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    if Bool.random() {
                        img.image = #imageLiteral(resourceName: "72234322_2621177168114965_4138154745783123968_n")
                    } else {
                        img.image = #imageLiteral(resourceName: "72978757_2621177041448311_6463973939948290048_n")
                    }
                }
            }
            img.cornerRadius = 8
            let lblView = activityView2.subviews[1] as! UIViewX
            lblView.cornerRadius = 8
            let title = lblView.subviews[0] as! UILabel
            title.text = news.title
            let desc = lblView.subviews[1] as! UILabel
            desc.text = news.body
            let date = lblView.subviews[2] as! UILabel
            date.text = " \(news.date) "
            let btn = activityView2.subviews[2] as! UIButton
            btn.tag = index
            btn.addTarget(self, action: #selector(self.showNews(sender:)), for: .touchUpInside)
            self.newsStackView.addArrangedSubview(activityView2)
        }
        self.newsScrollView.layoutIfNeeded()
        self.newsStackView.layoutIfNeeded()
    }
    
    func updateCommunityNews() {
        for subview in communityStackView.subviews {
            subview.removeFromSuperview()
        }
        for (index, news) in self.currentCommunityNews.enumerated() {
            let activityView2 = communityView.copyView()
            
            // 0:image, 1:label, 2:button
            let img = activityView2.subviews[0] as! UIImageViewX
            img.kf.setImage(with: URL(string: news.img), options: [.transition(.fade(0.3))])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    if Bool.random() {
                        img.image = #imageLiteral(resourceName: "72234322_2621177168114965_4138154745783123968_n")
                    } else {
                        img.image = #imageLiteral(resourceName: "72978757_2621177041448311_6463973939948290048_n")
                    }
                }
            }
            img.cornerRadius = 8
            let lblView = activityView2.subviews[1] as! UIViewX
            lblView.cornerRadius = 8
            let title = lblView.subviews[0] as! UILabel
            title.text = news.title
            let desc = lblView.subviews[1] as! UILabel
            desc.text = news.body
            let date = lblView.subviews[2] as! UILabel
            date.text = " \(news.date) "
            let btn = activityView2.subviews[2] as! UIButton
            btn.tag = index
            btn.addTarget(self, action: #selector(self.showCommunityNews(sender:)), for: .touchUpInside)
            self.communityStackView.addArrangedSubview(activityView2)
        }
        self.communityScrollView.layoutIfNeeded()
        self.communityStackView.layoutIfNeeded()
    }
    
    func updateEvent() {
        for subview in activityStackView.subviews {
            subview.removeFromSuperview()
        }
        for (index, event) in self.currentEvent.enumerated() {
            let activityView2 = activityView.copyView()
//            print("Activity View:", index, activityView.frame)
            
            // 0:image, 1:label, 2:button
            let img = activityView2.subviews[0] as! UIImageViewX
            img.kf.setImage(with: URL(string: event.cover_image), options: [.transition(.fade(0.3))])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    if Bool.random() {
                        img.image = #imageLiteral(resourceName: "72234322_2621177168114965_4138154745783123968_n")
                    } else {
                        img.image = #imageLiteral(resourceName: "72978757_2621177041448311_6463973939948290048_n")
                    }
                }
            }
            img.cornerRadius = 8
            let lblView = activityView2.subviews[1] as! UIViewX
            lblView.cornerRadius = 8
            let title = lblView.subviews[0] as! UILabel
            title.text = event.name
            let desc = lblView.subviews[1] as! UILabel
            desc.text = event.about_desc
            
            let dateFormatterMYSQL = DateFormatter()
            dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let eventDate = dateFormatterMYSQL.date(from: event.event_datetime)
            
            let dateFormatterSwift = DateFormatter()
            dateFormatterSwift.dateStyle = .short
            dateFormatterSwift.timeStyle = .none
            
            let date = lblView.subviews[2] as! UILabel
            date.text = " \(dateFormatterSwift.string(from: eventDate!)) "
            
            let btn = activityView2.subviews[2] as! UIButton
            btn.tag = index
            btn.addTarget(self, action: #selector(self.showEvent(sender:)), for: .touchUpInside)
            self.activityStackView.addArrangedSubview(activityView2)
        }
        self.activityScrollView.layoutIfNeeded()
        self.activityStackView.layoutIfNeeded()
    }
    
    @objc func showEvent(sender: UIButton) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Event_Detail") as? EventDetailController {
            viewController.thisTitle = currentEvent[sender.tag].name
            viewController.thisDetail = currentEvent[sender.tag].about_desc
            viewController.thisImg = currentEvent[sender.tag].cover_image
            viewController.thisLocation = currentEvent[sender.tag].venue
            viewController.thisRewardImg = currentEvent[sender.tag].reward_image
            viewController.thisID = currentEvent[sender.tag].id
            present(viewController, animated: true, completion: nil)
        }
        
    }
    
    @objc func showNews(sender: UIButton) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "News_Detail") as? NewsDetailController {
            viewController.setNews(title: allNews[sender.tag].title, desc: allNews[sender.tag].body, date: allNews[sender.tag].date, source: allNews[sender.tag].source, img: allNews[sender.tag].img)
            present(viewController, animated: true, completion: nil)
        }
        
    }
    
    @objc func showCommunityNews(sender: UIButton) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "News_Detail") as? NewsDetailController {
            viewController.setNews(title: communityNews[sender.tag].title, desc: communityNews[sender.tag].body, date: communityNews[sender.tag].date, source: communityNews[sender.tag].source, img: communityNews[sender.tag].img)
            present(viewController, animated: true, completion: nil)
        }
        
    }
    
    func updateProfileDetail() {
        if let data = defaults.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profile = try? decoder.decode(Profile.self, from: data) {
                self.profile = profile
                print(profile)
            }
        }
    }
    
    func setProfile() {
        userNameLbl.text = profile.nick_name
        availablePointLbl.text = "\(profile.total_points-profile.used_points)ƒ"
        if let image = profile.photo as? String {
            profileImg.kf.setImage(with: URL(string: image), options: [.transition(.fade(0.3))])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    if self.profile.gender == "F" {
                        self.profileImg.image = #imageLiteral(resourceName: "girl")
                        print("female")
                    } else {
                        self.profileImg.image = #imageLiteral(resourceName: "man")
                        print("male")
                    }
                }
            }
        }
    }
    
    func isAppAlreadyLaunchedOnce()->Bool{
        let defaults = UserDefaults.standard
        if let first = defaults.value(forKey: "isAppAlreadyLaunchedOnce") as? Bool {
            return first
        }
        return false
    }
    
    @IBAction func gotoFitness(_ sender: Any) {
        self.tabBarController?.selectedIndex = 1
    }
    @IBAction func gotoEvent(_ sender: Any) {
        self.tabBarController?.selectedIndex = 3
    }
    @IBAction func gotoBooking(_ sender: Any) {
        self.tabBarController?.selectedIndex = 4
    }
    @IBAction func gotoReward(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2
    }
}


extension DashboardController {
    func updateToken() {
        let url = "https://api.fitmyapp.asia/api/user/auth/request_token"
        let header = [String:String]()
        var body = [String:String]()
        body["phone_num"] = profile.phone_num
        body["app_password"] = profile.app_password
        print("Update Token Param: ", body)
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            UserDefaults.standard.set(data["token"]!.stringValue, forKey: "api_token")
                                            let data2 = data["profile"]!.dictionaryValue
                                            let profile = Profile(total_points: data2["total_points"]?.intValue ?? 0,
                                                                  birth_year: data2["birth_year"]?.intValue ?? 0,
                                                                  full_name: data2["full_name"]?.stringValue ?? "",
                                                                  nick_name: data2["nick_name"]?.stringValue ?? "",
                                                                  height_cm: data2["height_cm"]?.doubleValue ?? 0,
                                                                  weight_kg: data2["weight_kg"]?.doubleValue ?? 0,
                                                                  nationality: data2["nationality"]?.stringValue ?? "",
                                                                  race: data2["race"]?.stringValue ?? "",
                                                                  gender: data2["gender"]?.stringValue ?? "",
                                                                  email: data2["email"]?.stringValue ?? "",
                                                                  total_steps: data2["total_steps"]?.intValue ?? 0,
                                                                  birth_date: data2["birth_date"]?.stringValue ?? "",
                                                                  photo: data2["photo"]?.stringValue ?? "",
                                                                  id: data2["id"]?.stringValue ?? "",
                                                                  used_points: data2["used_points"]?.intValue ?? 0,
                                                                  phone_num: data2["phone_num"]?.stringValue ?? "",
                                                                  user_password: self.profile.user_password,
                                                                  app_password: self.profile.app_password,
                                                                  date_created: data2["date_created"]?.stringValue ?? "")
                                            print("Update Token Success")
                                            if let encoded = try? self.encoder.encode(profile) {
                                                self.defaults.set(encoded, forKey: "user_profile")
                                            }
                                            self.updateProfileDetail()
                                            self.getAllEvent()
                                            self.setProfile()
//                                            print(data)
                                        } else {
                                            print("Invalid Update Token response")
                                        }
                                    }
        },
                                 failure: { (error) in
                                    print("Update Token Error")
                                    print(error)
                                    
        })
        
    }
    
    func updateProfile() {
        let url = "https://api.fitmyapp.asia/api/user/auth/request_token"
        let header = [String:String]()
        var body = [String:String]()
        body["phone_num"] = profile.phone_num
        body["app_password"] = profile.app_password
        print("Update Profile Param: ", body)
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            UserDefaults.standard.set(data["token"]!.stringValue, forKey: "api_token")
                                            let data2 = data["profile"]!.dictionaryValue
                                            let profile = Profile(total_points: data2["total_points"]?.intValue ?? 0,
                                                                  birth_year: data2["birth_year"]?.intValue ?? 0,
                                                                  full_name: data2["full_name"]?.stringValue ?? "",
                                                                  nick_name: data2["nick_name"]?.stringValue ?? "",
                                                                  height_cm: data2["height_cm"]?.doubleValue ?? 0,
                                                                  weight_kg: data2["weight_kg"]?.doubleValue ?? 0,
                                                                  nationality: data2["nationality"]?.stringValue ?? "",
                                                                  race: data2["race"]?.stringValue ?? "",
                                                                  gender: data2["gender"]?.stringValue ?? "",
                                                                  email: data2["email"]?.stringValue ?? "",
                                                                  total_steps: data2["total_steps"]?.intValue ?? 0,
                                                                  birth_date: data2["birth_date"]?.stringValue ?? "",
                                                                  photo: data2["photo"]?.stringValue ?? "",
                                                                  id: data2["id"]?.stringValue ?? "",
                                                                  used_points: data2["used_points"]?.intValue ?? 0,
                                                                  phone_num: data2["phone_num"]?.stringValue ?? "",
                                                                  user_password: self.profile.user_password,
                                                                  app_password: self.profile.app_password,
                                                                  date_created: data2["date_created"]?.stringValue ?? "")
                                            print("Update Profile Success")
                                            if let encoded = try? self.encoder.encode(profile) {
                                                self.defaults.set(encoded, forKey: "user_profile")
                                            }
                                            self.updateProfileDetail()
                                            self.setProfile()
                                            //                                            print(data)
                                        } else {
                                            print("Invalid response")
                                        }
                                    }
        },
                                 failure: { (error) in
                                    print("Update Profile Error")
                                    print(error)
                                    
        })
        
    }
    
    func getAllEvent() {
        let url = "https://api.fitmyapp.asia/api/event/inventory/get_events"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        print("Event Header: ", header)
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.arrayValue
                                        self.events.removeAll()
                                        if data.count > 0 {
                                            for (index, value) in data.enumerated() {
                                                self.events.append(Event(id: value["id"].stringValue,
                                                                         name: value["name"].stringValue,
                                                                         cover_image: value["cover_image"].stringValue,
                                                                         event_datetime: value["event_datetime"].stringValue,
                                                                         venue: value["venue"].stringValue,
                                                                         about_desc: value["about_desc"].stringValue,
                                                                         about_image: value["about_image"].stringValue,
                                                                         reward_desc: value["reward_desc"].stringValue,
                                                                         reward_image: value["reward_image"].stringValue,
                                                                         eo_id: value["eo_id"].stringValue,
                                                                         eo_name: value["eo_name"].stringValue))
                                            }
                                            print("Update Event Success")
                                            self.currentEvent = self.events
                                            self.updateEvent()
                                        } else {
                                            print("Invalid Event response")
                                        }
                                    }
        },
                                failure: { (error) in
                                    print("Update Events Error")
                                    print(error)
                                    
        })
        
    }
    
    func getAllNews() {
        let url = "https://api.fitmyapp.asia/api/social/news/get_news"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        print("Event Header: ", header)
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        
                                        let dateFormatterMYSQL = DateFormatter()
                                        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                                        
                                        let dateFormatterSwift = DateFormatter()
                                        dateFormatterSwift.dateFormat = "MM/dd/yy"
                                        
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            self.allNews.removeAll()
                                            for (index, value) in data.enumerated() {
                                                if value["active"].stringValue == "1" {
                                                    print(value["date_created"].stringValue)
                                                    if let mysqlDate = dateFormatterMYSQL.date(from: value["date_created"].stringValue) {
                                                        let date = dateFormatterSwift.string(from: mysqlDate)
                                                        self.allNews.append(News(title: value["title"].stringValue, body: value["body"].stringValue, date: date, img: value["cover_image"].stringValue, source: value["author"].stringValue))
                                                    } else {
                                                        self.allNews.append(News(title: value["title"].stringValue, body: value["body"].stringValue, date: value["date_created"].stringValue, img: value["cover_image"].stringValue, source: value["author"].stringValue))
                                                    }
                                                }
                                            }
                                            print("Update Event Success")
                                            self.currentNews = self.allNews
                                            self.updateNews()
                                        } else {
                                            print("Invalid Event response")
                                        }
                                    }
        },
                                failure: { (error) in
                                    print("Update Events Error")
                                    print(error)
                                    
        })
        
    }
    
    func getAllCommunityNews() {
        let url = "https://api.fitmyapp.asia/api/social/news/get_news_community"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        print("Event Header: ", header)
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        
                                        let dateFormatterMYSQL = DateFormatter()
                                        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                                        
                                        let dateFormatterSwift = DateFormatter()
                                        dateFormatterSwift.dateFormat = "MM/dd/yy"
                                        
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            self.communityNews.removeAll()
                                            for (index, value) in data.enumerated() {
                                                if value["active"].stringValue == "1" {
                                                    print(value["date_created"].stringValue)
                                                    if let mysqlDate = dateFormatterMYSQL.date(from: value["date_created"].stringValue) {
                                                        let date = dateFormatterSwift.string(from: mysqlDate)
                                                        self.communityNews.append(News(title: value["title"].stringValue, body: value["body"].stringValue, date: date, img: value["cover_image"].stringValue, source: value["author"].stringValue))
                                                    } else {
                                                        self.communityNews.append(News(title: value["title"].stringValue, body: value["body"].stringValue, date: value["date_created"].stringValue, img: value["cover_image"].stringValue, source: value["author"].stringValue))
                                                    }
                                                }
                                            }
                                            print("Update Event Success")
                                            self.currentCommunityNews = self.communityNews
                                            self.updateCommunityNews()
                                        } else {
                                            print("Invalid Event response")
                                        }
                                    }
        },
                                failure: { (error) in
                                    print("Update Events Error")
                                    print(error)
                                    
        })
        
    }
}

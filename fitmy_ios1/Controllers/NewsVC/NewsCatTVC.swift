//
//  NewsTVC.swift
//  Fit MY
//
//  Created by Mahad on 3/10/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class NewsCatTVC: UITableViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet var catnameLbl: UILabel!
    
    @IBOutlet var newsCollectionView: UICollectionView!
    var newsarray:[NewsModel] = [NewsModel]()
    
    var uiViewController: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        newsCollectionView.delegate = self
        newsCollectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension NewsCatTVC:UICollectionViewDelegateFlowLayout,UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsarray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCVC", for: indexPath) as! NewsCVC
        let data = newsarray[indexPath.row]
        cell.fill(title: data.title, detail: data.news_description, image: data.filename)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: newsCollectionView.frame.size.width, height: newsCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailNewsVC") as! DetailNewsVC
        vc.newsModel = newsarray[indexPath.row]
        self.uiViewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//
//  NewsCVC.swift
//  Fit MY
//
//  Created by Mahad on 3/10/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class NewsCVC: UICollectionViewCell {
 
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var descLbl: UILabel!
    
    
    func fill(title: String, detail: String, image: String) {
        self.titleLbl.text = title
        self.descLbl.text = detail
        if image.isEmpty {
            self.imageView.image = nil
        } else {
            self.imageView.kf.setImage(with: URL(string: image))
        }
    }
}

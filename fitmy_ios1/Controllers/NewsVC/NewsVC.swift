//
//  NewsVC.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class NewsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var newsCatModelarray = [NewsCatModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNews()
    }
    
     @IBAction func menuBtn_Click(_ sender: Any) {
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            var frm = initialViewController.view.frame
            frm = self.view.frame
            initialViewController.view.frame = frm
            
            initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
    //        self.view.addSubview(initialViewController.view)
            initialViewController.viewController = self
            self.present(initialViewController, animated: false, completion: nil)
    //        self.navigationController?.pushViewController(initialViewController, animated: false)
        }
    
    func getNews() {
        let header:[String:String] = ["Accept":"application/json"]
        let url = "http://172.104.217.178/fitmy/public/api/get_news"
        AFWrapper.requestPOSTURL(url, params: nil, headers: header, success: { (Response) in
            let responseData = Response.dictionaryValue
            if(responseData["status"] == 200)
            {
                let data = responseData["data"]?.arrayValue
                
                var newsCatModel:NewsCatModel
                
                for obj in data ?? []
                {
                    newsCatModel = NewsCatModel()
                    newsCatModel.id = obj["id"].intValue
                    newsCatModel.name = obj["name"].stringValue
                    newsCatModel.filename = obj["filename"].stringValue
                    
                    let newsarray = obj["news"].arrayValue
                    var newsModel:NewsModel
                    for obj in newsarray
                    {
                        newsModel = NewsModel()
                        newsModel.id = obj["id"].intValue
                        newsModel.news_description = obj["description"].stringValue
                        newsModel.title = obj["title"].stringValue
                        newsModel.filename = obj["filename"].stringValue
                        newsModel.news_category_id = obj["news_category_id"].intValue
                        newsCatModel.news.append(newsModel)
                    }
                    self.newsCatModelarray.append(newsCatModel)
                }
                self.tableView.reloadData()
                
            }
            else
            {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: responseData["message"]?.stringValue ?? "Failed to Get News!", Sender: self)
            }
            
            
        }) { (Error) in
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: Error.localizedDescription, Sender: self)
        }
    }
    
    
}
extension NewsVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsCatModelarray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCatTVC", for: indexPath) as! NewsCatTVC
        let data = newsCatModelarray[indexPath.row]
        
        cell.catnameLbl.text = data.name
        cell.newsarray = data.news
        cell.uiViewController = self
        cell.newsCollectionView.reloadData()
        cell.bgView.layer.cornerRadius = 8
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    
}

//
//  NewsModel.swift
//  Fit MY
//
//  Created by Mahad on 3/10/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class NewsModel: NSObject {

    var id:Int = 0
    var title:String = ""
    var news_description:String = ""
    var filename:String = ""
    var news_category_id:Int = 0
}

class NewsCatModel: NSObject {

    var id:Int = 0
    var name:String = ""
    var filename:String = ""
    var news:[NewsModel] = [NewsModel]()
}

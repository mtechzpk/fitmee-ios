//
//  ExerciseTwoController.swift
//  Fit MY
//
//  Created by Azlan Shah on 14/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift
import CoreMotion
import HealthKit
import Localize_Swift

class ExerciseTwoController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var overlayView: UIViewX!
    @IBOutlet weak var menuBtnView: UIViewX!
    @IBOutlet weak var settingBtnView: UIViewX!
    @IBOutlet weak var statsStackView: UIStackView!
    @IBOutlet weak var accuracyView: UIStackView!
    @IBOutlet weak var PreActivityView: UIView!
    @IBOutlet weak var preActivityGPSStackView: UIStackView!
    @IBOutlet weak var preActivityTitle: UILabel!
    @IBOutlet weak var preActivityMethod: UILabel!
    @IBOutlet weak var preActivityImg: UIImageView!
    @IBOutlet weak var preActivityMessage: UILabel!
    @IBOutlet weak var preActivityGPS: UILabel!
    @IBOutlet weak var runningView: UIView!
    @IBOutlet weak var startBtnView: UIStackView!
    @IBOutlet weak var startBtnWidth: NSLayoutConstraint!
    
    @IBOutlet var saveActivityView: UIViewX!
    
    @IBOutlet weak var stopBtn: UIButton!
    @IBOutlet weak var stopBtnWidth: NSLayoutConstraint!
    
    @IBOutlet var saveActivityBtn: UIButton!
    
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var statusBarHeight: NSLayoutConstraint!
    @IBOutlet weak var statusBarLbl: UILabel!
    
    @IBOutlet weak var menuStackView: UIStackView!
    @IBOutlet weak var activityScrollView: UIView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityStackViewChildView: UIView!
    @IBOutlet weak var currentActivityImg: UIImageView!
    
    @IBOutlet weak var paceLbl: UILabel!
    @IBOutlet weak var bpmLbl: UILabel!
    @IBOutlet weak var speedLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceUnitLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var stepCountLbl: UILabel!
    @IBOutlet weak var caloriBurnLbl: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var gpsImg1: UIImageViewX!
    @IBOutlet weak var gpsImg2: UIImageViewX!
    @IBOutlet weak var gpsImg3: UIImageViewX!
    
    var healthStore = HKHealthStore()
    
    var allActivity = [Activity]()
    
    var locationManager: CLLocationManager!
    var locations: Results<Location>!
    var token: NotificationToken!
    var isUpdating = false
    
    var totalCalori = 0.0
    var counter = 0.0 //total activity time
    var actualCounter = 0.0 //actual activity time
    var distanceMain = 0.0
    var stepMain = 0
    var avgSpeed = Double()
    var timer = Timer()
    var restTimer = Timer()
    var isPlaying = false
    var currentStatus = "stop"
    var autoPauseStatus = false
    var startTime = Date()
    var endTime = Date()
    
    var threeSkip = 3
    var currentExercise = [CurrentExercise]()
    var lastLocation = Location()
    
    var maplocations:[CLLocation] = []
    
    
    var currentActivity = 0
    
    let pedometer = CMPedometer()
    
    var profile = Profile()
    
    var lockLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateProfileDetail()
        
        //        clearButtonDidTap(UIButton())
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        self.locationManager.distanceFilter = 6
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.activityType = .fitness
        self.locationManager.showsBackgroundLocationIndicator = true
        self.locationManager.pausesLocationUpdatesAutomatically = false
        self.locationManager.startUpdatingLocation()
        self.statusBarView.updateConstraintsIfNeeded()
        self.statusBarLbl.text = "Locking GPS Location"
        UIView.animate(withDuration: 0.3) {
            self.statusBarHeight.constant = 32
        }
        //        print("Current User Location: ", self.locationManager.requestLocation())
        
        self.activityScrollView.layoutIfNeeded()
        self.activityStackView.layoutIfNeeded()
        overlayView.alpha = 0.0
        menuStackView.alpha = 0.0
        activityScrollView.alpha = 0.0
        menuBtnView.alpha = 0.0
        statsStackView.alpha = 0.0
        PreActivityView.alpha = 0.0
        runningView.alpha = 0.0
        //        settingBtnView.alpha = 0.0
        
        //dynamically add type of activity
        addActivity()
        
        accuracyView.alpha = 0
        
        startBtnView.updateConstraints()
        stopBtnWidth.constant = 0
        
        saveActivityView.isHidden = true
        
        self.locations = self.loadStoredLocations()
        
        if let current = UserDefaults.standard.value(forKey: "current_status") as? String, current == "running" || current == "pause" {
            if let data = UserDefaults.standard.object(forKey: "current_activity") as? Data {
                if let exercise = try? JSONDecoder().decode([CurrentExercise].self, from: data) {
                    self.currentExercise = exercise
                    print(exercise)
                    if currentExercise.count > 0 {
                        currentStatus = "pause"
                        
                        DispatchQueue.main.async {
                            self.startBtnView.updateConstraints()
                            self.stopBtnWidth.constant = 120
                            
                            self.runningView.alpha = 1.0
                        
                            self.startTime =  self.currentExercise.last!.startTime
                            self.endTime =  self.currentExercise.last!.createdAt
                            
                            //check if missing gap more than 10 minute
                            let components = Calendar.current.dateComponents([.minute], from:  self.endTime, to: Date())
                            let fullDuration = Calendar.current.dateComponents([.second], from:  self.startTime, to: Date())
                            let oldDuration = Calendar.current.dateComponents([.second], from:  self.startTime, to:  self.endTime)
                             self.counter = Double(fullDuration.second!)
                             self.actualCounter = Double(oldDuration.second!)
                            if components.minute! < 10 {
                                 self.startButton.setTitle("RESUME", for: .normal)
                            } else {
                                 self.startBtnView.updateConstraints()
                                 self.startBtnWidth.constant = 0
                                 self.stopBtn.setTitle("SAVE", for: .normal)
                            }
                            //                        speedLbl.text = "\(currentExercise.last!.speed)"
                             self.currentActivity =  self.currentExercise.first!.activityType
                             self.currentActivityImg.image =  self.allActivity[ self.currentActivity].img
                            self.stepMain =  self.currentExercise.last!.steps
    //                        self.stepCountLbl.text = String(describing: stepMain)
                            self.distanceMain =  self.currentExercise.last!.distance
                            self.distanceLbl.text =  String(format: "%.02f",  self.distanceMain/1000)
                            let calori =  self.currentExercise.last!.calorie
    //                        self.caloriBurnLbl.text = String(format: "%.01f", calori)
                            self.totalCalori = calori
                            
                            if  self.currentActivity == 2 ||  self.currentActivity == 6 ||  self.currentActivity == 7 {
                            } else {
                                self.locations = self.loadStoredLocations()
                            }
                        }
//                        toggleLocationUpdate()
                    }
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        guard ProcessInfo.processInfo.isLowPowerModeEnabled == false else {
//            showAlert(title: "Low Power Mode Detected", message: "Please disable low power mode to get the best experience while using this app.")
//            return
//        }
        if avgSpeed > 0 {
            self.speedLbl.text = String(format: "%.01f", avgSpeed/Double(self.locations.count))
        }
        if distanceMain > 0 {
            self.distanceLbl.text =  String(format: "%.02f",  self.distanceMain/1000)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()
        if currentActivity == 2 || currentActivity == 6 || currentActivity == 7 { } else {
            if self.currentExercise.count == 0 {
                UserDefaults.standard.set("stop", forKey: "current_status")
            }
        }
    }
    
    @IBAction func dismissPreActivity(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.PreActivityView.alpha = 0.0
        }
    }
    
    @IBAction func hidePreActivity(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.setImage(#imageLiteral(resourceName: "tick"), for: .selected)
        if sender.isSelected {
            UserDefaults.standard.set("hide", forKey: "pre_activity_dialog")
        } else {
            UserDefaults.standard.set("show", forKey: "pre_activity_dialog")
        }
    }
    
    @IBAction func getActivityInfo(_ sender: UIButton) {
        showAlert(title: "\(allActivity[currentActivity].lbl)", message: "\(allActivity[currentActivity].lbl) is one of popular exercise conducted regularly to burn calorie and maintain healthy lifestyle. By performing this activity, you will be rewarded with FitPoint. Collect FitPoint and redeem exiting promotion we offers.")
    }
    
    
    
    @IBAction func toggleGPS(_ sender: Any) {
        if accuracyView.alpha == 0 {
            accuracyView.alpha = 1
        } else {
            accuracyView.alpha = 0
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        if currentStatus == "running" {
            toggleLocationUpdate()
        }
//        self.dismiss(animated: true, completion: nil)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func startButtonDidTap(_ sender: AnyObject) {
        //        clearButtonDidTap(UIButton())
        if currentStatus == "stop" {
            startBtnView.updateConstraints()
            UIView.animate(withDuration: 0.3) {
                self.runningView.alpha = 1.0
                self.stopBtnWidth.constant = 0
            }
            startButton.setTitle("PAUSE", for: .normal)
            startTime = Date()
            //start timer
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateTimer), userInfo: nil, repeats: true)
            print("Timer Started")
            if let showPreActivity = UserDefaults.standard.value(forKey: "pre_activity_dialog") as? String {
                if showPreActivity != "hide" {
                    self.statusBarView.updateConstraints()
                    UIView.animate(withDuration: 0.3) {
                        self.PreActivityView.alpha = 1.0
//                        self.statusBarHeight.constant = 32
                    }
                }
            } else {
                UserDefaults.standard.set("show", forKey: "pre_activity_dialog")
                self.statusBarView.updateConstraints()
                UIView.animate(withDuration: 0.3) {
                    self.PreActivityView.alpha = 1.0
//                    self.statusBarHeight.constant = 32
                }
            }
            self.toggleLocationUpdate()
        } else if currentStatus == "running"{
            currentStatus = "pause"
            startButton.setTitle("RESUME", for: .normal)
            startBtnView.updateConstraints()
            UIView.animate(withDuration: 0.3) {
                self.stopBtnWidth.constant = 120
            }
        } else {
            currentStatus = "running"
            startButton.setTitle("PAUSE", for: .normal)
            startBtnView.updateConstraints()
            UIView.animate(withDuration: 0.3) {
                self.stopBtnWidth.constant = 0
            }
        }
    }
    
    @IBAction func stopButtonDidTap(_ sender: Any) {
        currentStatus = "stop"
        isUpdating = true
        endTime = Date()
        self.toggleLocationUpdate()
    }
    
    
    @IBAction func saveActivityBtnTap(_ sender: Any) {
        SaveActivity()
    }
    
    @IBAction func clearButtonDidTap(_ sender: AnyObject) {
        // Delete old location objects
        self.deleteOldLocations()
        self.deleteAllLocations()
        self.locations = self.loadStoredLocations()
        self.removeAllAnnotations()
        //        self.tableView.reloadData()
        counter = 0.0
        actualCounter = 0.0
        distanceMain = 0.0
        stepMain = 0
        avgSpeed = 0.0
        removeLine()
    }
    
    // Load locations stored in realm at the table view
    fileprivate func loadStoredLocations() -> Results<Location> {
        // Get the default Realm
        let realm = try! Realm()
        
        // Load recent location objects
        return realm.objects(Location.self).sorted(byKeyPath: "createdAt", ascending: false)
    }
    
    @objc func pauseTimer() {
        self.autoPauseStatus = true
        print("user not moving, pause activity timer")
    }
    
    @objc func UpdateTimer() {
        if currentStatus == "running" {
            counter = counter + 1
            if threeSkip >= 5 && !autoPauseStatus {
                actualCounter = actualCounter + 1
            }
        }
        var hour = "00"
        var minute = "00"
        var second = "00"
        var ahour = 0
        var aminute = 0
        var asecond = 0
        if counter > 3600 {
            ahour = Int(counter/3600)
            aminute = Int((counter-Double(3600*ahour))/60)
            asecond = Int(counter-Double(3600*ahour)-Double(60*aminute))
        } else if counter > 60 {
            ahour = 00
            aminute = Int((counter-Double(3600*ahour))/60)
            asecond = Int(counter-Double(3600*ahour)-Double(60*aminute))
        } else {
            asecond = Int(counter)
        }
        if ahour < 10 {
            hour = "0\(ahour)"
        } else {
            hour = "\(ahour)"
        }
        if aminute < 10 {
            minute = "0\(aminute)"
        } else {
            minute = "\(aminute)"
        }
        if asecond < 10 {
            second = "0\(asecond)"
        } else {
            second = "\(asecond)"
        }
        durationLbl.text = "\(hour):\(minute):\(second)"
    }
    // Store object
    fileprivate func addCurrentLocation(_ rowLocation: CLLocation) {
        let location = makeLocation(rawLocation: rowLocation)
        
        // Get the default Realm
        let realm = try! Realm()
        
        // Add to the Realm inside a transaction
        try! realm.write {
            realm.add(location)
        }
    }
    
    // Delete old (-1 day) objects in a background thread
    fileprivate func deleteOldLocations() {
        DispatchQueue.global().async {
            // Get the default Realm
            let realm = try! Realm()
            
            // Old Locations stored in Realm
            let oldLocations = realm.objects(Location.self).filter(NSPredicate(format:"createdAt < %@", NSDate().addingTimeInterval(-86400)))
            
            // Delete an object with a transaction
            try! realm.write {
                realm.delete(oldLocations)
            }
        }
    }
    
    // Delete all location objects from realm
    fileprivate func deleteAllLocations() {
        // Get the default Realm
        let realm = try! Realm()
        
        // Delete all objects from the realm
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    // Make Location object from CLLocation
    fileprivate func makeLocation(rawLocation: CLLocation) -> Location {
        let location = Location()
        location.latitude = rawLocation.coordinate.latitude
        location.longitude = rawLocation.coordinate.longitude
        location.speed = rawLocation.speed
        location.altitude = rawLocation.altitude
        location.createdAt = Date()
        return location
    }
    
    // Drop pin on the map
    fileprivate func dropPin(at location: Location) {
        if location.latitude != 0 && location.longitude != 0 {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude)
            if counter > 1 {
                annotation.title = "Stop"
            } else {
                annotation.title = "Start"
            }
            annotation.subtitle = location.createdAt.description
            self.mapView.addAnnotation(annotation)
        }
    }
    
    // Draw Line on the map
    fileprivate func dropLine() {
        // Load stored location objects
        self.locations = self.loadStoredLocations()
        
        var routeCoordinates = [CLLocationCoordinate2D]()
        
        for location in self.locations {
            routeCoordinates.append(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        }
        
        let routeLine = MKPolyline(coordinates: routeCoordinates, count: routeCoordinates.count)
        //        mapView.setVisibleMapRect(routeLine.boundingMapRect, animated: false)
        mapView.addOverlay(routeLine)
    }
    
    //Remove Line on map
    fileprivate func removeLine() {
        for overlay in mapView.overlays {
            mapView.removeOverlay(overlay)
        }
        //        self.mapView.removeOverlays(self.mapView.overlays)
    }
    
    // Remove all pins on the map
    fileprivate func removeAllAnnotations() {
        let annotations = self.mapView.annotations.filter {
            $0 !== self.mapView.userLocation
        }
        self.mapView.removeAnnotations(annotations)
    }
    
    // Start or Stop location update
    func SaveActivity() {
        saveDistance(distance: distanceMain, start: startTime, end: endTime)
        saveCalori(calori: totalCalori, start: startTime, end: endTime)
        submitExercise()
        
        self.currentExercise.removeAll()
        if let encoded = try? JSONEncoder().encode(self.currentExercise) {
            UserDefaults.standard.set(encoded, forKey: "current_activity")
        }
        
        UIView.animate(withDuration: 0.3) {
            self.menuBtnView.alpha = 0.0
            self.settingBtnView.alpha = 1.0
            self.overlayView.alpha = 1.0
            self.runningView.alpha = 1.0
            self.menuStackView.alpha = 0.0
            self.startBtnView.alpha = 0.0
            self.saveActivityView.alpha = 0.0
        }
        
        
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        var gps_path = ""
        
        if currentActivity == 2 || currentActivity == 6 || currentActivity == 7 { } else {
            for path in locations {
                gps_path.append("\(path.latitude),\(path.longitude),\(dateFormatterMYSQL.string(from: path.createdAt));")
            }
        }
        
        let created_at = dateFormatterMYSQL.string(from: Date())
        let activity_type = allActivity[currentActivity].lbl
        let start_datetime = dateFormatterMYSQL.string(from: startTime)
        let end_datetime = dateFormatterMYSQL.string(from: endTime)
        let calories = String(format: "%.01f", totalCalori)
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Exercise_View") as? ExerciseViewController {
            
            viewController.thisDuration = durationLbl.text ?? ""
            
            viewController.fill(exercise: Exercise(id: "0", steps: "\(stepMain)", startDate: start_datetime, endDate: end_datetime, activity: activity_type, distance: String(format: "%.02f", distanceMain/1000), calorie: calories, path: gps_path))
            present(viewController, animated: true, completion: nil)
        }
        
        deleteOldLocations()
        deleteAllLocations()
    }
    
    fileprivate func toggleLocationUpdate() {
        let realm = try! Realm()
        if self.isUpdating && currentStatus == "stop" {
            // Stop
            self.isUpdating = false
            self.locationManager.stopUpdatingLocation()
            self.startButton.setTitle("START", for: UIControl.State())
            
            // Remove a previously registered notification
            if let token = self.token {
                token.invalidate()
            }
            
            if currentActivity == 2 || currentActivity == 6 || currentActivity == 7 {
            } else {
                // Drop pins at beginning point
                if locations.count > 0 {
                    guard let location = self.locations.first else {
                        print("location empty")
                        return
                    }
                    dropPin(at: location)
//                    dropLine()
                    avgSpeed = avgSpeed/Double(locations.count)
                }
            }
            
            timer.invalidate()
            self.restTimer.invalidate()
            self.autoPauseStatus = false
            pedometer.stopUpdates()
            isPlaying = false
            currentStatus = "stop"
            UserDefaults.standard.set(currentStatus, forKey: "current_status")
            
            speedLbl.text = String(format: "%.01f", avgSpeed)
            print(String(format: "Avg Speed: %.01f km/h", avgSpeed))
            saveActivityView.isHidden = false
            startBtnView.updateConstraints()
            stopBtnWidth.constant = 0
//            SaveActivity()
            
            
        } else {
            //clear
            //            clearButtonDidTap(UIButton())
            
            // Start
            UIView.animate(withDuration: 0.3) {
               self.menuStackView.alpha = 1.0
            }
            if currentActivity == 2 || currentActivity == 6 || currentActivity == 7 {
                if CMPedometer.isStepCountingAvailable() {
                    
                    self.currentExercise.append(CurrentExercise(latitude: 0.0, longitude: 0.0, calorie: 0, distance: 0, timer: 0, steps: 0, speed: 0, altitude: 0, activityType: self.currentActivity, createdAt: Date(), startTime: self.startTime))
                    if let encoded = try? JSONEncoder().encode(self.currentExercise) {
                        UserDefaults.standard.set(encoded, forKey: "current_activity")
                    }
                    
                    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(UpdateTimer), userInfo: nil, repeats: true)
                    endTime = Date()
                    pedometer.startUpdates(from: startTime) { (data, error) in
                        DispatchQueue.main.async {
                            if self.threeSkip < 5 {
                                let img = self.preActivityGPSStackView.subviews[self.threeSkip] as! UIImageViewX
                                img.backgroundColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
                                self.statusBarLbl.text = "Verifying fitness activity.. \(20*self.threeSkip)%"
                                self.threeSkip += 1
                            } else {
                                UIView.animate(withDuration: 0.3, animations: {
                                    self.statusBarHeight.constant = 0
                                })
                            }
                            if let step = data?.numberOfSteps as? Int, let distance = data?.distance as? Double{
                                let distances = distance/1000.0
                                self.stepMain = step
//                                self.stepCountLbl.text = String(describing: step)
                                self.distanceMain = distances
                                self.distanceLbl.text =  String(format: "%.02f", distances)
                                let calori = self.calculateCalorie(duration: self.actualCounter, distance: Double(distances))
//                                self.caloriBurnLbl.text = String(format: "%.01f", calori)
                                self.totalCalori = calori
                                print("Step: ", step, "Distance: ", "Duration: ", self.counter, distances, "Calori: ", calori)
                                
                                self.endTime = Date()
                                if let data = UserDefaults.standard.object(forKey: "current_activity") as? Data {
                                    if let exercise = try? JSONDecoder().decode([CurrentExercise].self, from: data), exercise.count > 0 {
                                        self.currentExercise = exercise
                                        //                                        print(exercise)
                                    }
                                }
                                self.currentExercise.append(CurrentExercise(latitude: 0.0, longitude: 0.0, calorie: calori, distance: distance, timer: self.counter, steps: step, speed: 0, altitude: 0, activityType: self.currentActivity, createdAt: Date(), startTime: self.startTime))
                                print("current activity:", self.allActivity[self.currentActivity].lbl)
                                if let encoded = try? JSONEncoder().encode(self.currentExercise) {
                                    UserDefaults.standard.set(encoded, forKey: "current_activity")
                                }
                            }
                        }
                        //                    print("Pedometer Update: ", data)
                    }
                } else {
                    print("Pedometer not available")
                }
            } else {
                if self.currentActivity == 0 || self.currentActivity == 1 || self.currentActivity == 4 || self.currentActivity == 6 {
                    self.restTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.pauseTimer), userInfo: nil, repeats: false)
                }
                if CMPedometer.isStepCountingAvailable() {
                    print("Pedo start time:", startTime)
                    pedometer.startUpdates(from: startTime) { (data, error) in
                        DispatchQueue.main.async {
                            if let step = data?.numberOfSteps as? Int, let distance = data?.distance as? Double{
                                let distances = distance/1000.0
                                self.stepMain = step
                                print("Pedo:\nStep: ", step, "\nDistance: ", distances, "\nDuration: ", self.counter)
                                self.restTimer.invalidate()
                                self.autoPauseStatus = false
                                if self.currentActivity == 0 || self.currentActivity == 1 || self.currentActivity == 4 || self.currentActivity == 6 {
                                    if distance > self.distanceMain {
                                        self.distanceLbl.text =  String(format: "%.02f",  distance/1000)
                                        self.distanceMain = distance
                                    }
                                    self.restTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.pauseTimer), userInfo: nil, repeats: false)
                                }
                            }
                        }
                    }
                }
                self.locationManager.startUpdatingLocation()
            }
            self.isUpdating = true
//            self.startButton.setTitle("PAUSE", for: UIControl.State())
            
            // Add a notification handler for changes
            self.token = realm.observe {
                [weak self] notification, realm in
                //                self?.tableView.reloadData()
            }
            isPlaying = true
            currentStatus = "running"
            UserDefaults.standard.set(currentStatus, forKey: "current_status")
            UIView.animate(withDuration: 0.3) {
                self.menuBtnView.alpha = 1.0
                self.settingBtnView.alpha = 0.0
                self.overlayView.alpha = 1.0
                self.statsStackView.alpha = 1.0
            }
        }
    }
    @IBAction func toggleMap(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            if self.overlayView.alpha == 0.0 {
                self.overlayView.alpha = 1.0
                self.runningView.alpha = 1.0
            } else {
                self.overlayView.alpha = 0.0
                self.runningView.alpha = 0.0
            }
        }
    }
    
    func setVisibleMapArea(polyline: MKPolyline, edgeInsets: UIEdgeInsets, animated: Bool = false) {
        mapView.setVisibleMapRect(polyline.boundingMapRect, edgePadding: edgeInsets, animated: animated)
    }
    
    func addActivity() {
        
        allActivity.append(Activity(img: #imageLiteral(resourceName: "relaxing-walk"), btn: 0, lbl: "Walking", MET: 3.5))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "man-sprinting"), btn: 1, lbl: "Running", MET: 11.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "man-climbing-stairs"), btn: 2, lbl: "Stairs Climbing", MET: 8.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "cyclist"), btn: 3, lbl: "Cycling", MET: 6.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "hiking"), btn: 4, lbl: "Hiking", MET: 8.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "swimming-figure"), btn: 5, lbl: "Swimming", MET: 6.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "exercising-silhouette"), btn: 6, lbl: "Aerobic", MET: 7.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "person-running-on-a-treadmill-silhouette-from-side-view"), btn: 7, lbl: "Indoor Running", MET: 10.5))
        
        //        var activityArray = [UIView]()
        let activityView = self.activityStackViewChildView.copyView()
        for subview in activityStackView.subviews {
            subview.removeFromSuperview()
        }
        for activity in self.allActivity {
            let activityView2 = activityView.copyView()
            
            // 0:image, 1:button, 2:label
            let img = activityView2.subviews[0] as! UIImageViewX
            img.image = activity.img
            //            img.cornerRadius = 14
            //            img.borderWidth = 2
            //            img.borderColor = #colorLiteral(red: 0.3703463674, green: 0.3789130151, blue: 0.3831247687, alpha: 1)
            let btn = activityView2.subviews[1] as! UIButton
            btn.tag = activity.btn
            btn.addTarget(self, action: #selector(self.chooseActivity(sender:)), for: .touchUpInside)
            let label = activityView2.subviews[2] as! UILabel
            label.text = activity.lbl
            self.activityStackView.addArrangedSubview(activityView2)
        }
        self.activityScrollView.layoutIfNeeded()
        self.activityStackView.layoutIfNeeded()
    }
    
    @objc func chooseActivity(sender: UIButton) {
        print(sender.tag)
        
        currentActivityImg.image = allActivity[sender.tag].img
        currentActivity = sender.tag
        selectActivity(sender)
        
        // setup pre activity message item
        preActivityImg.image = allActivity[sender.tag].img
        preActivityTitle.text = allActivity[sender.tag].lbl
        if currentActivity == 2 || currentActivity == 6 || currentActivity == 7 {
            preActivityMethod.text = "Pedometer Tracking"
            preActivityGPS.text = "Verifying Movement"
        } else {
            preActivityMethod.text = "GPS Tracking"
            preActivityGPS.text = "Locking GPS Signal"
        }
        preActivityMessage.text = "Start \(allActivity[sender.tag].lbl)! Tracker will automatically start after verify activity is being conduct."
        
        
    }
    
    @IBAction func tapActivity(_ sender: UIButton) {
        chooseActivity(sender: sender)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        if menuStackView.alpha == 0.0 {
            UIView.animate(withDuration: 0.3) {
                self.menuStackView.alpha = 1.0
            }
        } else {
            UIView.animate(withDuration: 0.3) {
                self.menuStackView.alpha = 0.0
            }
        }
    }
    @IBAction func selectActivity(_ sender: Any) {
        if currentStatus != "running" {
            if activityScrollView.alpha == 0.0 {
                UIView.animate(withDuration: 0.3) {
                    self.activityScrollView.alpha = 1.0
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.activityScrollView.alpha = 0.0
                }
            }
        }
    }
}
// MARK: - CLLocationManager delegate
extension ExerciseTwoController: CLLocationManagerDelegate {
    func alertLocationAccess(title: String, message: String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        //        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: "Change Settings".localized(), style: .default) { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)! as URL, options: [:], completionHandler: nil)
        }
        
        //        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func isLocationPermissionGranted() -> Bool
    {
        guard CLLocationManager.locationServicesEnabled() else { return false }
        return [.authorizedAlways].contains(CLLocationManager.authorizationStatus())
    }
    
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .notDetermined {
            manager.requestAlwaysAuthorization()
        } else if status == .authorizedAlways {
            // Center user location on the map
            let span = MKCoordinateSpan.init(latitudeDelta: 0.003, longitudeDelta: 0.003)
            let region = MKCoordinateRegion.init(center: self.mapView.userLocation.coordinate, span: span)
            self.mapView.setRegion(region, animated:true)
            self.mapView.userTrackingMode = MKUserTrackingMode.followWithHeading
        } else {
            alertLocationAccess(title: "Permission Required", message: "This app require \"Always\" location permission.")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error LocationManager:", error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        
        
        guard let newLocation = locations.last else {
            return
        }
        
        if !CLLocationCoordinate2DIsValid(newLocation.coordinate) {
            return
        }
        
        if UIApplication.shared.applicationState == .background {
            print("Background Mode:", newLocation)
        }
        #if targetEnvironment(simulator)
        // Ignore Filter for Simulator
        #else
        // Check GPS Source and Accuracy for Actual Device
        if newLocation.verticalAccuracy <= 0 || newLocation.horizontalAccuracy <= 0 {
            print("Vertical Accuracy: %, Horizontal Accuracy: %", newLocation.verticalAccuracy, newLocation.horizontalAccuracy)
            return
        }

        if (newLocation.speed*3.6) > 40 {
            print("too fast yo.")
            return
        }

        if (newLocation.horizontalAccuracy < 0)
        {
            // No Signal
            return
        }
        else if (newLocation.horizontalAccuracy > 163)
        {
            // Poor Signal
            return
        }
        else if (newLocation.horizontalAccuracy > 48)
        {
            // Average Signal
            return
        }
        else
        {
            // Full Signal
        }
        #endif
        print("Vertical Accuracy:",newLocation.verticalAccuracy,"Horizontal Accuracy:", newLocation.horizontalAccuracy)
        
        let va = accuracyView.subviews[0] as! UILabel
        va.text = "H.A: \(newLocation.verticalAccuracy)"
        let ha = accuracyView.subviews[1] as! UILabel
        ha.text = "V.A: \(newLocation.horizontalAccuracy)"
        
//        let span = MKCoordinateSpan.init(latitudeDelta: 0.003, longitudeDelta: 0.003)
//        let region = MKCoordinateRegion.init(center: self.mapView.userLocation.coordinate, span: span)
//        self.mapView.setRegion(region, animated:true)
        self.mapView.setCenter(self.mapView.userLocation.coordinate, animated: true)
        
        //lock user starting location
        if !lockLocation {
            lockLocation = true
            DispatchQueue.main.async {
                self.statusBarView.updateConstraintsIfNeeded()
                self.statusBarView.backgroundColor = .green
                self.statusBarLbl.text = "GPS Location Locked"
                UIView.animate(withDuration: 0.3, delay: 3, options: .curveLinear, animations: {
                    self.statusBarHeight.constant = 0
                }, completion: nil)
            }
            self.locationManager.stopUpdatingLocation()
            return
        }
        else
        {
            maplocations.append(newLocation)
        }
        
        //skip store location when paused
        if currentStatus == "pause" {
            threeSkip = 3
        }
        
        if threeSkip >= 5 && currentStatus == "running" {
            
            NotificationCenter.default.post(name: Notification.Name("notifySenpai"), object: nil, userInfo: nil)
            endTime = Date()
            if self.locations.count == 1 {
                actualCounter = 0
//                UIView.animate(withDuration: 0.3, animations: {
//                    self.statusBarHeight.constant = 0
//                })
            }
            print("Vertical Accuracy:",newLocation.verticalAccuracy,"Horizontal Accuracy:", newLocation.horizontalAccuracy)
            if self.locations.count > 0 {
                lastLocation = self.locations.last!
                let first = CLLocation(latitude: self.locations.first!.latitude, longitude: self.locations.first!.longitude)
                distanceMain += newLocation.distance(from: first)
            } else {
                lastLocation = makeLocation(rawLocation: newLocation)
                distanceMain = 0.0
            }
            
            self.addCurrentLocation(newLocation)
            
            // Drop pins at beginning point
            if self.locations.count == 1 {
                if let location = self.locations.last {
                    dropPin(at: location)
                    
                }
            }
            
            let kmh = locations.last!.speed * 3.6
            avgSpeed += Double(kmh)
            speedLbl.text = String(format: "%.01f", avgSpeed/Double(self.locations.count))
            print(String(format: "Current Speed: %.01f km/h", kmh))
            print(String(format: "Average Speed: %.01f km/h", avgSpeed/Double(self.locations.count)))
            
            //        let first = CLLocation(latitude: self.locations.last!.latitude, longitude: self.locations.last!.longitude)
            //        var distance: CLLocationDistance = newLocation.distance(from: first)
            
            var distance = distanceMain
            //        if distance > 999 {
            print("Distance:", distance/1000.0)
            distanceLbl.text = String(format: "%.02f", distance/1000.0)
//            distanceUnitLbl.text = "km"
            //        } else {
            //            print(String(format: "%f meter", distance))
            //            distanceLbl.text = String(format: "%.0f", distance)
            //            distanceUnitLbl.text = "meter"
            //        }
            
//            let stepCount = Int(distance/1.3123359580052)
//            stepMain = stepCount
//            stepCountLbl.text = "\(stepCount)"
            
            let paceCount = ((actualCounter/60)/(distance/1000.0))
            let paceCount2 = ((actualCounter/60).truncatingRemainder(dividingBy: (distance/1000.0)))
//            paceLbl.text = String(format: "%.0f\"%\'", paceCount, paceCount2)
            
            print("Floor: ", newLocation.floor)
            print("Altitude: ", newLocation.altitude)
            
            let caloriBurn = calculateCalorie(duration: actualCounter, distance: distance)
//            caloriBurnLbl.text = String(format: "%.01f", caloriBurn)
            self.totalCalori = caloriBurn
            print("Calori Burn: ", caloriBurn)
            
            if (locations.last!.horizontalAccuracy < 0)
            {
                // No Signal
                gpsImg1.backgroundColor = .red
                gpsImg2.backgroundColor = .red
                gpsImg3.backgroundColor = .red
                print("No GPS Signal")
            }
            else if (locations.last!.horizontalAccuracy > 163)
            {
                // Poor Signal
                gpsImg1.backgroundColor = .green
                gpsImg2.backgroundColor = .yellow
                gpsImg3.backgroundColor = .yellow
                print("No GPS Signal")
            }
            else if (locations.last!.horizontalAccuracy > 48)
            {
                // Average Signal
                gpsImg1.backgroundColor = .green
                gpsImg2.backgroundColor = .green
                gpsImg3.backgroundColor = .yellow
                print("Average GPS Signal")
            }
            else
            {
                // Full Signal
                gpsImg1.backgroundColor = .green
                gpsImg2.backgroundColor = .green
                gpsImg3.backgroundColor = .green
                print("Full GPS Signal")
            }
            
            if let data = UserDefaults.standard.object(forKey: "current_activity") as? Data {
                if let exercise = try? JSONDecoder().decode([CurrentExercise].self, from: data) {
                    self.currentExercise = exercise
                    //                    print(exercise)
                }
            }
            self.currentExercise.append(CurrentExercise(latitude: newLocation.coordinate.latitude, longitude:newLocation.coordinate.longitude, calorie: caloriBurn, distance: distance, timer: self.counter, steps: stepMain, speed: kmh, altitude: newLocation.altitude, activityType: self.currentActivity, createdAt: Date(), startTime: startTime))
            if let encoded = try? JSONEncoder().encode(self.currentExercise) {
                UserDefaults.standard.set(encoded, forKey: "current_activity")
                NotificationCenter.default.post(name: Notification.Name("notifySenpai2"), object: nil, userInfo: nil)
            }
        } else {
            let img = preActivityGPSStackView.subviews[threeSkip] as! UIImageViewX
            img.backgroundColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
            self.statusBarLbl.text = "Verifying fitness activity.. \(20*self.threeSkip)%"
            threeSkip += 1
            print("Skipping", threeSkip, "Location")
        }
        
    }
    
    func updateProfileDetail() {
        if let data = UserDefaults.standard.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profile = try? decoder.decode(Profile.self, from: data) {
                self.profile = profile
                print(profile)
            }
        }
    }
    
    func calculateCalorie(duration: Double, distance: Double) -> Double {
        let met = allActivity[currentActivity].MET
        let weight = profile.weight_kg
        return (met*4)*weight*(duration/3600)
    }
}

// MARK: - MKMapView delegate
extension ExerciseTwoController: MKMapViewDelegate {
    
    //    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    //        if annotation is MKUserLocation {
    //            return nil
    //        }
    //
    //        let reuseId = "annotationIdentifier"
    //
    //        var pinView = self.mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
    //        if pinView == nil {
    //            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
    //            pinView?.canShowCallout = true
    //            pinView?.animatesDrop = true
    //        }
    //        else {
    //            pinView?.annotation = annotation
    //        }
    //
    //        return pinView
    //    }
    
    //    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    //
    //        let annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "annotationIdentifier")
    //
    ////        if currentStatus == "running" {
    ////            annotationView.markerTintColor = .red
    ////            print("red")
    ////        } else {
    ////            annotationView.glyphTintColor = .green
    ////            print("green")
    ////        }
    //
    //
    //        return annotationView
    //    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polyline = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: polyline)
            polylineRenderer.strokeColor = .orange
            polylineRenderer.lineWidth = 2
            setVisibleMapArea(polyline: polyline, edgeInsets: UIEdgeInsets(top: 80.0, left: 50.0, bottom: 120.0, right: 50.0))
            return polylineRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}

extension ExerciseTwoController {
    func saveDistance(distance: Double, start: Date, end: Date) {
        
        // Set the quantity type to the running/walking distance.
        let type = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        // Set the unit of measurement to miles.
        let distanceQuantity = HKQuantity(unit: .meter(), doubleValue: distance)
        
        // Set the official Quantity Sample.
        let distances = HKQuantitySample(type: type, quantity: distanceQuantity, start: start, end: end)
        
        // Save the distance quantity sample to the HealthKit Store.
        healthStore.save(distances, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print(error)
            } else {
                print("The distance has been recorded! Better go check!")
            }
        })
    }
    func saveCalori(calori: Double, start: Date, end: Date) {
        
        // Set the quantity type to the running/walking distance.
        let type = HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned)!
        
        // Set the unit of measurement to miles.
        let quantity = HKQuantity(unit: .kilocalorie(), doubleValue: calori)
        
        // Set the official Quantity Sample.
        let distances = HKQuantitySample(type: type, quantity: quantity, start: start, end: end)
        
        // Save the distance quantity sample to the HealthKit Store.
        healthStore.save(distances, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print(error)
            } else {
                print("The distance has been recorded! Better go check!")
            }
        })
    }
    func submitExercise() {
        let url = "http://172.104.217.178/fitmy/public/api/activity"
        var header = [String:String]()
        var body = [String:Any]()
        
//        if let token = UserDefaults.standard.value(forKey: "api_token") {
//            header["Authorization"] = "Bearer \(token)"
//        }
        header["Accept"] = "application/json"
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterSwift = DateFormatter()
        dateFormatterSwift.dateStyle = .short
        dateFormatterSwift.timeStyle = .short
        
        //body["created_at"] = dateFormatterMYSQL.string(from: Date())
        body["activity_type"] = allActivity[currentActivity].lbl
        body["start_date"] = dateFormatterMYSQL.string(from: startTime)
        body["end_date"] = dateFormatterMYSQL.string(from: endTime)
        body["kcal"] = String(describing: totalCalori)
        body["timer"] = durationLbl.text!
        //        body["calories"] = "100000"
        var gps_path = ""
        
        if currentActivity == 2 || currentActivity == 6 || currentActivity == 7 { } else {
            for path in locations {
                gps_path.append("\(path.latitude),\(path.longitude),\(dateFormatterMYSQL.string(from: path.createdAt));")
            }
        }
        
        if(maplocations.count > 0)
        {
            if let startlocation = maplocations.first
            {
                body["start_latitude"] = String(describing: startlocation.coordinate.latitude)
                body["start_longitude"] = String(describing: startlocation.coordinate.longitude)
            }
            
            if let endlocation = maplocations.last
            {
                body["end_latitude"] = String(describing: endlocation.coordinate.latitude)
                body["end_longitude"] = String(describing: endlocation.coordinate.longitude)
            }
        }
        
        
        
        
//        body["gps_paths"] = gps_path
        body["steps"] = "\(stepMain)"
        body["distance"] = distanceLbl.text
        body["user_id"] = userModel.id//UserDefaults.standard.string(forKey: "user_id")   //UserDefaults.standard.set(user["id"], forKey: "user_id")
        print(body)
        
        
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        print("Activity Record Response:", JSON)
                                    }
                                    
        },
                                 failure: { (error) in
                                    print("Activity Record Error:", error)
                                    
        })
        
    }
}

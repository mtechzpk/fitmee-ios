//
//  ComplexController.swift
//  Fit MY
//
//  Created by Azlan Shah on 05/12/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import WebKit

class ComplexController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var bookingTypeField: UITextField!
    @IBOutlet weak var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.alpha = 0
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false
        
        let url = URL(string: "http://internal.mysysdemo.com/backend/web/index.php?r=api/tempahan-kemudahan/calendar")!
        webView.load(URLRequest(url: url))
//        view = webView
    }
    

    @IBAction func changeRate(_ sender: Any) {
        if bookingTypeField.text == "Hourly" {
            bookingTypeField.text = "Daily"
        } else {
            bookingTypeField.text = "Hourly"
        }
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.alpha = 1
    }
}

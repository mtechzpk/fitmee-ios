//
//  EventDetalController.swift
//  Fit MY
//
//  Created by Azlan Shah on 25/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class EventDetailController: UIViewController {

    @IBOutlet weak var promoImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var termsLbl: UILabel!
    @IBOutlet weak var rewardImg: UIImageView!
    
    var thisImg = String()
    var thisTitle = String()
    var thisLocation = String()
    var thisDuration = String()
    var thisDetail = String()
    var thisTerms = String()
    var thisID = String()
    var thisRewardImg = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termsLbl.alpha = 0.0
        // Do any additional setup after loading the view.
        fill()
    }
    
    func fill() {
        promoImg.kf.setImage(with: URL(string: thisImg), options: [.transition(.fade(0.3))])
        titleLbl.text = thisTitle
        locationLbl.text = thisLocation
        detailLbl.text = thisDetail
        rewardImg.kf.setImage(with: URL(string: thisRewardImg), options: [.transition(.fade(0.3))])
        //        termsLbl.text = thisTerms
    }
    @objc func dismisscurrentView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func registerEvent(_ sender: Any) {
        let url = "https://api.fitmyapp.asia/api/event/interest/post_interest"
        var header = [String:String]()
        var body = [String:String]()
        body["item_id"] = thisID
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            //alert
                                            let alertController = UIAlertController(title: "Success", message: "Register Successfully!", preferredStyle: .alert)
                                            let doneAction = UIAlertAction(title: "Done", style: .default) { (action) in
                                                //            sender.resetStateWithAnimation(false)
                                            }
                                            alertController.addAction(doneAction)
                                            self.present(alertController, animated: true, completion: nil)
                                        }
                                        print("redeem data:", data)
                                    }
        },
                                 failure: { (error) in
                                    print(error)
                                    
        })
    }
}

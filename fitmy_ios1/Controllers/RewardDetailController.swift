//
//  RewardDetailController.swift
//  Fit MY
//
//  Created by Azlan Shah on 24/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import MTSlideToOpen
import EFQRCode

class RewardDetailController: UIViewController {

    @IBOutlet weak var sliderView: UIViewX!
    @IBOutlet weak var promoImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var termsLbl: UILabel!
    @IBOutlet weak var qrView: UIView!
    @IBOutlet weak var qrImage: UIImageView!
    
    var thisImg = String()
    var thisTitle = String()
    var thisDuration = String()
    var thisDetail = String()
    var thisTerms = String()
    var thisID = String()
    var thisPoint = Double()
    var thisHistory = false
    
    var redeem = Redeem()
    
    var profile = Profile()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        qrView.alpha = 0.0
        
        fill()
        
        sliderView.layoutIfNeeded()
        
        var slideToOpen: MTSlideToOpenView = {
            let slide = MTSlideToOpenView(frame: CGRect(x: 0, y: 0, width: sliderView.frame.width, height: 60))
            slide.sliderViewTopDistance = 0
            slide.sliderCornerRadius = 30
            slide.showSliderText = true
            slide.defaultThumbnailColor = .white
            slide.defaultSlidingColor = .orange
            slide.defaultSliderBackgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9490196078, blue: 0.9647058824, alpha: 1)
            slide.delegate = self
            slide.thumnailImageView.image = #imageLiteral(resourceName: "fitpoint")
            slide.defaultLabelText = "Slide to Redeem"
            return slide
        }()
        
        sliderView.addSubview(slideToOpen)
        
        updateProfileDetail()
        
        if thisHistory {
            sliderView.alpha = 0
            if let tryImage = EFQRCode.generate(
                content: "https://api.fitmyapp.asia/api/reward/redeem/validate_code?redeem_id=\(thisID)",
                watermark: UIImage(named: "WWF")?.cgImage()
                ) {
                self.qrImage.image = UIImage(cgImage: tryImage)
                print("Create QRCode image success: \(tryImage)")
            } else {
                print("Create QRCode image failed!")
            }
            UIView.animate(withDuration: 0.3) {
                self.qrView.alpha = 1
            }
        }
        
    }

    func fill() {
        promoImg.kf.setImage(with: URL(string: thisImg), options: [.transition(.fade(0.3))])
        titleLbl.text = thisTitle
//        durationLbl.text = thisDuration
        detailLbl.text = thisDetail
//        termsLbl.text = thisTerms
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateProfileDetail() {
        if let data = UserDefaults.standard.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profile = try? decoder.decode(Profile.self, from: data) {
                self.profile = profile
                print(profile)
            }
        }
    }
}


extension RewardDetailController: MTSlideToOpenDelegate {
    // MARK: MTSlideToOpenDelegate
    func mtSlideToOpenDelegateDidFinish(_ sender: MTSlideToOpenView) {
        sender.isEnabled = false
        if (profile.total_points - profile.used_points) > Int(thisPoint) {
            redeemPromotion()
            sender.defaultLabelText = "Successfully Redeem"
        } else {
            sender.defaultLabelText = "Redeem Fail"
            //alert
            let alertController = UIAlertController(title: "Fail", message: "Insufficient Fit Point", preferredStyle: .alert)
            let doneAction = UIAlertAction(title: "Done", style: .default) { (action) in
                //            sender.resetStateWithAnimation(false)
            }
            alertController.addAction(doneAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
}


extension RewardDetailController {
    func redeemPromotion() {
        let url = "https://api.fitmyapp.asia/api/reward/redeem/request_code"
        var header = [String:String]()
        var body = [String:String]()
        body["item_id"] = thisID
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        AFWrapper.requestPOSTURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            self.redeem = Redeem(id: data["id"]!.stringValue, item_id: data["item_id"]!.stringValue, user_id: data["user_id"]!.stringValue, code: data["code"]!.stringValue, date_used: data["date_used"]!.stringValue, date_created: data["date_created"]!.stringValue)
                                            
                                            
                                            if let tryImage = EFQRCode.generate(
                                                content: "https://api.fitmyapp.asia/api/reward/redeem/validate_code?redeem_id=\(data["id"]!.stringValue)",
                                                watermark: UIImage(named: "WWF")?.cgImage()
                                                ) {
                                                self.qrImage.image = UIImage(cgImage: tryImage)
                                                print("Create QRCode image success: \(tryImage)")
                                            } else {
                                                print("Create QRCode image failed!")
                                            }
                                            UIView.animate(withDuration: 0.3) {
                                                self.qrView.alpha = 1
                                            }
                                            
                                            //alert
                                            let alertController = UIAlertController(title: "Success", message: "Redeem Successfully!", preferredStyle: .alert)
                                            let doneAction = UIAlertAction(title: "Done", style: .default) { (action) in
                                                //            sender.resetStateWithAnimation(false)
                                            }
                                            alertController.addAction(doneAction)
                                            self.present(alertController, animated: true, completion: nil)
                                        }
                                        print("redeem data:", data)
                                    }
        },
                                failure: { (error) in
                                    print(error)
                                    
        })
    }
}

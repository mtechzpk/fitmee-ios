//
//  RegisterController.swift
//  Fit MY
//
//  Created by Azlan Shah on 26/09/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import DTRuler
import SwiftyJSON
import NVActivityIndicatorView


class RegisterController: UIViewController, NVActivityIndicatorViewable {

    var fullName = ""
    var nickName = ""
    var height = 0.0
    var weight = 0.0
    var gender = "M"
    
    @IBOutlet weak var physicalLbl: UILabel!
    @IBOutlet weak var yearBornLbl: UILabel!
    @IBOutlet weak var heightLbl2: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var weightLbl2: UILabel!
    
    func localizeLabel() {
        physicalLbl.text = "PHYSICAL".localized()
        yearBornLbl.text = "YEAR BORN".localized()
        heightLbl2.text = "HEIGHT".localized()
        weightLbl2.text = "WEIGHT".localized()
        genderLbl.text = "GENDER".localized()
        mulaBtn.setTitle("START".localized(), for: .normal)
    }
    
    @IBOutlet weak var indicatorView: NVActivityIndicatorView!
    
    @IBOutlet weak var weightRuler: UIView!
    @IBOutlet weak var heightRuler: UIView!
    @IBOutlet weak var dobRuler: UIView!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var maleView: UIViewX!
    @IBOutlet weak var femaleView: UIViewX!
    @IBOutlet weak var mulaBtn: UIButtonX!
    @IBOutlet weak var profileImg: UIImageView!
    
    let encoder = JSONEncoder()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeLabel()
        // Do any additional setup after loading the view.
        
        print(fullName,nickName)
        
        layoutRulerDOB()
        layoutRulerWeight()
        layoutRulerHeight()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func saveToProfile() {
        UserDefaults.standard.set(weight, forKey: "user_weight")
        UserDefaults.standard.set(height, forKey: "user_height")
        UserDefaults.standard.set("\(fullName)", forKey: "user_full_name")
        UserDefaults.standard.set("\(nickName)", forKey: "user_nick_name")
        UserDefaults.standard.set(gender, forKey: "user_gender")
    }
    
    @IBAction func start(_ sender: Any) {
//        UserDefaults.standard.set(true, forKey: "isAppAlreadyLaunchedOnce")
//        saveToProfile()
        mulaBtn.isEnabled = false
        startAnimating()
        submitRegistration()
    }
    
    fileprivate func layoutRulerDOB() {
        guard let text = dobLbl.text, var scale = Int(text) else {
            return
        }
    
        scale = 1991
        
        dobLbl.text = "\(scale)"
        
        DTRuler.theme = Colorful()
        
        let ruler = DTRuler(scale: .integer(scale), minScale: .integer(1900), maxScale: .integer(2010), width: view.bounds.width - 50)
        ruler.delegate = self
        ruler.tag = 1
        dobRuler.addSubview(ruler)
        ruler.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: ruler, attribute: .top, relatedBy: .equal, toItem: dobRuler, attribute: .top, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: ruler, attribute: .leading, relatedBy: .equal, toItem: dobRuler, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: ruler, attribute: .trailing, relatedBy: .equal, toItem: dobRuler, attribute: .trailing, multiplier: 1, constant: 0)
        
        dobRuler.addConstraints([top, leading, trailing])
    }
    
    fileprivate func layoutRulerWeight() {
        guard let text = dobLbl.text, var scale = Int(text) else {
            return
        }
        weight = 50
        scale = 50
        
        weightLbl.text = "\(scale) cm"
        
        DTRuler.theme = Colorful()
        
        let ruler = DTRuler(scale: .integer(scale), minScale: .integer(7), maxScale: .integer(200), width: view.bounds.width - 50)
        ruler.delegate = self
        ruler.tag = 2
        
        weightRuler.addSubview(ruler)
        
        ruler.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: ruler, attribute: .top, relatedBy: .equal, toItem: weightRuler, attribute: .top, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: ruler, attribute: .leading, relatedBy: .equal, toItem: weightRuler, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: ruler, attribute: .trailing, relatedBy: .equal, toItem: weightRuler, attribute: .trailing, multiplier: 1, constant: 0)
        
        weightRuler.addConstraints([top, leading, trailing])
    }
    
    fileprivate func layoutRulerHeight() {
        guard let text = dobLbl.text, var scale = Int(text) else {
            return
        }
        height = 150
        scale = 150
        
        heightLbl.text = "\(scale) cm"
        
        DTRuler.theme = Colorful()
        
        let ruler = DTRuler(scale: .integer(scale), minScale: .integer(100), maxScale: .integer(220), width: view.bounds.width - 50)
        ruler.delegate = self
        ruler.tag = 3
        
        heightRuler.addSubview(ruler)
        
        ruler.translatesAutoresizingMaskIntoConstraints = false
        
        let top = NSLayoutConstraint(item: ruler, attribute: .top, relatedBy: .equal, toItem: heightRuler, attribute: .top, multiplier: 1, constant: 0)
        let leading = NSLayoutConstraint(item: ruler, attribute: .leading, relatedBy: .equal, toItem: heightRuler, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: ruler, attribute: .trailing, relatedBy: .equal, toItem: heightRuler, attribute: .trailing, multiplier: 1, constant: 0)
        
        heightRuler.addConstraints([top, leading, trailing])
    }
    
    struct Colorful: DTRulerTheme {
        
        var backgroundColor: UIColor {
            return .clear
        }
        
        var pointerColor: UIColor {
            return #colorLiteral(red: 0.9647058824, green: 0.2941176471, blue: 0.1607843137, alpha: 1)
        }
        
        var majorScaleColor: UIColor {
            return #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        }
        
        var minorScaleColor: UIColor {
            return #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        }
        
        var labelColor: UIColor {
            return #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        }
        
    }
    
    @IBAction func chooseGender(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            gender = "M"
            maleView.backgroundColor = #colorLiteral(red: 0, green: 0.2916806638, blue: 0.3971883059, alpha: 1)
            femaleView.backgroundColor = #colorLiteral(red: 0.102768369, green: 0.1401897073, blue: 0.2030574083, alpha: 1)
            profileImg.image = #imageLiteral(resourceName: "man")
        case 1:
            gender = "F"
            maleView.backgroundColor = #colorLiteral(red: 0.102768369, green: 0.1401897073, blue: 0.2030574083, alpha: 1)
            femaleView.backgroundColor = #colorLiteral(red: 0, green: 0.2916806638, blue: 0.3971883059, alpha: 1)
            profileImg.image = #imageLiteral(resourceName: "girl")
        default:
            break
        }
    }
}

extension RegisterController: DTRulerDelegate {
    func didChange(on ruler: DTRuler, withScale scale: DTRuler.Scale) {
        switch ruler.tag {
        case 1:dobLbl.text = scale.minorTextRepresentation()
        case 2: weightLbl.text = "\(scale.minorTextRepresentation()) kg";weight = Double(scale.minorTextRepresentation()) as! Double
        case 3: heightLbl.text = "\(scale.minorTextRepresentation()) cm";height = Double(scale.minorTextRepresentation()) as! Double
        default: break
        }
    }
}

extension RegisterController {
    func submitRegistration() {
        let url = "https://api.fitmyapp.asia/api/user/profile/register_profile"
        let header = [String:String]()
        var body = [String:String]()
        body["full_name"] = fullName
        body["nick_name"] = nickName
        body["birth_year"] = dobLbl.text
        body["weight_kg"] = "\(weight)"
        body["height_cm"] = "\(height)"
        body["gender"] = gender
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            UserDefaults.standard.set(data["phone_num"]!.stringValue, forKey: "phone_num")
                                            UserDefaults.standard.set(data["user_password"]!.stringValue, forKey: "user_password")
                                            print(UserDefaults.standard.value(forKey: "user_password")!)
                                            self.submitLogin()
                                        } else {
                                            print("Invalid response")
                                            
                                            self.stopAnimating()
                                            self.mulaBtn.isEnabled = true
                                        }
                                    }
                                    
        },
                                 failure: { (error) in
                                    print(error)
                                    self.mulaBtn.isEnabled = true
                                    self.stopAnimating()
                                    
        })
        
    }
    func submitLogin() {
        let url = "https://api.fitmyapp.asia/api/user/auth/request_app_password"
        let header = [String:String]()
        var body = [String:String]()
        if let phone_num = UserDefaults.standard.value(forKey: "phone_num") {
            body["phone_num"] = phone_num as! String
        }
        if let user_password = UserDefaults.standard.value(forKey: "user_password") {
            body["user_password"] = user_password as! String
        }
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            UserDefaults.standard.set(data["phone_num"]!.stringValue, forKey: "phone_num")
                                            UserDefaults.standard.set(data["app_password"]!.stringValue, forKey: "app_password")
                                            print(UserDefaults.standard.value(forKey: "app_password")!)
                                            self.updateToken()
                                        } else {
                                            print("Invalid response")
                                            self.stopAnimating()
                                            self.mulaBtn.isEnabled = true
                                        }
                                    }
                                    
        },
                                 failure: { (error) in
                                    print(error)
                                    self.mulaBtn.isEnabled = true
                                    self.stopAnimating()
                                    
        })
        
    }
    
    func updateToken() {
        let url = "https://api.fitmyapp.asia/api/user/auth/request_token"
        let header = [String:String]()
        var body = [String:String]()
        if let phone_num = UserDefaults.standard.value(forKey: "phone_num") {
            body["phone_num"] = phone_num as! String
        }
        if let app_password = UserDefaults.standard.value(forKey: "app_password") {
            body["app_password"] = app_password as! String
        }
        print(body)
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            UserDefaults.standard.set(data["token"]!.stringValue, forKey: "api_token")
                                            let data2 = data["profile"]!.dictionaryValue
                                            let profile = Profile(total_points: data2["total_points"]?.intValue ?? 0,
                                                                  birth_year: data2["birth_year"]?.intValue ?? 0,
                                                                  full_name: data2["full_name"]?.stringValue ?? "",
                                                                  nick_name: data2["nick_name"]?.stringValue ?? "",
                                                                  height_cm: data2["height_cm"]?.doubleValue ?? 0,
                                                                  weight_kg: data2["weight_kg"]?.doubleValue ?? 0,
                                                                  nationality: data2["nationality"]?.stringValue ?? "",
                                                                  race: data2["race"]?.stringValue ?? "",
                                                                  gender: data2["gender"]?.stringValue ?? "",
                                                                  email: data2["email"]?.stringValue ?? "",
                                                                  total_steps: data2["total_steps"]?.intValue ?? 0,
                                                                  birth_date: data2["birth_date"]?.stringValue ?? "",
                                                                  photo: data2["photo"]?.stringValue ?? "",
                                                                  id: data2["id"]?.stringValue ?? "",
                                                                  used_points: data2["used_points"]?.intValue ?? 0,
                                                                  phone_num: data2["phone_num"]?.stringValue ?? "",
                                                                  user_password: UserDefaults.standard.value(forKey: "user_password") as! String,
                                                                  app_password: UserDefaults.standard.value(forKey: "app_password") as! String,
                                                                  date_created: data2["date_created"]?.stringValue ?? "")
                                            print("Registered Profile: ", profile)
                                            if let encoded = try? self.encoder.encode(profile) {
                                                self.defaults.set(encoded, forKey: "user_profile")
                                            }
                                            
                                            UserDefaults.standard.set(true, forKey: "isAppAlreadyLaunchedOnce")
                                            
                                            self.stopAnimating()
                                            self.dismiss(animated: true, completion: nil)
                                        } else {
                                            print("Invalid response")
                                            
                                            self.stopAnimating()
                                            self.mulaBtn.isEnabled = true
                                        }
                                    }
        },
                                 failure: { (error) in
                                    print(error)
                                    self.mulaBtn.isEnabled = true
                                    self.stopAnimating()
                                    
        })
        
    }
    func json(from object:AnyObject) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

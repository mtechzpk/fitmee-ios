//
//  OnboardingController.swift
//  Fit MY
//
//  Created by Azlan Shah on 26/08/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class OnboardingController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(next(sender:)))
        leftSwipe.direction = .left
        view.addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(back(sender:)))
        rightSwipe.direction = .right
        view.addGestureRecognizer(rightSwipe)
    }
    
    @IBAction func back(_ sender: Any) {
        back(sender: UISwipeGestureRecognizer())
    }
    
    @objc func back(sender: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func next(_ sender: Any) {
        next(sender: UISwipeGestureRecognizer())
    }
    
    @objc func next(sender: UISwipeGestureRecognizer) {
        var id = ""
        switch storyboardId {
            case "onboarding_one": id = "onboarding_two"
            case "onboarding_two": id = "onboarding_three"
            case "onboarding_three": id = "onboarding_four"
            case "onboarding_four": id = "onboarding_five"
            case "onboarding_five": id = "login_split"
            default: id = ""
        }
        if id != "" {
            if id == "login_split" {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as? LoginSplitController {
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            } else {
                if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: id) as? OnboardingController {
                    
                    if let navigator = navigationController {
                        navigator.pushViewController(viewController, animated: true)
                    }
                }
            }
        }
    }
}

extension UIViewController {
    var storyboardId: String {
        return (value(forKey: "storyboardIdentifier") as? String)!
    }
}

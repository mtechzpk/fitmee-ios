//
//  FitnessHistoryController.swift
//  Fit MY
//
//  Created by Azlan Shah on 22/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import Charts
import CoreMotion
import HealthKit

struct Exercise {
    var id = String()
    var steps = String()
    var timer = String()
    var startDate = String()
    var endDate = String()
    var activity = String()
    var distance = String()
    var calorie = String()
    
    var start_latitude = String()
    var start_longitude = String()
    var end_latitude = String()
    var end_longitude = String()
    
    var path = String()
    
}

class FitnessHistoryController: UIViewController, ChartViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var chartView: BarChartView!
    weak var axisFormatDelegate: IAxisValueFormatter?
    @IBOutlet weak var activityScrollView: UIView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityStackViewChildView: UIView!
    
    let healthStore = HKHealthStore()
    let pedometer = CMPedometer()
    var days:[String] = []
    var stepsTaken:[Int] = []
    var distanceTaken:[Double] = []
    var caloriTaken:[Double] = []
    var heartTaken:[Int] = []
    var sleepTaken:[Int] = []
    
    var exercise = [Exercise]()
    var currentExercise = [Exercise]()
    
    var currentGraph = 0
    var currentDate = Date().reduceDayFrom(days: 7, date: Date())
    var currentTitle = "Daily Steps"
    var installDate: Date? {
        guard
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last,
            let attributes = try? FileManager.default.attributesOfItem(atPath: documentsURL.path)
            else { return nil }
        return attributes[.creationDate] as? Date
    }
    
    var imageView = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(cellType: RewardCell.self)
        tableView.backgroundColor = .clear
        
        self.title = "Bar Chart"
        chartView.alpha = 0.0
        
//        self.setup(barLineChartView: chartView)
        
        chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        
        currentController()
        getExercise()
    }
    
    
    
    
    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupBar() {
        chartView.maxVisibleCount = 2000
        
        let xAxis = chartView.xAxis
//        xAxis.enabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = days.count
//        xAxis.valueFormatter =
        xAxis.valueFormatter = IndexAxisValueFormatter(values: days)
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
//        leftAxisFormatter.negativeSuffix = " $"
//        leftAxisFormatter.positiveSuffix = " $"
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        rightAxis.labelFont = .systemFont(ofSize: 10)
        rightAxis.labelCount = 8
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 0.15
        rightAxis.axisMinimum = 0
        
        let l = chartView.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4
        //        chartView.legend = l
        
        let marker = XYMarkerView(color: UIColor(white: 180/250, alpha: 1),
                                  font: .systemFont(ofSize: 12),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: chartView.xAxis.valueFormatter!)
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView.marker = marker
        
//        self.setDataCount(Int(3) + 1, range: UInt32(3))
    }
    
    func setup(barLineChartView chartView: BarLineChartViewBase) {
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = false
        
        // ChartYAxis *leftAxis = chartView.leftAxis;
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        
        chartView.rightAxis.enabled = false
    }
    
    func setDataCount() {
        
        var entry = [BarChartDataEntry]()
        switch currentGraph
        {
        case 0:
            for (i, step) in stepsTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 1:
            for (i, step) in caloriTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 2:
            for (i, step) in distanceTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 3:
            for (i, step) in stepsTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 4:
            for (i, step) in stepsTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        default:
            break
        }
        
        let dataSet = BarChartDataSet(entries: entry, label: currentTitle)
        dataSet.colors = ChartColorTemplates.liberty()
        let data = BarChartData(dataSets: [dataSet])
        chartView.data = data
//        chartView.chartDescription?.text = "Step taken for past 7 days"
        
        //All other additions to this function will go here
        let xAxisValue = chartView.xAxis
        xAxisValue.valueFormatter = axisFormatDelegate
        
        //This must stay at end of function
        chartView.notifyDataSetChanged()
    }
    
    func currentController() {
        switch currentGraph
        {
        case 0:
            getStepFrom(duration: currentDate)
            currentTitle = "Daily Steps"
        case 1:
//            print("calori takda")
            getCaloriFrom(duration: currentDate)
            currentTitle = "Daily Calori Burn"
        case 2:
            getDistanceFrom(duration: currentDate)
            currentTitle = "Daily Walking Distance (km)"
        case 3:
            getHeartFrom(duration: currentDate)
            currentTitle = "Daily Average Hearbeat (bpm)"
        case 4:
            getSleepFrom(duration: currentDate)
            currentTitle = "Daily Sleep (hour)"
        default:
            break
        }
    }
    
    @IBAction func updateDuration(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            chartView.xAxis.enabled = true
            currentDate = Date().reduceDayFrom(days: 7, date: Date())
            currentController()
        case 1:
            chartView.xAxis.enabled = false
            currentDate = Date().reduceDayFrom(days: 30, date: Date())
            currentController()
        case 2:
            chartView.xAxis.enabled = false
            currentDate = Date().reduceDayFrom(days: 365, date: Date())
            currentController()
        default:
            break
        }
    }
    
    @IBAction func updateType(_ sender: UIButton) {
        currentGraph = sender.tag
        let btn = sender.superview as! UIViewX
        let v = btn.superview as! UIStackView
        for value in v.arrangedSubviews {
            let a = value as! UIViewX
            a.borderColor = #colorLiteral(red: 0.9467939734, green: 0.9468161464, blue: 0.9468042254, alpha: 1)
        }
        btn.borderColor = .orange
        
        currentController()
    }
    
}

extension FitnessHistoryController: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return days[Int(value)]
    }
}

extension FitnessHistoryController {
    func getStepFrom(duration:Date) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //daily result split
        
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            self.stepsTaken.removeAll()
            
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    let startDate = statistics.startDate
                    let sum = sum.doubleValue(for: HKUnit.count())
                    self.stepsTaken.append(Int(sum))
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "d MMM"
                    
                    self.days.append("\(formatter.string(from: startDate))")
                    resultCount += sum
                    print("\(startDate): steps = \(sum)")
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    self.setDataCount()
                    self.setupBar()
                    UIView.animate(withDuration: 0.3, delay: 0.3  , animations: {
                        self.chartView.alpha = 1.0
                    })
                    print(resultCount)
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    func getCaloriFrom(duration:Date) {
        let type = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //daily result split
        
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            self.caloriTaken.removeAll()
            self.days.removeAll()
            
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    let startDate = statistics.startDate
                    let sum = sum.doubleValue(for: HKUnit.kilocalorie())
                    self.caloriTaken.append(Double(sum))
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "d MMM"
                    
                    self.days.append("\(formatter.string(from: startDate))")
                    resultCount += sum
                    print("\(startDate): calori = \(sum)")
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    self.setDataCount()
                    self.setupBar()
                    UIView.animate(withDuration: 0.3, delay: 0.3  , animations: {
                        self.chartView.alpha = 1.0
                    })
                    print(resultCount)
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    func getDistanceFrom(duration:Date) {
        let type = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //daily result split
        
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            self.distanceTaken.removeAll()
            self.days.removeAll()
            
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    let startDate = statistics.startDate
                    let sum = sum.doubleValue(for: HKUnit.meter())
                    self.distanceTaken.append(Double(sum/1000))
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "d MMM"
                    
                    self.days.append("\(formatter.string(from: startDate))")
                    resultCount += sum
                    print("\(startDate): distance = \(sum)")
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    self.setDataCount()
                    self.setupBar()
                    UIView.animate(withDuration: 0.3, delay: 0.3  , animations: {
                        self.chartView.alpha = 1.0
                    })
                    print(resultCount)
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    @available(iOS 12.0, *)
    func getHeartFrom(duration:Date) {
        let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        
        // replaced options parameter .cumulativeSum with .discreteMostRecent
        let query = HKStatisticsQuery(quantityType: heartRateType, quantitySamplePredicate: predicate, options: .discreteMostRecent) { (_, result, error) in
            var resultCount = 0
            guard let result = result else {
                print("Failed to fetch heart rate")
                print(Double(resultCount))
                return
            }
            
            // More changes here in order to get bpm value
            guard let beatsPerMinute: Double = result.mostRecentQuantity()?.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute())) else { return }
            resultCount = Int(beatsPerMinute)
            
            DispatchQueue.main.async {
                print(Double(resultCount))
            }
        }
        healthStore.execute(query)
    }
    
    func getSleepFrom(duration:Date) {
        
        // first, we define the object type we want
        if let sleepType = HKQuantityType.categoryType(forIdentifier: .sleepAnalysis) {
            
            // Use a sortDescriptor to get the recent data first
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            
            // we create our query with a block completion to execute
            let query = HKSampleQuery(sampleType: sleepType, predicate: nil, limit: 3000, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                
                if error != nil {
                    
                    // something happened
                    return
                    
                }
                
                if let result = tmpResult {
                    
                    // do something with my data
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            let value = (sample.value == HKCategoryValueSleepAnalysis.inBed.rawValue) ? "InBed" : "Asleep"
                            print("Healthkit sleep: \(sample.startDate) \(sample.endDate) - sleep: \(value)")
                            if sample.endDate.compareDate(toDate: result[0].endDate) {
                                print("Same day")
                            } else {
                                print("next day")
                            }
                        }
                    }
                }
            }
            
            // finally, we execute our query
            healthStore.execute(query)
        }
    }
}

extension FitnessHistoryController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentExercise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as RewardCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let mysqlDate = dateFormatterMYSQL.date(from: currentExercise[indexPath.row].startDate)
        
        let dateFormatterSwift = DateFormatter()
        dateFormatterSwift.dateStyle = .short
        dateFormatterSwift.timeStyle = .short
        
        switch currentExercise[indexPath.row].activity{
        case "Walking":
            imageView = #imageLiteral(resourceName: "relaxing-walk")
        case "Running":
            imageView = #imageLiteral(resourceName: "man-sprinting")
        case "Stairs Climbing":
            imageView = #imageLiteral(resourceName: "man-climbing-stairs")
        case "Cycling":
            imageView = #imageLiteral(resourceName: "cyclist")
        case "Hiking":
            imageView = #imageLiteral(resourceName: "hiking")
        case "Swimming":
            imageView = #imageLiteral(resourceName: "swimming-figure")
        case "Aerobic":
            imageView = #imageLiteral(resourceName: "exercising-silhouette")
        case "Indoor Running":
            imageView = #imageLiteral(resourceName: "person-running-on-a-treadmill-silhouette-from-side-view")
        default:
            break
        }
        
        cell.promoImg.contentMode = .center
        
        cell.fill(title: currentExercise[indexPath.row].activity, detail: "\(currentExercise[indexPath.row].steps) Steps", price: "\(currentExercise[indexPath.row].distance) km" , image: "", image2: imageView)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Exercise_View") as? ExerciseViewController {
            viewController.thisDuration = ""
            viewController.fill(exercise: currentExercise[indexPath.row])
            present(viewController, animated: true, completion: nil)
        }
    }
}

extension FitnessHistoryController {
    func getExercise() {
        let url = "https://api.fitmyapp.asia/api/fitness/activity/get_activities"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd HH:mm:ss"
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            for (index, value) in data.enumerated() {
                                                self.exercise.append(Exercise(id: value["id"].stringValue, steps: value["steps"].stringValue, startDate: value["start_datetime"].stringValue, endDate: value["end_datetime"].stringValue, activity: value["activity_type"].stringValue, distance: value["distance"].stringValue,calorie: value["calories"].stringValue, path: value["paths"].stringValue))
                                            }
                                            self.currentExercise = self.exercise
                                            print("Exercise data:", JSON)
                                            self.tableView.reloadData()
                                        }
                                    }
        },
                                failure: { (error) in
                                    print(error)
                                    
        })
        
    }
}

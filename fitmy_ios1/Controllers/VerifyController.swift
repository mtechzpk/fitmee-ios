//
//  VerifyController.swift
//  Fit MY
//
//  Created by Azlan Shah on 12/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import SVPinView

class VerifyController: UIViewController {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var messageLbl: UILabel!
    
    func localizeLabel() {
        backBtn.setTitle("Back".localized(), for: .normal)
        messageLbl.text = "TAC Number has been sent to your phone via SMS. \n Insert the 6 digit TAC Number below".localized()
    }
    
    @IBOutlet weak var pinView: SVPinView!
    
    var phone_num = String()
    var otp = Int()
    var delegate : isAbleToReceiveData?
    
    let encoder = JSONEncoder()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeLabel()
        configurePinView()
        print(otp)
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.delegate?.pass(data: false)
        }
    }
    
    func configurePinView() {
        pinView.pinLength = 6
        pinView.secureCharacter = "\u{25CF}"
        pinView.interSpace = 8
        pinView.textColor = UIColor.white
        pinView.borderLineColor = UIColor.white
        pinView.activeBorderLineColor = UIColor.white
        pinView.borderLineThickness = 1
        pinView.shouldSecureText = false
        pinView.allowsWhitespaces = false
        pinView.style = .none
        pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
        pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
        pinView.fieldCornerRadius = 8
        pinView.activeFieldCornerRadius = 8
        pinView.becomeFirstResponderAtIndex = 0
        
        pinView.font = UIFont.systemFont(ofSize: 15)
        pinView.keyboardType = .phonePad
        pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        
        pinView.didFinishCallback = didFinishEnteringPin(pin:)
        pinView.didChangeCallback = { pin in
            print("The entered pin is \(pin)")
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(false)
    }
    
    @IBAction func printPin() {
        let pin = pinView.getPin()
        guard !pin.isEmpty else {
            showAlert(title: "Error", message: "Pin entry incomplete")
            return
        }
        showAlert(title: "Success", message: "The Pin entered is \(pin)")
    }
    
    @IBAction func clearPin() {
        pinView.clearPin()
    }
    
    @IBAction func pastePin() {
        guard let pin = UIPasteboard.general.string else {
            showAlert(title: "Error", message: "Clipboard is empty")
            return
        }
        pinView.pastePin(pin: pin)
    }
    
    @IBAction func toggleStyle() {
        var nextStyle = pinView.style.rawValue + 1
        if nextStyle == 3 {nextStyle = 0}
        let style = SVPinViewStyle(rawValue: nextStyle)!
        switch style {
        case .none:
            pinView.fieldBackgroundColor = UIColor.white.withAlphaComponent(0.3)
            pinView.activeFieldBackgroundColor = UIColor.white.withAlphaComponent(0.5)
            pinView.fieldCornerRadius = 15
            pinView.activeFieldCornerRadius = 15
            pinView.style = style
        case .box:
            pinView.activeBorderLineThickness = 4
            pinView.fieldBackgroundColor = UIColor.clear
            pinView.activeFieldBackgroundColor = UIColor.clear
            pinView.fieldCornerRadius = 0
            pinView.activeFieldCornerRadius = 0
            pinView.style = style
        case .underline:
            pinView.activeBorderLineThickness = 4
            pinView.fieldBackgroundColor = UIColor.clear
            pinView.activeFieldBackgroundColor = UIColor.clear
            pinView.fieldCornerRadius = 0
            pinView.activeFieldCornerRadius = 0
            pinView.style = style
        }
        clearPin()
    }
    
    func didFinishEnteringPin(pin:String) {
        //        showAlert(title: "Success", message: "The Pin entered is \(pin)")
        if Int(pin) == otp {
            UserDefaults.standard.set(phone_num, forKey: "phone_num")
            UserDefaults.standard.set(otp, forKey: "user_password")
            self.dismiss(animated: true, completion: {
                self.delegate?.pass(data: true)
            })
        } else {
            print("pin length: ", pin.count)
            if pin.count == 6 {
                showAlert(title: "Incorrect TAC", message: "Please re-insert correct TAC")
            }
        }
    }
    
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setGradientBackground(view:UIView, colorTop:UIColor, colorBottom:UIColor) {
        for layer in view.layer.sublayers! {
            if layer.name == "gradientLayer" {
                layer.removeFromSuperlayer()
            }
        }
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop.cgColor, colorBottom.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        gradientLayer.name = "gradientLayer"
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func saveData() {
        
    }
}

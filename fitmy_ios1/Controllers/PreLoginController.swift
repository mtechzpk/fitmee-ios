//
//  PreLoginController.swift
//  Fit MY
//
//  Created by Azlan Shah on 12/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import SafariServices
import NVActivityIndicatorView

class PreLoginController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var contactUsBtn: UIButton!
    @IBOutlet weak var TermsBtn: UIButton!
    @IBOutlet weak var PrivacyBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    func localizeLabel() {
        titleLbl.text = "Welcome Back! \n Please Sign-In to continue".localized()
        phoneNumberLbl.text = "Phone Number".localized()
        contactUsBtn.setTitle("Have a problem? Contact Us".localized(), for: .normal)
        TermsBtn.setTitle("Terms and Condition".localized(), for: .normal)
        PrivacyBtn.setTitle("Privacy Policy".localized(), for: .normal)
        backBtn.setTitle("Back".localized(), for: .normal)
        loginBtn.setTitle(" GO ".localized(), for: .normal)
    }
    
    @IBOutlet weak var phoneField: UITextFieldX!
    @IBOutlet weak var loginBtn: UIButtonX!
    @IBOutlet weak var indicatorView: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizeLabel()
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doLogin(_ sender: Any) {
        if phoneField.text!.count > 0 {
            loginBtn.isEnabled = false
            getOTP()
            startAnimating()
            self.view.endEditing(true)
        } else {
            showAlert(title: "Phone Field Empty", message: "Please fill in phone number to continue")
        }
        
    }
    
    func gotoLogin(otp : Int) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login") as? LoginController {
            viewController.phone_num = phoneField.text!
            viewController.otp = otp
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    @IBAction func contactUs(_ sender: Any) {
        let message = "Help Me on FitMY."
        let appURL = NSURL(string: "whatsapp://send?text=\(message.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)&phone=601766908025")!
        if UIApplication.shared.canOpenURL(appURL as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL as URL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL as URL)
            }
        }
        else {
            showAlert(title: "Whatsapp not installed", message: "Please install whatsapp to communicate with us.")
        }
    }
    
    @IBAction func privacyPolicy(_ sender: Any) {
        let safariVC = SFSafariViewController(url: NSURL(string: "http://www.northparkapp.com/privacypolicy.html")! as URL)
        self.present(safariVC, animated: true, completion: nil)
    }
}


extension PreLoginController {
    func getOTP() {
        let url = "https://api.fitmyapp.asia/api/user/auth/request_tac"
        let header = [String:String]()
        var body = [String:String]()
        if let phone_num = phoneField.text {
            body["phone_num"] = phone_num
        }
        if let user_password = UserDefaults.standard.value(forKey: "user_password") {
            body["user_password"] = ""
        }
        print(body)
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        self.loginBtn.isEnabled = true
                                        self.stopAnimating()
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            print("OTP:", data["user_password"]!.intValue)
                                            self.gotoLogin(otp: data["user_password"]!.intValue)
                                        } else {
                                            print("Get OTP:", JSON)
                                        }
                                    }
        },
                                 failure: { (error) in
                                    print(error)
                                    self.stopAnimating()
                                    self.showAlert(title: "Invalid Phone Number", message: "Please insert valid phone number")
                                    self.loginBtn.isEnabled = true
                                    
        })
        
    }
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}


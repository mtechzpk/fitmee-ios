//
//  SettingController.swift
//  Fit MY
//
//  Created by Azlan Shah on 06/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import SafariServices
import Localize_Swift

class SettingController: UIViewController, SFSafariViewControllerDelegate {
    
    @IBOutlet weak var profileImg: UIImageViewX!
    @IBOutlet weak var nickNameLbl: UILabel!
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet var fitpointLbl: UILabel!
    // Static Label for localize
    @IBOutlet weak var profileLbl: UILabel!
    @IBOutlet weak var faqLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
    @IBOutlet weak var languageLbl: UILabel!
    @IBOutlet weak var privacyLbl: UILabel!
    @IBOutlet weak var signoutLbl: UILabel!

    
    // Static button for localize
    @IBOutlet weak var backBtn: UIButton!
    
    
    let availableLanguages = Localize.availableLanguages()
    
    var actionSheet: UIAlertController!
    
    var profile = Profile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tap(_:))))
        profileImg.isUserInteractionEnabled = true
        
        
        updateProfileDetail()
        setProfile()
        getVersion()
        localizeLabel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func tap(_ gestureRecognizer: UITapGestureRecognizer) {
        openGallary()
    }
    
    @objc func localizeLabel() {
        profileLbl.text = "My Profile".localized()
        faqLbl.text = "FAQ".localized()
        feedbackLbl.text = "Feedback".localized()
        languageLbl.text = "".localized()
        privacyLbl.text = "Privacy Policy".localized()
        signoutLbl.text = "Sign Out".localized()
        
        fitpointLbl.text = userModel.fitness_points
//        backBtn.setTitle("Back".localized(), for: .normal)
        
        print("Language Changed: Setting")
    }
    
    func getVersion() {
        versionLbl.text = "FitMY iOS \(UIApplication.versionBuild())"
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(localizeLabel), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
        
        updateProfileDetail()
    }
    
    @IBAction func feedback(_ sender: Any) {
        let safariVC = SFSafariViewController(url: NSURL(string: "https://docs.google.com/forms/d/e/1FAIpQLSfLXFPL600uUzwZmABlaDZLrDntb22uj1RdR_vGhRKmXuwv3Q/viewform?vc=0&c=0&w=1")! as URL)
        self.present(safariVC, animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func privacyPolicy(_ sender: Any) {
        let safariVC = SFSafariViewController(url: NSURL(string: "http://www.northparkapp.com/privacypolicy.html")! as URL)
        self.present(safariVC, animated: true, completion: nil)
    }
//    @IBAction func back(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }

    
     @IBAction func menuBtn_Click(_ sender: Any) {
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            var frm = initialViewController.view.frame
            frm = self.view.frame
            initialViewController.view.frame = frm
            
            initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
    //        self.view.addSubview(initialViewController.view)
            initialViewController.viewController = self
            self.present(initialViewController, animated: false, completion: nil)
    //        self.navigationController?.pushViewController(initialViewController, animated: false)
        }
    
    
    @IBAction func myProfileBtn_Click(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let updateProfileVC:UpdateProfileVC = storyboard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        self.navigationController?.pushViewController(updateProfileVC, animated: true)
            
//                   self.navigationController?.pushViewController(initialViewController, animated: false)
    }
    
    
    @IBAction func signOut(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isAppAlreadyLaunchedOnce")
        print(UserDefaults.standard.value(forKey: "isAppAlreadyLaunchedOnce"))
        self.navigationController?.popToRootViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    func updateProfileDetail() {
        if let data = UserDefaults.standard.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profile = try? decoder.decode(Profile.self, from: data) {
                self.profile = profile
                print(profile)
                setProfile()
            }
        }
    }
    
    func setProfile() {
        nickNameLbl.text = userModel.full_name
        
        if let image = profile.photo as? String, image != nil {
            profileImg.kf.setImage(with: URL(string: image), options: [.transition(.fade(0.3))])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    if self.profile.gender == "F" {
                        self.profileImg.image = #imageLiteral(resourceName: "girl")
                        print("female")
                    } else {
                        self.profileImg.image = #imageLiteral(resourceName: "man")
                        print("male")
                    }
                }
            }
        }
    }
    
    @IBAction func changeLanguage(_ sender: Any) {
        actionSheet = UIAlertController(title: nil, message: "Switch Language".localized(), preferredStyle: UIAlertController.Style.actionSheet)
        for language in availableLanguages {
            let displayName = Localize.displayNameForLanguage(language)
            if displayName.count > 0 {
                let languageAction = UIAlertAction(title: displayName, style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    Localize.setCurrentLanguage(language)
                    UserDefaults.standard.set(language, forKey: "AppLanguage")
//                    self.localizeLabel()
//                    Bundle.swizzleLocalization()
                })
                actionSheet.addAction(languageAction)
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
            (alert: UIAlertAction) -> Void in
        })
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension SettingController: UIImagePickerControllerDelegate , UINavigationControllerDelegate
{
    func openGallary()
    {
        let imagePicketController = UIImagePickerController()
        imagePicketController.delegate = self
        
        let alerts = UIAlertController.init(title: "Select an option", message: "", preferredStyle: .actionSheet)
        alerts.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera)
            {
                imagePicketController.sourceType = .camera
                self.present(imagePicketController, animated: true, completion: nil)
            } else {
                print("Camera not available")
            }
        }))
        alerts.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePicketController.sourceType = .photoLibrary
            self.present(imagePicketController, animated: true, completion: nil)
            
        }))
        alerts.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alerts, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        let imageSelected = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImg.image = imageSelected
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}


extension UIApplication {
    
    class func appVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    class func appBuild() -> String {
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    class func versionBuild() -> String {
        let version = appVersion(), build = appBuild()
        
        return version == build ? "v\(version)" : "v\(version)(\(build))"
    }
}

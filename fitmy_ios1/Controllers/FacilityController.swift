//
//  FacilityController.swift
//  Fit MY
//
//  Created by Azlan Shah on 28/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

struct Facility {
    var id = String()
    var name = String()
    var address = String()
    var phone = String()
    var email = String()
    var cover_image = String()
}

struct RootFasiliti: Codable {
    var status : Bool
    var data : [Fasiliti]
}

struct RootKompleks: Codable {
    var status : Bool
    var data : [Kompleks]
}

struct RootNegeri: Codable {
    var status : Bool
    var data : [Negeri]
}

struct RefSukanRekreasi: Codable {
    var id : Int?
    var desc : String?
    var aktif : Int?
    var created_by : Int?
    var updated_by : Int?
    var created : String?
    var updated : String?
}

struct RefJenisKemudahan: Codable {
    var id : Int?
    var desc : String?
    var aktif : Int?
    var created_by : Int?
    var updated_by : Int?
    var created : String?
    var updated : String?
}

struct RefPengurusanVenue: Codable {
    var pengurusan_kemudahan_venue_id : Int?
    var nama_venue : String?
    var alamat_1: String?
    var alamat_2: String?
    var alamat_3: String?
    var alamat_negeri: String?
    var alamat_bandar: String?
    var alamat_poskod: String?
    var no_telefon: String?
    var no_faks: String?
    var emel: String?
    var tahun_pembinaan: String?
    var tahun_siap_pembinaan: String?
    var tarikh_siap_pembinaan: String?
    var kos_project: String?
    var keluasan_venue: String?
    var pemilik: String?
    var sewaan: String?
    var status: String?
    var public_user_id: Int?
    var kategori_hakmilik: Int?
    var created_by: Int?
    var updated_by: Int?
    var created: String?
    var updated: String?
}

struct Fasiliti: Codable {
    var pengurusan_kemudahan_sedia_ada_id : Int?
    var pengurusan_kemudahan_venue_id : Int?
    var refPengurusanVenue : RefPengurusanVenue?
    var kod_penjenisan : String?
    var nama_kemudahan : String?
    var sukan_rekreasi : Int?
    var refSukanRekreasi : RefSukanRekreasi?
    var jenis_kemudahan : Int?
    var refJenisKemudahan : RefJenisKemudahan?
    var size : String?
    var lokasi : String?
    var keluasan_padang : String?
    var jumlah_kapasiti : Int?
    var bilangan_kekerapan_penyenggaran : String?
    var kekerapan_penggunaan : String?
    var kekerapan_kerosakan_berlaku : String?
    var cost_pembaikian : String?
    var kadar_sewaan_sejam_siang : String?
    var kadar_sewaan_sehari_siang : String?
    var kadar_sewaan_seminggu_siang : String?
    var kadar_sewaan_sebulan_siang : String?
    var kadar_sewaan_sejam_malam : String?
    var kadar_sewaan_sehari_malam : String?
    var kadar_sewaan_seminggu_malam : String?
    var kadar_sewaan_sebulan_malam : String?
    var kadar_sewaan_sejam_siang_foreigner : String?
    var kadar_sewaan_sejam_malam_foreigner : String?
    var kadar_sewaan_sehari_foreigner : String?
    var diskaun_peratus : Int?
    var unit_ada : Int?
    var item_tambahan : String?
    var gambar_1 : String?
    var gambar_2 : String?
    var gambar_3 : String?
    var gambar_4 : String?
    var gambar_5 : String?
    var session_id : String?
    var created_by : Int?
    var updated_by : Int?
    var created : String?
    var updated : String?
}

struct Kompleks: Codable {
    var pengurusan_kemudahan_venue_id : Int?
    var nama_venue : String?
    var alamat_1 : String?
    var alamat_2 : String?
    var alamat_3 : String?
    var alamat_negeri : String?
    var alamat_bandar : String?
    var alamat_poskod : String?
    var no_telefon : String?
    var no_faks : String?
    var emel : String?
    var tahun_pembinaan : String?
    var tahun_siap_pembinaan : String?
    var tarikh_siap_pembinaan : String?
    var kos_project : String?
    var keluasan_venue : String?
    var pemilik : String?
    var sewaan : String?
    var status : String?
    var public_user_id : Int?
    var kategori_hakmilik : Int?
    var created_by : Int?
    var updated_by : Int?
    var created : String?
    var updated : String?
}

struct Negeri: Codable {
    var id: String?
    var kod: String?
    var desc: String?
    var aktif: Int?
    var turutan: Int?
    var created_by: Int?
    var updated_by: Int?
    var created: String?
    var updated: String?
}

class FacilityController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var headerImg: UIImageView!
    
    var category = [Category]()
    var rewards = [Reward]()
    var currentReward = [Reward]()
    var makanan = [Reward]()
    var perkhidmatan = [Reward]()
    var peralatan = [Reward]()
    var penginapan = [Reward]()
    var pakaian = [Reward]()
    var rumah = [Reward]()
    var history = [Reward]()
    
    var currentCategory = 6
    
    var facility = [Facility]()
    var currentFacility = [Facility]()
    
    var kompleks = [Kompleks]()
    var fasiliti = [Fasiliti]()
    var negeri = [Negeri]()
    
    var currentKompleks = [Kompleks]()
    
//    var currentComplex = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchField.delegate = self
        tableView.register(cellType: RewardCell.self)
        tableView.backgroundColor = .clear
        
        category.append(Category(img: #imageLiteral(resourceName: "restaurant"), btn: 0, lbl: "Court"))
        category.append(Category(img: #imageLiteral(resourceName: "basketball"), btn: 1, lbl: "Field"))
        category.append(Category(img: #imageLiteral(resourceName: "dumbbell"), btn: 2, lbl: "Gym"))
        category.append(Category(img: #imageLiteral(resourceName: "hot-air-balloon"), btn: 3, lbl: "Dorm"))
        category.append(Category(img: #imageLiteral(resourceName: "medicine"), btn: 4, lbl: "Hall"))
        category.append(Category(img: #imageLiteral(resourceName: "lamp"), btn: 5, lbl: "Track"))
        category.append(Category(img: #imageLiteral(resourceName: "sea-knot"), btn: 6, lbl: "Others"))
        category.append(Category(img: #imageLiteral(resourceName: "calendar"), btn: 7, lbl: "History"))
        
        
        
//        getAllReward()
        facility.append(Facility(id: "1", name: "Kompleks Belia Dan Sukan Perlis", address: "Kompleks Belia Dan Sukan, Negeri Perlis, Jalan Hospital", phone: "049793505", email: "jbsnperlis@kbs.gov.my", cover_image: "https://fitmyapp-public-bucket-01.s3-ap-southeast-1.amazonaws.com/facility_resources/cover_image/1.jpg"))
        facility.append(Facility(id: "2", name: "Kompleks Belia Dan Sukan Kedah", address: "Kompleks Belia Dan Sukan, Negeri Kedah, Jln Stadium", phone: "047334616", email: "kombeskdh@yahoo.com", cover_image: "https://fitmyapp-public-bucket-01.s3-ap-southeast-1.amazonaws.com/facility_resources/cover_image/2.jpg"))
        facility.append(Facility(id: "3", name: "Kompleks Rakan Muda Klang", address: "Kompleks Rakan Muda, Daerah Klang, Lot 43204,, Jalan Klang Perdana, Sementa", phone: "032904171", email: "pbsdklang@gmail.com", cover_image: "https://fitmyapp-public-bucket-01.s3-ap-southeast-1.amazonaws.com/facility_resources/cover_image/3.jpg"))
        facility.append(Facility(id: "4", name: "Kompleks Belia Dan Sukan Johor", address: "Kompleks Belia dan Sukan, Negeri Johor, Jalan Sentosa Larkin", phone: "072223818", email: "kombes_johor@yahoo.com.my", cover_image: "https://fitmyapp-public-bucket-01.s3-ap-southeast-1.amazonaws.com/facility_resources/cover_image/4.jpg"))
        facility.append(Facility(id: "5", name: "Kompleks Rakan Muda Kuala Pilah", address: "Kompleks Rakan Muda, Kuala Pilah, Jalan Melang", phone: "0364811328", email: "jbsn9@kbs.gov.my", cover_image: "https://fitmyapp-public-bucket-01.s3-ap-southeast-1.amazonaws.com/facility_resources/cover_image/5.jpg"))
        facility.append(Facility(id: "6", name: "Kompleks Rakan Muda Jempol", address: "Kompleks Rakan Muda Jempol, Jalan Kasih, Bandar Seri jempol", phone: "064581399", email: "pbsdjempol@gmail.com", cover_image: "https://fitmyapp-public-bucket-01.s3-ap-southeast-1.amazonaws.com/facility_resources/cover_image/6.jpg"))

        currentFacility = facility
        
        self.headerImg.kf.setImage(with: URL(string: facility[Int.random(in: 0 ..< facility.count)].cover_image), options: [.transition(.fade(0.3))])
        
        getNegeri()
        getKompleks()
        getfasiliti()
    }
    
    func getNegeri() {
        
        // Get url for negeri
        guard let states = Bundle.main.url(forResource: "negeri", withExtension: "json") else {
            print("File could not be located at the given url")
            return
        }; do {
            // Get data from negeri
            let data = try Data(contentsOf: states)
            
            let decoder = JSONDecoder()
            //            decoder.keyDecodingStrategy = .convertFromSnakeCase
            //            decoder.dateDecodingStrategy = .secondsSince1970
            let result = try decoder.decode(RootNegeri.self, from: data)
            //            print(result)
            negeri = result.data
            print("Negeri:",negeri)
        } catch { print(error) }
        
    }
    func getKompleks() {
        // Get url for kompleks
        guard let complex = Bundle.main.url(forResource: "complex", withExtension: "json") else {
            print("File could not be located at the given url")
            return
        }; do {
            // Get data from file
            let data = try Data(contentsOf: complex)
            
            let decoder = JSONDecoder()
            //            decoder.keyDecodingStrategy = .convertFromSnakeCase
            //            decoder.dateDecodingStrategy = .secondsSince1970
            let result = try decoder.decode(RootKompleks.self, from: data)
            //            print(result)
            kompleks = result.data
            currentKompleks = kompleks
            tableView.reloadData()
            print("kompleks:",kompleks)
        } catch { print(error) }
    }
    func getfasiliti() {
        // Get url for facility
        guard let facility = Bundle.main.url(forResource: "facility", withExtension: "json") else {
            print("File could not be located at the given url")
            return
        }; do {
            // Get data from file
            let data = try Data(contentsOf: facility)
            
            let decoder = JSONDecoder()
            //            decoder.keyDecodingStrategy = .convertFromSnakeCase
            //            decoder.dateDecodingStrategy = .secondsSince1970
            let result = try decoder.decode(RootFasiliti.self, from: data)
//                        print(result)
            fasiliti = result.data
            print("fasiliti:",fasiliti)
        } catch { print(error) }
        
    }
    
    @IBAction func doSearch(_ sender: Any) {
        let search = searchField!.text!
//        currentFacility =  facility.filter{$0.name.lowercased().contains(search.lowercased())}
        currentKompleks = kompleks.filter{$0.nama_venue!.lowercased().contains(search.lowercased())}
        if search == "" {
            currentKompleks = kompleks
        }
        tableView.reloadData()
//        print(currentFacility)
    }
    
    
    func selectCategory(information: Any?) {
        print("select category: ", information!)
        let tag = information as! Int
        switch tag {
        case 0:
            currentReward = makanan
            currentCategory = 0
        case 1:
            currentReward = perkhidmatan
            currentCategory = 1
        case 2:
            currentReward = peralatan
            currentCategory = 2
        case 3:
            currentReward = penginapan
            currentCategory = 3
        case 4:
            currentReward = pakaian
            currentCategory = 4
        case 5:
            currentReward = rumah
            currentCategory = 5
        case 6:
            currentReward = rewards
            currentCategory = 6
        case 7:
            currentReward = history
            currentCategory = 7
        default:
            currentReward = rewards
            currentCategory = 6
        }
        
        tableView.reloadData()
    }
    
    func allReward() {
        selectCategory(information: 6)
    }
    
    func historyReward() {
        selectCategory(information: 7)
    }
    
    func nearbyReward() {
        selectCategory(information: 6)
    }
}

extension FacilityController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return FacilityCategoryView.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame = CGRect(
            x: 0,
            y: 0,
            width: tableView.bounds.size.width,
            height: self.tableView(tableView, heightForHeaderInSection: section)
        )
        // See the overridden `MyHeaderTableView.init(frame:)` initializer, which
        // automatically loads the view content from its nib using loadNibContent()
        let view = FacilityCategoryView(frame: frame) as! FacilityCategoryView
        view.fill(categories: category)
        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9490196078, blue: 0.9647058824, alpha: 1)
        view.events.listenTo(eventName: "category", action: selectCategory)
        view.events.listenTo(eventName: "allReward", action: allReward)
        view.events.listenTo(eventName: "historyReward", action: historyReward)
        view.events.listenTo(eventName: "nearbyReward", action: nearbyReward)
        view.updateBG(tag: currentCategory)
        //        view.fillForSection(section)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentKompleks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as RewardCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
//        cell.fill(title: currentFacility[indexPath.row].name, detail: currentFacility[indexPath.row].address, price: "RM 8.00", image: currentFacility[indexPath.row].cover_image, image2: #imageLiteral(resourceName: "relaxing-walk"))
//        dump(currentVenue)
        cell.fill(title: currentKompleks[indexPath.row].nama_venue ?? "Kompleks", detail: "\(currentKompleks[indexPath.row].alamat_1 ?? ""), \(currentKompleks[indexPath.row].alamat_2 ?? "") , \(currentKompleks[indexPath.row].alamat_3 ?? "")", price: "", image: currentFacility[Int.random(in: 0 ..< currentFacility.count)].cover_image, image2: #imageLiteral(resourceName: "relaxing-walk"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Facility_Detail") as? FacilityDetailController {
            let navController = UINavigationController(rootViewController: viewController)
            viewController.thisTitle = currentFacility[indexPath.row].name
            viewController.thisDetail = "Phone: \(currentFacility[indexPath.row].phone) \nEmail: \(currentFacility[indexPath.row].email)"
            viewController.thisImg = currentFacility[indexPath.row].cover_image
            viewController.thisLocation = currentFacility[indexPath.row].address
            viewController.thisRewardImg = ""
            present(navController, animated: true, completion: nil)
        }
    }
}

extension FacilityController {
    func json(from object:AnyObject) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

extension FacilityController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 { //search textfield
            currentKompleks = kompleks
            if textField.text!.count > 0 {
                doSearch(UIButton())
            } else {
                currentKompleks = kompleks
                tableView.reloadData()
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 1 { //search textfield
            currentFacility = facility
            if textField.text!.count > 0 {
                doSearch(UIButton())
            } else {
                currentFacility = facility
                tableView.reloadData()
            }
        }
    }
}

//
//  FitnessDiaryController.swift
//  Fit MY
//
//  Created by Azlan Shah on 30/08/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import SwiftSVG
import HealthKit
import CoreMotion
import Alamofire
import SwiftyJSON
class FitnessDiaryController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var burnLbl: UILabel!
    @IBOutlet weak var stepLbl: UILabel!
    @IBOutlet weak var gpsTrackingLbl: UILabel!
    @IBOutlet weak var fitnessActivityLbl: UILabel!
    @IBOutlet weak var startBtn: UIButtonX!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var exerciseLbl: UILabel!
    @IBOutlet weak var minuteLbl: UILabel!
    
    @IBOutlet weak var subTodayLbl: UILabel!
    @IBOutlet weak var subStep2Lbl: UILabel!
    @IBOutlet weak var subCalori2Lbl: UILabel!
    @IBOutlet weak var subTodayDistanceLbl: UILabel!
    @IBOutlet weak var subHeart2Lbl: UILabel!
    @IBOutlet weak var subSleep2Lbl: UILabel!
    @IBOutlet weak var hourLbl: UILabel!
    
    @IBOutlet weak var subStepLbl: UILabel!
    @IBOutlet weak var subCaloriLbl: UILabel!
    @IBOutlet weak var subDistanceLbl: UILabel!
    
    
    @IBOutlet var stepsView: UIViewX!
    @IBOutlet var CaloriesView: UIViewX!
    
    
    
    func localizeLabel() {
        titleLbl.text = "FITNESS DIARY".localized()
        distanceLbl.text = "Kilometer".localized()
        burnLbl.text = "Calorie".localized()
        stepLbl.text = "Total Steps".localized()
        gpsTrackingLbl.text = "GPS Tracking".localized()
        fitnessActivityLbl.text = "Fitness Activity".localized()
//        heightLbl.text = "Height".localized()
//        weightLbl.text = "Weight".localized()
//        exerciseLbl.text = "Exercise".localized()
//        minuteLbl.text = "Minutes".localized()
        startBtn.setTitle(" Start ".localized(), for: .normal)
        
        subTodayLbl.text = "Today".localized()
        subStep2Lbl.text = "steps".localized()
        subCalori2Lbl.text = "Calorie".localized()
        subTodayDistanceLbl.text = "Distance".localized()
        subHeart2Lbl.text = "Heart".localized()
        subSleep2Lbl.text = "Sleep".localized()
        hourLbl.text = "hour".localized()
    }
    
    var installDate: Date? {
        guard
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last,
            let attributes = try? FileManager.default.attributesOfItem(atPath: documentsURL.path)
            else { return nil }
        return attributes[.creationDate] as? Date
    }
    
    var healthStore = HKHealthStore()
    var timer = Timer()
    
    let pedometer = CMPedometer()
    
    var allActivity = [Activity]()
    
    var profile = Profile()
    
    @IBOutlet weak var activityScrollView: UIView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityStackViewChildView: UIView!
    @IBOutlet weak var todayStepLbl: UILabel!
    @IBOutlet weak var todayDistanceLbl: UILabel!
    @IBOutlet weak var todayCaloriesLbl: UILabel!
    @IBOutlet weak var height: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var bmi: UILabel!
    @IBOutlet weak var bmi_status: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateProfileDetail()
        checkPermission()
        addActivity()
        getFitnessDiary()
        self.activityScrollView.layoutIfNeeded()
        self.activityStackView.layoutIfNeeded()
        
        let Steptap = UITapGestureRecognizer(target: self, action: #selector(self.StepsAction(_:)))
        stepsView.addGestureRecognizer(Steptap)
        
        let Calorietap = UITapGestureRecognizer(target: self, action: #selector(self.CalorieAction(_:)))
        CaloriesView.addGestureRecognizer(Calorietap)
        
        if CMPedometer.isStepCountingAvailable() {
            let now = Date()
            var startOfDay = Calendar.current.startOfDay(for: now)
            if now.compareDate(toDate: installDate!) {
                print("same day installed, so use install date time")
                startOfDay = Calendar.current.startOfDay(for: installDate!)
            }
            pedometer.queryPedometerData(from: startOfDay, to: Date()) { (data, error) in
                DispatchQueue.main.async {
                    if let step = data?.numberOfSteps as? Int, step != 0 {
//                        self.todayStepLbl.text = String(describing: step)
                        self.subStepLbl.text = String(describing: step)
//                        self.todayCaloriesLbl.text = String(format: "%.01f", Double(step/20))
                        self.subCaloriLbl.text = String(format: "%.01f", Double(step/20))
                    }
                }
                print("Pedometer Query: ", data)
            }
            pedometer.startUpdates(from: startOfDay) { (data, error) in
                
                DispatchQueue.main.async {
                    if let step = data?.numberOfSteps as? Int, step != 0 {
//                        self.todayStepLbl.text = String(describing: step)
                        self.subStepLbl.text = String(describing: step)
//                        self.todayCaloriesLbl.text = String(format: "%.01f", Double(step/20))
                        self.subCaloriLbl.text = String(format: "%.01f", Double(step/20))
                    }
                }
                print("Pedometer Update: ", data)
            }
        }
    }
    
    @objc func StepsAction(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let stepsandCalVC:StepsAndCalorieVC = storyboard.instantiateViewController(withIdentifier: "StepsAndCalorieVC") as! StepsAndCalorieVC
        stepsandCalVC.fill(title: "Steps")
        stepsandCalVC.isSteps = true
        self.navigationController?.pushViewController(stepsandCalVC, animated: true)
    }
    
    @objc func CalorieAction(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let stepsandCalVC:StepsAndCalorieVC = storyboard.instantiateViewController(withIdentifier: "StepsAndCalorieVC") as! StepsAndCalorieVC
        stepsandCalVC.fill(title: "Calorie")
        stepsandCalVC.isSteps = false
        self.navigationController?.pushViewController(stepsandCalVC, animated: true)
    }
    
    
    func getFitnessDiary(){
        let param:Parameters = ["user_id":userModel.id]
        let header:HTTPHeaders = ["Accept":"application/json"]
        Alamofire.request("http://172.104.217.178/fitmy/public/api/fitness_diary", method: .post, parameters: param, headers: header).responseData { response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                let data = json["data"].dictionaryValue
                if let total_steps = data["total_steps"]?.stringValue{
                    self.todayStepLbl.text = total_steps
                }
                if let avg_distance = data["total_distance"]?.stringValue{
                    self.todayDistanceLbl.text = avg_distance
                    self.subDistanceLbl.text = avg_distance
                }
                if let avg_kcales = data["total_kcals"]?.stringValue{
                    self.todayCaloriesLbl.text = avg_kcales
                }
            case.failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
    
    func updateProfileDetail() {
        if let data = UserDefaults.standard.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profile = try? decoder.decode(Profile.self, from: data) {
                self.profile = profile
                print(profile)
            }
        }
    }
    
    func addActivity() {
        
        allActivity.append(Activity(img: #imageLiteral(resourceName: "relaxing-walk"), btn: 0, lbl: "Walking", MET: 3.5))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "man-sprinting"), btn: 1, lbl: "Running", MET: 11.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "man-climbing-stairs"), btn: 2, lbl: "Stairs Climbing", MET: 8.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "cyclist"), btn: 3, lbl: "Cycling", MET: 6.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "hiking"), btn: 4, lbl: "Hiking", MET: 8.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "swimming-figure"), btn: 5, lbl: "Swimming", MET: 6.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "exercising-silhouette"), btn: 6, lbl: "Aerobic", MET: 7.0))
        allActivity.append(Activity(img: #imageLiteral(resourceName: "person-running-on-a-treadmill-silhouette-from-side-view"), btn: 7, lbl: "Indoor Running", MET: 10.5))
        
        //        var activityArray = [UIView]()
        let activityView = self.activityStackViewChildView.copyView()
        for subview in activityStackView.subviews {
            subview.removeFromSuperview()
        }
        for activity in self.allActivity {
            let activityView2 = activityView.copyView() as! UIViewX
            
            // 0:image, 1:button, 2:label
            let img = activityView2.subviews[0] as! UIImageViewX
            img.image = activity.img
//            img.popIn = true
            //            img.cornerRadius = 14
            //            img.borderWidth = 2
            //            img.borderColor = #colorLiteral(red: 0.3703463674, green: 0.3789130151, blue: 0.3831247687, alpha: 1)
            let btn = activityView2.subviews[1] as! UIButton
            btn.tag = activity.btn
            btn.addTarget(self, action: #selector(self.chooseActivity(sender:)), for: .touchUpInside)
//            let label = activityView2.subviews[2] as! UILabel
//            label.text = activity.lbl
            activityView2.cornerRadius = 30
            activityView2.borderColor = #colorLiteral(red: 0.9921568627, green: 0.4392156863, blue: 0.07843137255, alpha: 1)
            activityView2.borderWidth = 1
            self.activityStackView.addArrangedSubview(activityView2)
        }
        self.activityScrollView.layoutIfNeeded()
        self.activityStackView.layoutIfNeeded()
    }
    
    @objc func chooseActivity(sender: UIButton) {
        print(sender.tag)
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Fitness_History_Two") as? FitnessHistoryTwoController {
            viewController.fill(title: allActivity[sender.tag].lbl)
            present(viewController, animated: true, completion: nil)
        }
        
//        performSegue(withIdentifier: "fitness_history", sender: sender)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        localizeLabel()
        updateProfileDetail()
        checkPermission()
//        getProfile()
        getExercise()
        getFitnessDiary()
    }
    
    @IBAction func menuBtn_Click(_ sender: Any) {
        
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        
        var frm = initialViewController.view.frame
        frm = self.view.frame
        initialViewController.view.frame = frm
        
        initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        
//        self.view.addSubview(initialViewController.view)
        initialViewController.viewController = self
        self.present(initialViewController, animated: false, completion: nil)
//        self.navigationController?.pushViewController(initialViewController, animated: false)
    }
    
    
    
    func getProfile() {
        let height = profile.height_cm
        let weight = profile.weight_kg
        
        self.height.text = String(format: "%.0f", height)
        self.weight.text = String(format: "%.0f", weight)
        let bmi = (weight/height/height)*10000.0
        self.bmi.text = String(format: "%.01f", bmi)
        var status = ""
        switch bmi {
        case 0 ..< 18.5:
            status = "Underweight".localized()
        case 18.5 ..< 25:
            status = "Healthy".localized()
        case 25 ..< 100:
            status = "Overweight".localized()
        default:
            status = "Healthy".localized()
        }
        self.bmi_status.text = status
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fitness_activity" {
            
        }
    }
    
}



extension FitnessDiaryController {
    func checkPermission() {
        // Access Step Count
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.distanceWalkingRunning)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!,HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!,HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!]
        // Check for Authorization
        healthStore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (bool, error) in
            if (bool) {
                // Authorization Successful
                
//                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getData), userInfo: nil, repeats: true)
                
                self.getData()
                
            } else {
                return
            }// end if
        } // end of checking authorization
        
//        self.timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.getData), userInfo: nil, repeats: true)
    }
    
    @objc func getData() {
        self.getSteps { (result) in
            DispatchQueue.main.async {
                let step = String(Int(result))
//                self.todayStepLbl.text = String(step)
                self.subStepLbl.text = String(step)
//                print(step, "step updated at", Date())
            }
        }
        self.getDistance { (result) in
            DispatchQueue.main.async {
//                self.todayDistanceLbl.text = String(format: "%.1f", Double(result))
//                self.subDistanceLbl.text = String(format: "%.1f", Double(result))
                print(String(format: "%.2f", Double(result)), "distance updated at", Date())
            }
        }
//        self.getCalories { (result) in
//            DispatchQueue.main.async {
//                let calories = String(Int(result))
//                if result > 0 {
//                    self.todayCaloriesLbl.text = String(calories)
//                }
//                print(calories, "calories updated at", Date())
//            }
//        }
        //        timer.invalidate()
        
        //        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getData), userInfo: nil, repeats: false)
    }
    
    func getTotalSteps(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        

        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //now.days(sinceDate: startDate)
        
        
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: installDate!,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            
            if result != nil {
                result!.enumerateStatistics(from: self.installDate!, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        let startDate = statistics.startDate
                        let sum = sum.doubleValue(for: HKUnit.count())
                        resultCount += sum
                        print("\(startDate): steps = \(resultCount)")
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    func getSteps(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        print("Install at:", installDate)
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.count())
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }
        
        
        // Update query data if new update is available (disable because not needed)
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in

            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                let resultCount = sum.doubleValue(for: HKUnit.count())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    
    func getDistance(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        let now = Date()
        
        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }
        
        var interval = DateComponents()
        interval.day = 1
        
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                    
                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.meter())
                        resultCount = resultCount/1000
                    } // end if
                    
                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            } else {
                print("Result Distance Empty")
            }
        }
        
        
        
        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in
            
            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                var resultCount = sum.doubleValue(for: HKUnit.meter())
                resultCount = resultCount/1000
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }
        
        healthStore.execute(query)
    }
    
    func getCalories(completion: @escaping (Double) -> Void) {
        let type = HKQuantityType.quantityType(forIdentifier: .heartRate)!

        let now = Date()

        var startOfDay = Calendar.current.startOfDay(for: now)
        if now.compareDate(toDate: installDate!) {
            print("same day installed, so use install date time")
            startOfDay = Calendar.current.startOfDay(for: installDate!)
        }

        var interval = DateComponents()
        interval.day = 1

        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            if result != nil {
                result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in

                    if let sum = statistics.sumQuantity() {
                        // Get steps (they are of double type)
                        resultCount = sum.doubleValue(for: HKUnit.kilocalorie())
                    } // end if

                    // Return
                    DispatchQueue.main.async {
                        completion(resultCount)
                    }
                }
            }
        }

        query.statisticsUpdateHandler = {
            query, statistics, statisticsCollection, error in

            // If new statistics are available
            if let sum = statistics?.sumQuantity() {
                var resultCount = sum.doubleValue(for: HKUnit.kilocalorie())
                // Return
                DispatchQueue.main.async {
                    completion(resultCount)
                }
            } // end if
        }

        healthStore.execute(query)
    }
}
extension FitnessDiaryController {
    
    func getExercise() {
        let url = "https://api.fitmyapp.asia/api/fitness/activity/get_activities"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd HH:mm:ss"
        AFWrapper.requestGETURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        print("Exercise data:", JSON)
                                    }
                                    
        },
                                 failure: { (error) in
                                    print(error)
                                    
        })
        
    }

    func json(from object:AnyObject) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    func reduceDayFrom(days: Int, date: Date) -> Date {
        return Calendar.current.date(byAdding: .day, value: -days, to: date)!
    }
    var sevenDayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -7, to: noon)!
    }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}

extension Date {
    
    func years(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.year], from: sinceDate, to: self).year
    }
    
    func months(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.month], from: sinceDate, to: self).month
    }
    
    func days(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.day], from: sinceDate, to: self).day
    }
    
    func hours(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.hour], from: sinceDate, to: self).hour
    }
    
    func minutes(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.minute], from: sinceDate, to: self).minute
    }
    
    func seconds(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.second], from: sinceDate, to: self).second
    }
    
}

extension Date {

    func compareDate(toDate:Date) -> Bool {
        let order = NSCalendar.current.compare(self, to: toDate, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }

}

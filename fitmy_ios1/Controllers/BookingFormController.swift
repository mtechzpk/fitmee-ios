//
//  BookingFormController.swift
//  Fit MY
//
//  Created by Azlan Shah on 05/12/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import WebKit

class BookingFormController: UIViewController, WKNavigationDelegate  {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var bookingTypeField: UITextField!
    @IBOutlet weak var startDateField: UITextField!
    @IBOutlet weak var endDateField: UITextField!
    @IBOutlet weak var startTImeField: UITextField!
    @IBOutlet weak var endTimeField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.alpha = 0
        webView.navigationDelegate = self
        webView.scrollView.isScrollEnabled = false
        
        let url = URL(string: "http://internal.mysysdemo.com/backend/web/index.php?r=api/tempahan-kemudahan/calendar")!
        webView.load(URLRequest(url: url))

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func changeRate(_ sender: Any) {
        if bookingTypeField.text == "Hourly" {
            bookingTypeField.text = "Daily"
        } else {
            bookingTypeField.text = "Hourly"
        }
    }
    
    @IBAction func selectDate(_ sender: Any) {
        let myDatePicker: UIDatePicker = UIDatePicker()
        myDatePicker.datePickerMode = .date
        myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let selectAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            print("Selected Date: \(myDatePicker.date)")
            self.startDateField.text = "\(myDatePicker.date)"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
        
    }
    
    @IBAction func selectEndDate(_ sender: Any) {
        let myDatePicker: UIDatePicker = UIDatePicker()
        myDatePicker.datePickerMode = .date
        myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let selectAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            print("Selected Date: \(myDatePicker.date)")
            
            self.endDateField.text = "\(myDatePicker.date)"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
    }
    
    @IBAction func selectStartTime(_ sender: Any) {
        let myDatePicker: UIDatePicker = UIDatePicker()
        myDatePicker.datePickerMode = .time
        myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let selectAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            print("Selected Date: \(myDatePicker.date)")
            
            self.startTImeField.text = "\(myDatePicker.date)"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
    }
    
    @IBAction func selectEndTime(_ sender: Any) {
        let myDatePicker: UIDatePicker = UIDatePicker()
        myDatePicker.datePickerMode = .time
        myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let selectAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            print("Selected Date: \(myDatePicker.date)")
            
            self.endTimeField.text = "\(myDatePicker.date)"
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
    }
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.alpha = 1
    }
    

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

//
//  FitnessHistoryTwoController.swift
//  Fit MY
//
//  Created by Azlan Shah on 22/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class FitnessHistoryTwoController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var days:[String] = []
    var stepsTaken:[Int] = []
    var distanceTaken:[Double] = []
    var caloriTaken:[Double] = []
    var heartTaken:[Int] = []
    var sleepTaken:[Int] = []
    
    var exercise = [Exercise]()
    var currentExercise = [Exercise]()
    
    var thisTitle = String()
    
    var imageView = UIImage()
    
    var activity_type = ""
    
    
    @IBOutlet weak var titleHeader: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(cellType: RewardCell.self)
        tableView.backgroundColor = .clear
        
        titleHeader.text = thisTitle.localized()
        
//        getExercise()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getActivityHistory()
    }
    
    func fill(title: String) {
        thisTitle = title
    }
    
    @IBAction func done(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension FitnessHistoryTwoController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentExercise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as RewardCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let mysqlDate = dateFormatterMYSQL.date(from: currentExercise[indexPath.row].startDate)
        
        let dateFormatterSwift = DateFormatter()
        dateFormatterSwift.dateStyle = .short
        dateFormatterSwift.timeStyle = .short
        
        switch currentExercise[indexPath.row].activity{
        case "Walking":
            imageView = #imageLiteral(resourceName: "relaxing-walk")
        case "Running":
            imageView = #imageLiteral(resourceName: "man-sprinting")
        case "Stairs Climbing":
            imageView = #imageLiteral(resourceName: "man-climbing-stairs")
        case "Cycling":
            imageView = #imageLiteral(resourceName: "cyclist")
        case "Hiking":
            imageView = #imageLiteral(resourceName: "hiking")
        case "Swimming":
            imageView = #imageLiteral(resourceName: "swimming-figure")
        case "Aerobic":
            imageView = #imageLiteral(resourceName: "exercising-silhouette")
        case "Indoor Running":
            imageView = #imageLiteral(resourceName: "person-running-on-a-treadmill-silhouette-from-side-view")
        default:
            break
        }
        
        cell.promoImg.contentMode = .center
        
        cell.fill(title: currentExercise[indexPath.row].activity, detail: "\(currentExercise[indexPath.row].steps) Steps", price: "\(currentExercise[indexPath.row].distance) km" , image: "", image2: imageView)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Exercise_View") as? ExerciseViewController {
            viewController.thisDuration = currentExercise[indexPath.row].timer
            viewController.fill(exercise: currentExercise[indexPath.row])
            present(viewController, animated: true, completion: nil)
        }
    }
}

extension FitnessHistoryTwoController {
    func getExercise() {
        let url = "https://api.fitmyapp.asia/api/fitness/activity/get_activities"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd HH:mm:ss"
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            for (index, value) in data.enumerated() {
                                                if value["activity_type"].stringValue == self.thisTitle {
                                                    self.exercise.append(Exercise(id: value["id"].stringValue, steps: value["steps"].stringValue, startDate: value["start_datetime"].stringValue, endDate: value["end_datetime"].stringValue, activity: value["activity_type"].stringValue, distance: value["distance"].stringValue,calorie: value["calories"].stringValue, path: value["paths"].stringValue))
                                                }
                                            }
                                            self.currentExercise = self.exercise
                                            print("Exercise data:", JSON)
                                            self.tableView.reloadData()
                                        }
                                    }
        },
                                failure: { (error) in
                                    print(error)
                                    
        })
        
    }
    
    
    func getActivityHistory() {
        let url = "http://172.104.217.178/fitmy/public/api/get_activity_history"
        var header = [String:String]()
        header["Accept"] = "application/json"
        
        var body = [String:Any]()
        body["user_id"] = userModel.id
        body["activity_type"] = thisTitle //UserDefaults.standard.string(forKey: "user_id") // UserID
        
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { (Response) in
            let responseData = Response.dictionaryValue
            if(responseData["status"] == 200)
            {
                let data = responseData["data"]?.arrayValue
                
                for obj in data ?? []
                {
                    self.exercise.append(Exercise(id: obj["id"].stringValue,steps: obj["steps"].stringValue, timer: obj["timer"].stringValue, startDate: obj["start_date"].stringValue, endDate: obj["end_date"].stringValue, activity: self.thisTitle, distance: obj["distance"].stringValue,calorie: obj["kcal"].stringValue, start_latitude: obj["start_latitude"].stringValue, start_longitude: obj["start_longitude"].stringValue, end_latitude: obj["end_latitude"].stringValue, end_longitude: obj["end_longitude"].stringValue, path: ""))
                }
                self.currentExercise = self.exercise
                self.tableView.reloadData()
                
            }
            else
            {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: responseData["message"]?.stringValue ?? "Registration Failed!", Sender: self)
            }
            
            
        }) { (error) in
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: error.localizedDescription, Sender: self)
            return
        }
    }
    
    
}

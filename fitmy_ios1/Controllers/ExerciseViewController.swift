//
//  ExerciseViewController.swift
//  Fit MY
//
//  Created by Azlan Shah on 06/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift
import CoreMotion
import HealthKit
import HCKalmanFilter

class ExerciseViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var overlayView: UIViewX!
    @IBOutlet weak var startBtn: UIButtonX!
    @IBOutlet weak var menuBtnView: UIViewX!
    @IBOutlet weak var settingBtnView: UIViewX!
    @IBOutlet weak var statsStackView: UIStackView!
    @IBOutlet weak var startBtnView: UIViewX!
    
    
    @IBOutlet weak var menuStackView: UIStackView!
    @IBOutlet weak var activityScrollView: UIView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityStackViewChildView: UIView!
    @IBOutlet weak var currentActivityImg: UIImageView!
    
    @IBOutlet weak var paceLbl: UILabel!
    @IBOutlet weak var bpmLbl: UILabel!
    @IBOutlet weak var speedLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceUnitLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var stepCountLbl: UILabel!
    @IBOutlet weak var caloriBurnLbl: UILabel!
    @IBOutlet weak var startButton: UIButton!
    
    @IBOutlet weak var exerciseTypeLbl: UILabel!
    @IBOutlet weak var exerciseTypeImg: UIImageView!
    
    @IBOutlet weak var caloriLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var stepLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    func localizeLabel() {
        distanceLabel.text = "Kilometer".localized()
        paceLabel.text = "Pace".localized()
        timeLabel.text = "Time".localized()
        caloriLabel.text = "Calories".localized()
        speedLabel.text = "kmh".localized()
        stepLabel.text = "Steps".localized()
    }
    
    var healthStore = HKHealthStore()
    
    var allActivity = [Activity]()
    
    var locationManager: CLLocationManager!
    var locations: Results<Location>!
    var token: NotificationToken!
    var isUpdating = false
    
    var totalCalori = 0.0
    var counter = 0.0
    var distanceMain = 0.0
    var stepMain = 0
    var avgSpeed = Double()
    var timer = Timer()
    var isPlaying = false
    var currentStatus = "stop"
    var startTime = Date()
    var endTime = Date()
    
    var lastLocation = Location()
    
    var currentActivity = 0
    
    let pedometer = CMPedometer()
    
    var thisExercise = Exercise()
    var thisActivityType = String()
    var thisAvgSpeed = String()
    var thisDistance = String()
    var thisDuration = String()
    var thisCalories = String()
    var thisSteps = String()
    var thisPace = String()
    var activityImg = UIImage()
    
    var latitude = [String]()
    var longitude = [String]()
    var timestamp = [String]()
    var routeCoordinates = [CLLocationCoordinate2D]()
    
    
    var resetKalmanFilter: Bool = false
    var hcKalmanFilter: HCKalmanAlgorithm?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        localizeLabel()
        exerciseTypeLbl.text = thisActivityType
        exerciseTypeImg.image = activityImg
        stepCountLbl.text = thisSteps
        distanceLbl.text = thisDistance
        caloriBurnLbl.text = thisCalories
        paceLbl.text = thisPace
        durationLbl.text = thisDuration
        
        if thisActivityType == "Cycling" {
            paceLbl.alpha = 0.0
            stepCountLbl.alpha = 0.0
            paceLabel.alpha = 0.0
            stepLabel.alpha = 0.0
        }
        
        print(thisExercise.path)
        
        dropLine()
    }
    
    func fill(exercise: Exercise) {
        thisExercise = exercise
        
        switch thisExercise.activity{
        case "Walking":
            activityImg = #imageLiteral(resourceName: "relaxing-walk")
        case "Running":
            activityImg = #imageLiteral(resourceName: "man-sprinting")
        case "Stairs Climbing":
            activityImg = #imageLiteral(resourceName: "man-climbing-stairs")
        case "Cycling":
            activityImg = #imageLiteral(resourceName: "cyclist")
        case "Hiking":
            activityImg = #imageLiteral(resourceName: "hiking")
        case "Swimming":
            activityImg = #imageLiteral(resourceName: "swimming-figure")
        case "Aerobic":
            activityImg = #imageLiteral(resourceName: "exercising-silhouette")
        case "Indoor Running":
            activityImg = #imageLiteral(resourceName: "person-running-on-a-treadmill-silhouette-from-side-view")
        default:
            break
        }
        thisActivityType = exercise.activity
        thisDistance = exercise.distance
        thisSteps = exercise.steps
        thisCalories = exercise.calorie
        
        var x = [[String]]()
        var set = thisExercise.path.components(separatedBy: ";")
        _ = set.popLast()
        for value in set {
            x.append(value.components(separatedBy: ","))
        }
        for value in x {
            latitude.append(value[0])
            longitude.append(value[1])
            timestamp.append(value[2])
        }
        
        if(thisDuration == "")
        {
        let dateFormatterMYSQL = DateFormatter()
        dateFormatterMYSQL.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let startDate = dateFormatterMYSQL.date(from: exercise.startDate)
        let endDate = dateFormatterMYSQL.date(from: exercise.endDate)
        
        let difference = Calendar.current.dateComponents([.second], from: startDate!, to: endDate!)
        
        let counter = difference.second!
        var hour = "00"
        var minute = "00"
        var second = "00"
        var ahour = 0
        var aminute = 0
        var asecond = 0
        if counter > 3600 {
            ahour = counter/3600
            aminute = (counter-(3600*ahour))/60
            asecond = counter-(3600*ahour)-(60*aminute)
        } else if counter > 60 {
            aminute = (counter-(3600*ahour))/60
            asecond = counter-(3600*ahour)-(60*aminute)
        } else {
            asecond = counter
        }
        if ahour < 10 {
            hour = "0\(ahour)"
        } else {
            hour = "\(ahour)"
        }
        if aminute < 10 {
            minute = "0\(aminute)"
        } else {
            minute = "\(aminute)"
        }
        if asecond < 10 {
            second = "0\(asecond)"
        } else {
            second = "\(asecond)"
        }
        
        
            thisDuration = "\(hour):\(minute):\(second)"
            let dsec = Double(difference.second!/60)
            let distance = Double(exercise.distance)
            
            let paceCount = (dsec/(distance!/1000.0))
            let paceCount2 = (dsec.truncatingRemainder(dividingBy: (distance!/1000.0)))
            thisPace = String(format: "%.0f\"%\'", paceCount, paceCount2)
            
            print(difference.second!)
        }
        
//        let dsec = Double(difference.second!/60)
//        let distance = Double(exercise.distance)
//        
//        let paceCount = (dsec/(distance!/1000.0))
//        let paceCount2 = (dsec.truncatingRemainder(dividingBy: (distance!/1000.0)))
//        thisPace = String(format: "%.0f\"%\'", paceCount, paceCount2)
//        
//        print(difference.second!)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Draw Pin and Line on the map
    fileprivate func dropLine() {
        if latitude.count > 0 {
//            for (index,lat) in latitude.enumerated() {
//                let myLocation = CLLocation(latitude: Double(latitude[index]) as! CLLocationDegrees, longitude: Double(longitude[index]) as! CLLocationDegrees)
//                if hcKalmanFilter == nil {
//                    self.hcKalmanFilter = HCKalmanAlgorithm(initialLocation: myLocation)
//                    self.hcKalmanFilter?.rValue = 10
//                    routeCoordinates.append(CLLocationCoordinate2D(latitude: Double(latitude[index])?.rounded(toPlaces: 4) as! CLLocationDegrees, longitude: Double(longitude[index])?.rounded(toPlaces: 4) as! CLLocationDegrees))
//                } else {
//                    if let hcKalmanFilter = self.hcKalmanFilter {
//                        let kalmanLocation = hcKalmanFilter.processState(currentLocation: myLocation)
//                        routeCoordinates.append(CLLocationCoordinate2D(latitude: kalmanLocation.coordinate.latitude.rounded(toPlaces: 4), longitude: kalmanLocation.coordinate.longitude.rounded(toPlaces: 4)))
//                        print("Coordinate Count: ", routeCoordinates.count)
//
//                    }
//                }
//            }
//            print("Server Total Point: ", latitude.count)
//            print("Kalman Total Point: ", routeCoordinates.count)
//            
//
//            for (index,coord) in routeCoordinates.enumerated() {
//                print(index, ": Latitude: w", coord.latitude, latitude[index])
//                print(index, ": Longitude: ", coord.longitude, longitude[index])
//            }
            
            for (index,lat) in latitude.enumerated() {
                print(index, ": Latitude: ", latitude[index], " | Longitude: ", longitude[index])
    
                routeCoordinates.append(CLLocationCoordinate2D(latitude: Double(lat) as! CLLocationDegrees, longitude: Double(longitude[index]) as! CLLocationDegrees))
            }
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = routeCoordinates.last!
            annotation.title = "Start"
            annotation.subtitle = timestamp.first
            self.mapView.addAnnotation(annotation)
            
            let annotation2 = MKPointAnnotation()
            annotation2.coordinate = routeCoordinates.first!
            annotation2.title = "Stop"
            annotation2.subtitle = timestamp.last
            self.mapView.addAnnotation(annotation2)
            
            let routeLine = MKPolyline(coordinates: routeCoordinates, count: routeCoordinates.count)
            mapView.setVisibleMapRect(routeLine.boundingMapRect, animated: false)
            mapView.addOverlay(routeLine)
        }
    }
    
    func setVisibleMapArea(polyline: MKPolyline, edgeInsets: UIEdgeInsets, animated: Bool = false) {
        mapView.setVisibleMapRect(polyline.boundingMapRect, edgePadding: edgeInsets, animated: animated)
    }
    
}

// MARK: - MKMapView delegate
extension ExerciseViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polyline = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: polyline)
            polylineRenderer.strokeColor = .orange
            polylineRenderer.lineWidth = 2
            setVisibleMapArea(polyline: polyline, edgeInsets: UIEdgeInsets(top: 148.0, left: 48.0, bottom: 32.0, right: 48.0))
            return polylineRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


//
//  NewsDetailController.swift
//  Fit MY
//
//  Created by Azlan Shah on 04/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class NewsDetailController: UIViewController {

    @IBOutlet weak var bgImg: UIImageView!
    @IBOutlet weak var frontImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var sourceLbl: UILabel!
    
    var thisTitle = String()
    var thisDesc = String()
    var thisDate = String()
    var thisSource = String()
    var thisImg = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        bgImg.image = bgImg.image?.blurImage(blurAmount: 4.0)
        
        titleLbl.text = thisTitle
        descLbl.text = thisDesc
        dateLbl.text = thisDate
        sourceLbl.text = thisSource
        bgImg.kf.setImage(with: URL(string: thisImg), options: [.transition(.fade(0.3))])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
                self.bgImg.image = self.bgImg.image?.blurImage(blurAmount: 4.0)
            }
        }
        frontImg.kf.setImage(with: URL(string: thisImg), options: [.transition(.fade(0.3))])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
                self.bgImg.image = self.bgImg.image
            }
        }
    }
    
    @IBAction func done(_ sender: UIButtonX) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setNews(title: String, desc: String, date: String, source: String, img: String) {
        thisTitle = title
        thisDesc = desc
        thisDate = date
        thisSource = source
        thisImg = img
    }
    
}

extension UIImage {
    func blurImage(blurAmount: CGFloat) -> UIImage? {
        guard let ciImage = CIImage(image: self) else {
            return nil
        }
        
        let blurFilter = CIFilter(name: "CIGaussianBlur")
        blurFilter?.setValue(ciImage, forKey: kCIInputImageKey)
        blurFilter?.setValue(blurAmount, forKey: kCIInputRadiusKey)
        
        guard let outputImage = blurFilter?.outputImage else {
            return nil
        }
        
        return UIImage(ciImage: outputImage)
    }
}

//
//  MapController.swift
//  Fit MY
//
//  Created by Azlan Shah on 06/09/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class MapController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var paceLbl: UILabel!
    @IBOutlet weak var bpmLbl: UILabel!
    @IBOutlet weak var speedLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceUnitLbl: UILabel!
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var stepCountLbl: UILabel!
    @IBOutlet weak var caloriBurnLbl: UILabel!
    
    var locationManager: CLLocationManager!
    var locations: Results<Location>!
    var token: NotificationToken!
    var isUpdating = false
    
    var counter = 0.0
    var distanceMain = 0.0
    var stepMain = 0
    var timer = Timer()
    var isPlaying = false
    
    var lastLocation = Location()
    
    @IBAction func back(_ sender: Any) {
        clearButtonDidTap(UIButton())
        print("back")
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
//        self.locationManager.distanceFilter = 25
        
        // Delete old location objects
        self.deleteOldLocations()
        
        // Load stored location objects
//        self.locations = self.loadStoredLocations()
        
        // Drop pins
//        for location in self.locations {
//            self.dropPin(at: location)
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Private methods
    
    @IBAction func startButtonDidTap(_ sender: AnyObject) {
        self.toggleLocationUpdate()
    }
    
    @IBAction func clearButtonDidTap(_ sender: AnyObject) {
        self.deleteAllLocations()
        self.locations = self.loadStoredLocations()
        self.removeAllAnnotations()
        self.tableView.reloadData()
        counter = 0.0
    }
    
    // Load locations stored in realm at the table view
    fileprivate func loadStoredLocations() -> Results<Location> {
        // Get the default Realm
        let realm = try! Realm()
        
        // Load recent location objects
        return realm.objects(Location.self).sorted(byKeyPath: "createdAt", ascending: false)
    }
    
    // Start or Stop location update
    fileprivate func toggleLocationUpdate() {
        let realm = try! Realm()
        if self.isUpdating {
            // Stop
            self.isUpdating = false
            self.locationManager.stopUpdatingLocation()
            self.startButton.setTitle("Start", for: UIControl.State())
            
            // Remove a previously registered notification
            if let token = self.token {
                token.invalidate()
            }
            
            // Drop pins at beginning point
            if let location = self.locations.first {
                dropPin(at: location)
            }
            
            dropLine()
            
            timer.invalidate()
            isPlaying = false
            
        } else {
            //clear
            clearButtonDidTap(UIButton())
            removeLine()
            
            // Start
            self.isUpdating = true
            self.locationManager.startUpdatingLocation()
            self.startButton.setTitle("Stop", for: UIControl.State())
            
            // Add a notification handler for changes
            self.token = realm.observe {
                [weak self] notification, realm in
                self?.tableView.reloadData()
            }
            
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateTimer), userInfo: nil, repeats: true)
            isPlaying = true
            
        }
    }
    
    @objc func UpdateTimer() {
        counter = counter + 0.1
        if counter > 3600 {
            let hour = Int(counter/3600)
            let minute = Int((counter-Double(3600*hour))/60)
            let second = Int(counter-Double(3600*hour)-Double(60*minute))
            durationLbl.text = "\(hour):\(minute):\(second)"
        } else if counter > 60 {
            let hour = Int(counter/3600)
            let minute = Int((counter-Double(3600*hour))/60)
            let second = Int(counter-Double(3600*hour)-Double(60*minute))
            durationLbl.text = "\(minute) Minute, \(second) Second"
        } else {
            let hour = 00
            let minute = 00
            let second = Int(counter)
            durationLbl.text = "\(second) Second"
        }
    }
    // Store object
    fileprivate func addCurrentLocation(_ rowLocation: CLLocation) {
        let location = makeLocation(rawLocation: rowLocation)
        
        // Get the default Realm
        let realm = try! Realm()
        
        // Add to the Realm inside a transaction
        try! realm.write {
            realm.add(location)
        }
    }
    
    // Delete old (-1 day) objects in a background thread
    fileprivate func deleteOldLocations() {
        DispatchQueue.global().async {
            // Get the default Realm
            let realm = try! Realm()
            
            // Old Locations stored in Realm
            let oldLocations = realm.objects(Location.self).filter(NSPredicate(format:"createdAt < %@", NSDate().addingTimeInterval(-86400)))
            
            // Delete an object with a transaction
            try! realm.write {
                realm.delete(oldLocations)
            }
        }
    }
    
    // Delete all location objects from realm
    fileprivate func deleteAllLocations() {
        // Get the default Realm
        let realm = try! Realm()
        
        // Delete all objects from the realm
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    // Make Location object from CLLocation
    fileprivate func makeLocation(rawLocation: CLLocation) -> Location {
        let location = Location()
        location.latitude = rawLocation.coordinate.latitude
        location.longitude = rawLocation.coordinate.longitude
        location.createdAt = Date()
        return location
    }
    
    // Drop pin on the map
    fileprivate func dropPin(at location: Location) {
        if location.latitude != 0 && location.longitude != 0 {
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude)
            annotation.title = "\(location.latitude),\(location.longitude)"
            annotation.subtitle = location.createdAt.description
            self.mapView.addAnnotation(annotation)
        }
    }
    
    // Draw Line on the map
    fileprivate func dropLine() {
        // Load stored location objects
        self.locations = self.loadStoredLocations()
        
        var routeCoordinates = [CLLocationCoordinate2D]()
        
        for location in self.locations {
            routeCoordinates.append(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        }
        
        let routeLine = MKPolyline(coordinates: routeCoordinates, count: routeCoordinates.count)
//        mapView.setVisibleMapRect(routeLine.boundingMapRect, animated: false)
        mapView.addOverlay(routeLine)
    }
    
    //Remove Line on map
    fileprivate func removeLine() {
        for overlay in mapView.overlays {
            mapView.removeOverlay(overlay)
        }
    }
    
    // Remove all pins on the map
    fileprivate func removeAllAnnotations() {
        let annotations = self.mapView.annotations.filter {
            $0 !== self.mapView.userLocation
        }
        self.mapView.removeAnnotations(annotations)
    }
}

// MARK: - CLLocationManager delegate
extension MapController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.notDetermined {
            locationManager.requestAlwaysAuthorization()
        }
        else if status == CLAuthorizationStatus.authorizedAlways {
            // Center user location on the map
            let span = MKCoordinateSpan.init(latitudeDelta: 0.003, longitudeDelta: 0.003)
            let region = MKCoordinateRegion.init(center: self.mapView.userLocation.coordinate, span: span)
            self.mapView.setRegion(region, animated:true)
            self.mapView.userTrackingMode = MKUserTrackingMode.followWithHeading
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations:[CLLocation]) {
        guard let newLocation = locations.last else {
            return
        }
        
        if !CLLocationCoordinate2DIsValid(newLocation.coordinate) {
            return
        }
        
        if self.locations.count > 0 {
            lastLocation = self.locations.last!
            let first = CLLocation(latitude: self.locations.first!.latitude, longitude: self.locations.first!.longitude)
            distanceMain += newLocation.distance(from: first)
        } else {
            lastLocation = makeLocation(rawLocation: newLocation)
            distanceMain = 0.0
        }
        
        self.addCurrentLocation(newLocation)
        
        // Drop pins at beginning point
        if self.locations.count == 1 {
            if let location = self.locations.last {
                dropPin(at: location)
                
            }
        }
        
        let kmh = locations.last!.speed * 3.6
        speedLbl.text = String(format: "%.0f", kmh)
        print(String(format: "%.0f km/h", kmh))
        
//        let first = CLLocation(latitude: self.locations.last!.latitude, longitude: self.locations.last!.longitude)
//        var distance: CLLocationDistance = newLocation.distance(from: first)
        
        var distance = distanceMain
        if distance > 999 {
            distance = distance/1000.0
            print(distance)
            distanceLbl.text = String(format: "%.02f", distance)
            distanceUnitLbl.text = "km"
        } else {
            print(String(format: "%f meter", distance))
            distanceLbl.text = String(format: "%.0f", distance)
            distanceUnitLbl.text = "meter"
        }
        
        let stepCount = Int(distance/1.3123359580052)
        stepCountLbl.text = "\(stepCount)"
        
        let paceCount = (counter/distance)
        paceLbl.text = String(format: "%.01f\"", paceCount)
        
        print("Floor: ", newLocation.floor)
        print("Altitude: ", newLocation.altitude)
        
        let caloriBurn = (counter/60)*(distance/1000)*2
        caloriBurnLbl.text = String(format: "%.01f", caloriBurn)
        print("Calori Burn: ", caloriBurn)
        
    }
}

// MARK: - MKMapView delegate
extension MapController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let reuseId = "annotationIdentifier"
        
        var pinView = self.mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView?.canShowCallout = true
            pinView?.animatesDrop = true
        }
        else {
            pinView?.annotation = annotation
        }
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polyline = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: polyline)
            polylineRenderer.strokeColor = .blue
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}

// MARK: - Table view data source
extension MapController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return self.locations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellIdentifier", for: indexPath)
        
        let location = self.locations[indexPath.row]
        cell.textLabel?.text = "\(location.latitude),\(location.longitude)"
        cell.detailTextLabel?.text = location.createdAt.description
        
        return cell
    }
}

// MARK: - Table view delegate
extension MapController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

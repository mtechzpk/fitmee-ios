//
//  RewardController.swift
//  Fit MY
//
//  Created by Azlan Shah on 25/09/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

struct Reward {
    var id = String()
    var my_name = String()
    var my_description = String()
    var en_name = String()
    var en_description = String()
    var start_datetime = String()
    var end_datetime = String()
    var rv_name = String()
    var rv_logo = String()
    var rv_phone_num = String()
    var rv_address = String()
    var rc_id = String()
    var c_en_name = String()
    var rc_my_name = String()
    var point = Double()
}

struct Redeem {
    var id = String()
    var item_id = String()
    var user_id = String()
    var code = String()
    var date_used = String()
    var date_created = String()
}

class RewardController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var fitPointLbl: UILabel!
    
    func localizeLabel() {
        titleLbl.text = "REDEEM REWARDS".localized()
        descLbl.text = "Choose reward to redeem".localized()
        fitPointLbl.text = "Fit Point".localized()
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pointLbl: UILabel!
    
    var category = [Category]()
    var rewards = [Reward]()
    var currentReward = [Reward]()
    var makanan = [Reward]()
    var perkhidmatan = [Reward]()
    var peralatan = [Reward]()
    var penginapan = [Reward]()
    var pakaian = [Reward]()
    var rumah = [Reward]()
    var history = [Reward]()
    
    var redeem = [Redeem]()
    
    var currentCategory = 6
    
    var totalPoint = Int()
    var usedPoint = Int()
    
    var profile = Profile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(cellType: RewardCell.self)
        tableView.backgroundColor = .clear
        
        category.append(Category(img: #imageLiteral(resourceName: "restaurant"), btn: 0, lbl: "Food & Beverages".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "basketball"), btn: 1, lbl: "Sports Facilities".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "dumbbell"), btn: 2, lbl: "Sports Equipment".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "hot-air-balloon"), btn: 3, lbl: "Travel & Tours".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "medicine"), btn: 4, lbl: "Healthcare & Beauty".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "lamp"), btn: 5, lbl: "Home & Furniture".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "sea-knot"), btn: 6, lbl: "Others".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "calendar"), btn: 7, lbl: "History".localized()))
        
        getAllReward()
        getRewardHistory()
        updateProfileDetail()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localizeLabel()
        tableView.reloadData()
        tableView.reloadSections([0], with: .none)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getPoint()
        self.history.removeAll()
        getRewardHistory()
        EventManager().trigger(eventName: "localizeHeader", information: UIButton())
    }
    
    func updateProfileDetail() {
        if let data = UserDefaults.standard.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profile = try? decoder.decode(Profile.self, from: data) {
                self.profile = profile
                print(profile)
            }
        }
    }
    
    func updatePoint() {
        pointLbl.text = "\(totalPoint-usedPoint)ƒ"
    }

    
    func selectCategory(information: Any?) {
        print("select category: ", information!)
        let tag = information as! Int
        switch tag {
        case 0:
            currentReward = makanan
            currentCategory = 0
        case 1:
            currentReward = perkhidmatan
            currentCategory = 1
        case 2:
            currentReward = peralatan
            currentCategory = 2
        case 3:
            currentReward = penginapan
            currentCategory = 3
        case 4:
            currentReward = pakaian
            currentCategory = 4
        case 5:
            currentReward = rumah
            currentCategory = 5
        case 6:
            currentReward = rewards
            currentCategory = 6
        case 7:
            currentReward = history
            currentCategory = 7
        default:
            currentReward = rewards
            currentCategory = 6
        }
        
        tableView.reloadData()
    }
    
    func allReward() {
        selectCategory(information: 6)
    }
    
    func historyReward() {
        selectCategory(information: 7)
    }
    
    func nearbyReward() {
        selectCategory(information: 6)
    }
}

extension RewardController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CategoryView.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame = CGRect(
            x: 0,
            y: 0,
            width: tableView.bounds.size.width,
            height: self.tableView(tableView, heightForHeaderInSection: section)
        )
        // See the overridden `MyHeaderTableView.init(frame:)` initializer, which
        // automatically loads the view content from its nib using loadNibContent()
        let view = CategoryView(frame: frame) as! CategoryView
        view.fill(categories: category)
        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9490196078, blue: 0.9647058824, alpha: 1)
        view.events.listenTo(eventName: "category", action: selectCategory)
        view.events.listenTo(eventName: "allReward", action: allReward)
        view.events.listenTo(eventName: "historyReward", action: historyReward)
        view.events.listenTo(eventName: "nearbyReward", action: nearbyReward)
        view.updateBG(tag: currentCategory)
//        view.fillForSection(section)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentReward.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as RewardCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.fill(title: currentReward[indexPath.row].rv_name, detail: currentReward[indexPath.row].my_name, price: "ƒ\(currentReward[indexPath.row].point)", image: currentReward[indexPath.row].rv_logo, image2: #imageLiteral(resourceName: "relaxing-walk"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Reward_Detail") as? RewardDetailController {
            viewController.thisTitle = currentReward[indexPath.row].rv_name
            viewController.thisDetail = currentReward[indexPath.row].my_name
            viewController.thisImg = currentReward[indexPath.row].rv_logo
            viewController.thisID = currentReward[indexPath.row].id
            viewController.thisPoint = currentReward[indexPath.row].point
            if currentCategory == 7 {
                viewController.thisHistory = true
                viewController.thisID = redeem[indexPath.row].id
            }
            present(viewController, animated: true, completion: nil)
        }
    }
}

extension RewardController {
    func getAllReward() {
        let url = "https://api.fitmyapp.asia/api/reward/inventory/get_rewards"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        AFWrapper.requestGETURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            self.rewards.removeAll()
                                            for (index, value) in data.enumerated() {
                                                self.rewards.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                switch value["rc_my_name"].stringValue {
                                                case "MAKANAN":
                                                    self.makanan.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                case "PERKHIDMATAN":
                                                    self.perkhidmatan.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                case "PERALATAN SUKAN":
                                                    self.peralatan.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                case "PENGINAPAN":
                                                    self.penginapan.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                case "PAKAIAN":
                                                    self.pakaian.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                case "PERALATAN RUMAH":
                                                    self.rumah.append(Reward(id: value["id"].stringValue, my_name: value["my_name"].stringValue, my_description: value["my_description"].stringValue, en_name: value["en_name"].stringValue, en_description: value["en_description"].stringValue, start_datetime: value["start_datetime"].stringValue, end_datetime: value["end_datetime"].stringValue, rv_name: value["rv_name"].stringValue, rv_logo: value["rv_logo"].stringValue, rv_phone_num: value["rv_phone_num"].stringValue, rv_address: value["rv_address"].stringValue, rc_id: value["rc_id"].stringValue, c_en_name: value["c_en_name"].stringValue, rc_my_name: value["rc_my_name"].stringValue, point: value["point"].doubleValue))
                                                default:
                                                    break
                                                }
                                            }
                                            
                                            self.currentReward = self.rewards
                                            self.tableView.reloadData()
                                            print(data)
                                        } else {
                                            print("Invalid response")
                                        }
                                    }
        },
                                 failure: { (error) in
                                    print(error)
                                    
        })
    }
    
    func getRewardHistory() {
        let url = "https://api.fitmyapp.asia/api/reward/redeem/get_codes"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        print("reward history before call:", self.history)
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
//                                        print(JSON)
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            for value in data {
                                                self.redeem.append(Redeem(id: value["id"].stringValue, item_id: value["item_id"].stringValue, user_id: value["user_id"].stringValue, code: value["code"].stringValue, date_used: value["date_used"].stringValue, date_created: value["date_created"].stringValue))
                                            }
                                            
                                            self.history.removeAll()
                                            for (index, data) in self.rewards.enumerated() {
                                                for (index2, data2) in self.redeem.enumerated() {
                                                    if data.id == data2.item_id {
                                                        self.history.append(Reward(id: data.id, my_name: data.my_name, my_description: data.my_description, en_name: data.en_name, en_description: data.en_description, start_datetime: data.start_datetime, end_datetime: data.end_datetime, rv_name: data.rv_name, rv_logo: data.rv_logo, rv_phone_num: data.rv_phone_num, rv_address: data.rv_address, rc_id: data.rc_id, c_en_name: data.c_en_name, rc_my_name: data.rc_my_name, point: data.point))
                                                        break
                                                    }
//                                                    print(data2.item_id)
                                                }
                                            }
//                                            print(self.rewards)
//                                            print(self.redeem)
                                            self.tableView.reloadData()
                                            print("reward history:", self.history)
                                        } else {
                                            print("Invalid response for reward history")
                                        }
                                    }
        },
                                failure: { (error) in
                                    print(error)
                                    
        })
    }
        
        
    func getPoint() {
        let url = "https://api.fitmyapp.asia/api/user/auth/request_token"
        let header = [String:String]()
        var body = [String:String]()
        body["phone_num"] = profile.phone_num
        body["app_password"] = profile.app_password
//        print(body)
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                        UserDefaults.standard.set(data["token"]!.stringValue, forKey: "api_token")
                                            if let data2 = data["profile"]?.dictionaryValue {
                                            self.totalPoint = data2["total_points"]!.intValue
                                            self.usedPoint = data2["used_points"]!.intValue
                                            }
                                            self.updatePoint()
                                            print("Update Point: ", self.totalPoint, self.usedPoint)
                                        } else {
                                            print("Invalid response")
                                        }
                                    }
        },
                                 failure: { (error) in
                                    print(error)
                                    
        })
    }
    
    func json(from object:AnyObject) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
}

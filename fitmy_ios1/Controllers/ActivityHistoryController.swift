//
//  FitnessHistoryController.swift
//  Fit MY
//
//  Created by Azlan Shah on 22/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import Charts
import CoreMotion
import HealthKit


class ActivityHistoryController: UIViewController, ChartViewDelegate {
    
    @IBOutlet var chartView: BarChartView!
    weak var axisFormatDelegate: IAxisValueFormatter?
    @IBOutlet weak var activityScrollView: UIView!
    @IBOutlet weak var activityStackView: UIStackView!
    @IBOutlet weak var activityStackViewChildView: UIView!
    @IBOutlet weak var addSleep: UIButtonX!
    @IBOutlet weak var startPicker: UIDatePicker!
    @IBOutlet weak var endPicker: UIDatePicker!
    @IBOutlet weak var sleepPickerView: UIView!
    
    let healthStore = HKHealthStore()
    let pedometer = CMPedometer()
    var days:[String] = []
    var stepsTaken:[Int] = []
    var distanceTaken:[Double] = []
    var caloriTaken:[Double] = []
    var heartTaken:[Int] = []
    var sleepTaken:[Int] = []
    
    var startTime = Date()
    var endTime = Date()
    
    var exercise = [Exercise]()
    var currentExercise = [Exercise]()
    
    var currentGraph = 0
    var currentDate = Date().reduceDayFrom(days: 7, date: Date())
    var currentTitle = "Daily Steps"
    var installDate: Date? {
        guard
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last,
            let attributes = try? FileManager.default.attributesOfItem(atPath: documentsURL.path)
            else { return nil }
        return attributes[.creationDate] as? Date
    }
    
    var imageView = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "Bar Chart"
        chartView.alpha = 0.0
        sleepPickerView.alpha = 0.0
        addSleep.alpha = 0.0
        //        self.setup(barLineChartView: chartView)
        
        chartView.delegate = self
        
        chartView.drawBarShadowEnabled = false
        chartView.drawValueAboveBarEnabled = false
        
        currentController()
        
        startPicker.addTarget(self, action: #selector(startDateChanged(_:)), for: .valueChanged)
        endPicker.addTarget(self, action: #selector(endDateChanged(_:)), for: .valueChanged)
    }
    
    
    @IBAction func recordSleep(_ sender: Any) {
        sleepPickerView.alpha = 1.0
    }
    
    @objc func startDateChanged(_ sender: UIDatePicker) {
//        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
//        if let day = components.day, let month = components.month, let year = components.year {
//            print("\(day) \(month) \(year)")
//        }
        startTime = sender.date
    }
    
    @objc func endDateChanged(_ sender: UIDatePicker) {
        //        let components = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        //        if let day = components.day, let month = components.month, let year = components.year {
        //            print("\(day) \(month) \(year)")
        //        }
        endTime = sender.date
    }
    
    @IBAction func submitSleep(_ sender: Any) {
        saveSleepAnalysis(startTime: startTime, endTime: endTime)
        sleepPickerView.alpha = 0.0
    }
    
    @IBAction func cancelAdd(_ sender: Any) {
        sleepPickerView.alpha = 0.0
    }
    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupBar() {
        chartView.maxVisibleCount = 2000
        
        let xAxis = chartView.xAxis
        //        xAxis.enabled = false
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = days.count
        //        xAxis.valueFormatter =
        xAxis.valueFormatter = IndexAxisValueFormatter(values: days)
        
        let leftAxisFormatter = NumberFormatter()
        leftAxisFormatter.minimumFractionDigits = 0
        leftAxisFormatter.maximumFractionDigits = 1
        //        leftAxisFormatter.negativeSuffix = " $"
        //        leftAxisFormatter.positiveSuffix = " $"
        
        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = .systemFont(ofSize: 10)
        leftAxis.labelCount = 8
        leftAxis.valueFormatter = DefaultAxisValueFormatter(formatter: leftAxisFormatter)
        leftAxis.labelPosition = .outsideChart
        leftAxis.spaceTop = 0.15
        leftAxis.axisMinimum = 0 // FIXME: HUH?? this replaces startAtZero = YES
        
        let rightAxis = chartView.rightAxis
        rightAxis.enabled = false
        rightAxis.labelFont = .systemFont(ofSize: 10)
        rightAxis.labelCount = 8
        rightAxis.valueFormatter = leftAxis.valueFormatter
        rightAxis.spaceTop = 0.15
        rightAxis.axisMinimum = 0
        
        let l = chartView.legend
        l.horizontalAlignment = .left
        l.verticalAlignment = .bottom
        l.orientation = .horizontal
        l.drawInside = false
        l.form = .circle
        l.formSize = 9
        l.font = UIFont(name: "HelveticaNeue-Light", size: 11)!
        l.xEntrySpace = 4
        //        chartView.legend = l
        
        let marker = XYMarkerView(color: UIColor(white: 180/250, alpha: 1),
                                  font: .systemFont(ofSize: 12),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: chartView.xAxis.valueFormatter!)
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView.marker = marker
        
        //        self.setDataCount(Int(3) + 1, range: UInt32(3))
    }
    
    func setup(barLineChartView chartView: BarLineChartViewBase) {
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = false
        
        // ChartYAxis *leftAxis = chartView.leftAxis;
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        
        chartView.rightAxis.enabled = false
    }
    
    func setDataCount() {
        
        var entry = [BarChartDataEntry]()
        switch currentGraph
        {
        case 0:
            for (i, step) in stepsTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 1:
            for (i, step) in caloriTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 2:
            for (i, step) in distanceTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 3:
            for (i, step) in stepsTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        case 4:
            for (i, step) in stepsTaken.enumerated() {
                entry.append(BarChartDataEntry(x: Double(i), y: Double(step), data: days))
            }
        default:
            break
        }
        
        let dataSet = BarChartDataSet(entries: entry, label: currentTitle)
        dataSet.colors = ChartColorTemplates.liberty()
        let data = BarChartData(dataSets: [dataSet])
        chartView.data = data
        //        chartView.chartDescription?.text = "Step taken for past 7 days"
        
        //All other additions to this function will go here
        let xAxisValue = chartView.xAxis
        xAxisValue.valueFormatter = axisFormatDelegate
        
        //This must stay at end of function
        chartView.notifyDataSetChanged()
    }
    
    func currentController() {
        switch currentGraph
        {
        case 0:
            getStepFrom(duration: currentDate)
            currentTitle = "Daily Steps"
            addSleep.alpha = 0.0
        case 1:
            //            print("calori takda")
            getCaloriFrom(duration: currentDate)
            currentTitle = "Daily Calori Burn"
            addSleep.alpha = 0.0
        case 2:
            getDistanceFrom(duration: currentDate)
            currentTitle = "Daily Walking Distance (km)"
            addSleep.alpha = 0.0
        case 3:
            getHeartFrom(duration: currentDate)
            currentTitle = "Daily Average Hearbeat (bpm)"
            addSleep.alpha = 0.0
        case 4:
            getSleepFrom(duration: currentDate)
            currentTitle = "Daily Sleep (hour)"
            addSleep.alpha = 1.0
        default:
            break
        }
    }
    
    @IBAction func updateDuration(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            chartView.xAxis.enabled = true
            currentDate = Date().reduceDayFrom(days: 7, date: Date())
            currentController()
        case 1:
            chartView.xAxis.enabled = false
            currentDate = Date().reduceDayFrom(days: 30, date: Date())
            currentController()
        case 2:
            chartView.xAxis.enabled = false
            currentDate = Date().reduceDayFrom(days: 365, date: Date())
            currentController()
        default:
            break
        }
    }
    
    @IBAction func updateType(_ sender: UIButton) {
        currentGraph = sender.tag
        let btn = sender.superview as! UIViewX
        let v = btn.superview as! UIStackView
        for value in v.arrangedSubviews {
            let a = value as! UIViewX
            a.borderColor = #colorLiteral(red: 0.9467939734, green: 0.9468161464, blue: 0.9468042254, alpha: 1)
        }
        btn.borderColor = .orange
        
        currentController()
    }
    
}

extension ActivityHistoryController: IAxisValueFormatter {
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return days[Int(value)]
    }
}

extension ActivityHistoryController {
    func getStepFrom(duration:Date) {
        let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //daily result split
        
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            self.stepsTaken.removeAll()
            
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    let startDate = statistics.startDate
                    let sum = sum.doubleValue(for: HKUnit.count())
                    self.stepsTaken.append(Int(sum))
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "d MMM"
                    
                    self.days.append("\(formatter.string(from: startDate))")
                    resultCount += sum
                    print("\(startDate): steps = \(sum)")
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    self.setDataCount()
                    self.setupBar()
                    UIView.animate(withDuration: 0.3, delay: 0.3  , animations: {
                        self.chartView.alpha = 1.0
                    })
                    print(resultCount)
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    func getCaloriFrom(duration:Date) {
        let type = HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.activeEnergyBurned)!
        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //daily result split
        
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            self.caloriTaken.removeAll()
            self.days.removeAll()
            
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    let startDate = statistics.startDate
                    let sum = sum.doubleValue(for: HKUnit.kilocalorie())
                    self.caloriTaken.append(Double(sum))
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "d MMM"
                    
                    self.days.append("\(formatter.string(from: startDate))")
                    resultCount += sum
                    print("\(startDate): calori = \(sum)")
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    self.setDataCount()
                    self.setupBar()
                    UIView.animate(withDuration: 0.3, delay: 0.3  , animations: {
                        self.chartView.alpha = 1.0
                    })
                    print(resultCount)
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    func getDistanceFrom(duration:Date) {
        let type = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
        
        let now = Date()
        var interval = DateComponents()
        interval.day = 1 //daily result split
        
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let query = HKStatisticsCollectionQuery(quantityType: type,
                                                quantitySamplePredicate: nil,
                                                options: [.cumulativeSum],
                                                anchorDate: startOfDay,
                                                intervalComponents: interval)
        
        
        query.initialResultsHandler = { _, result, error in
            var resultCount = 0.0
            self.distanceTaken.removeAll()
            self.days.removeAll()
            
            result!.enumerateStatistics(from: startOfDay, to: now) { statistics, _ in
                
                if let sum = statistics.sumQuantity() {
                    // Get steps (they are of double type)
                    let startDate = statistics.startDate
                    let sum = sum.doubleValue(for: HKUnit.meter())
                    self.distanceTaken.append(Double(sum/1000))
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "d MMM"
                    
                    self.days.append("\(formatter.string(from: startDate))")
                    resultCount += sum
                    print("\(startDate): distance = \(sum)")
                } // end if
                
                // Return
                DispatchQueue.main.async {
                    self.setDataCount()
                    self.setupBar()
                    UIView.animate(withDuration: 0.3, delay: 0.3  , animations: {
                        self.chartView.alpha = 1.0
                    })
                    print(resultCount)
                }
            }
        }
        
        healthStore.execute(query)
    }
    
    @available(iOS 12.0, *)
    func getHeartFrom(duration:Date) {
        let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: duration)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        
        // replaced options parameter .cumulativeSum with .discreteMostRecent
        let query = HKStatisticsQuery(quantityType: heartRateType, quantitySamplePredicate: predicate, options: .discreteMostRecent) { (_, result, error) in
            var resultCount = 0
            guard let result = result else {
                print("Failed to fetch heart rate")
                print(Double(resultCount))
                return
            }
            
            // More changes here in order to get bpm value
            guard let beatsPerMinute: Double = result.mostRecentQuantity()?.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute())) else { return }
            resultCount = Int(beatsPerMinute)
            
            DispatchQueue.main.async {
                print(Double(resultCount))
            }
        }
        healthStore.execute(query)
    }
    
    func saveSleepAnalysis(startTime: Date, endTime: Date) {
        
        // alarmTime and endTime are NSDate objects
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            
            // we create our new object we want to push in Health app
            let object = HKCategorySample(type:sleepType, value: HKCategoryValueSleepAnalysis.inBed.rawValue, start: startTime, end: endTime)
            
            // at the end, we save it
            healthStore.save(object, withCompletion: { (success, error) -> Void in
                
                if error != nil {
                    // something happened
                    return
                }
                
                if success {
                    print("My new data was saved in HealthKit")
                    
                } else {
                    // something happened again
                }
                
            })
            
            
            let object2 = HKCategorySample(type:sleepType, value: HKCategoryValueSleepAnalysis.asleep.rawValue, start: startTime, end: endTime)
            
            healthStore.save(object2, withCompletion: { (success, error) -> Void in
                if error != nil {
                    // something happened
                    return
                }
                
                if success {
                    print("My new data (2) was saved in HealthKit")
                } else {
                    // something happened again
                }
                
            })
            
        }
        
    }
    
    func getSleepFrom(duration:Date) {
        
        // first, we define the object type we want
        if let sleepType = HKQuantityType.categoryType(forIdentifier: .sleepAnalysis) {
            
            // Use a sortDescriptor to get the recent data first
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            
            // we create our query with a block completion to execute
            let query = HKSampleQuery(sampleType: sleepType, predicate: nil, limit: 3000, sortDescriptors: [sortDescriptor]) { (query, tmpResult, error) -> Void in
                
                if error != nil {
                    
                    // something happened
                    return
                    
                }
                
                if let result = tmpResult {
                    
                    // do something with my data
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            let value = (sample.value == HKCategoryValueSleepAnalysis.inBed.rawValue) ? "InBed" : "Asleep"
                            print("Healthkit sleep: \(sample.startDate) \(sample.endDate) - sleep: \(value)")
                            if sample.endDate.compareDate(toDate: result[0].endDate) {
                                print("Same day")
                            } else {
                                print("next day")
                            }
                        }
                    }
                }
            }
            
            // finally, we execute our query
            healthStore.execute(query)
        }
    }
}

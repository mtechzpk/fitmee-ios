//
//  LoginSplitController.swift
//  Fit MY
//
//  Created by Azlan Shah on 12/11/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class LoginSplitController: UIViewController {

    @IBOutlet weak var areYouLbl: UILabel!
    @IBOutlet weak var orLbl: UILabel!
    @IBOutlet weak var firstTimeBtn: UIButtonX!
    @IBOutlet weak var existingBtn: UIButtonX!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        localizeLabel()
    }
    
    func localizeLabel() {
        areYouLbl.text = "Are you ..".localized()
        orLbl.text = "- OR -".localized()
        firstTimeBtn.setTitle("FIRST TIME USER".localized(), for: .normal)
        existingBtn.setTitle("EXISTING USER".localized(), for: .normal)
    }

}

//
//  StepsAndCalorieVC.swift
//  Fit MY
//
//  Created by Mahad on 3/24/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

struct ActivityModel {
    var id = String()
    var steps = String()
    var start_date = String()
    var end_date = String()
    var activity_type = String()
    var kcal = String()
}


class StepsAndCalorieVC: UIViewController {

    // MARK: - Properties
    var activityArray = [ActivityModel]()
    var thisTitle = String()
    
    var isSteps:Bool = false
    
    // MARK: - IBOutlets
    
    @IBOutlet var headerLbl: UILabel!
    @IBOutlet var tableView: UITableView!
    
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerLbl.text = thisTitle.localized()
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if isSteps
        {
            getTotalSteps()
            
        }
        else
        {
            getTotalCalories()
        }
    }
    
    
    // MARK: - Set up
    
    func fill(title: String) {
        thisTitle = title
    }
    
    // MARK: - IBActions
    
    @IBAction func backBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    
    func getTotalSteps() {
        let url = "http://172.104.217.178/fitmy/public/api/get_total_steps"
        var header = [String:String]()
        header["Accept"] = "application/json"
        
        var body = [String:Any]()
        body["user_id"] = userModel.id //UserDefaults.standard.string(forKey: "user_id") // UserID
        
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { (Response) in
            let responseData = Response.dictionaryValue
            if(responseData["status"] == 200)
            {
                let data = responseData["data"]?.arrayValue
                
                for obj in data ?? []
                {
                    self.activityArray.append(ActivityModel(id: obj["id"].stringValue, steps: obj["steps"].stringValue, start_date: obj["start_date"].stringValue, end_date: obj["end_date"].stringValue, activity_type: obj["activity_type"].stringValue, kcal: ""))
                }
                self.tableView.reloadData()
                
            }
            else
            {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: responseData["message"]?.stringValue ?? "Registration Failed!", Sender: self)
            }
            
            
        }) { (error) in
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: error.localizedDescription, Sender: self)
            return
        }
    }
    
    func getTotalCalories() {
        let url = "http://172.104.217.178/fitmy/public/api/get_total_cals"
        var header = [String:String]()
        header["Accept"] = "application/json"
        
        var body = [String:Any]()
        body["user_id"] = userModel.id //UserDefaults.standard.string(forKey: "user_id") // UserID
        
        AFWrapper.requestPOSTURL(url, params: body as [String : AnyObject], headers: header, success: { (Response) in
            let responseData = Response.dictionaryValue
            if(responseData["status"] == 200)
            {
                let data = responseData["data"]?.arrayValue
                
                for obj in data ?? []
                {
                    self.activityArray.append(ActivityModel(id: obj["id"].stringValue, steps: "", start_date: obj["start_date"].stringValue, end_date: obj["end_date"].stringValue, activity_type: obj["activity_type"].stringValue, kcal: obj["kcal"].stringValue))
                }
                self.tableView.reloadData()
                
            }
            else
            {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: responseData["message"]?.stringValue ?? "Registration Failed!", Sender: self)
            }
            
            
        }) { (error) in
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: error.localizedDescription, Sender: self)
            return
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: - Extensions

extension StepsAndCalorieVC:UITableViewDataSource,UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.activityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let stepsandCalCell:StepsAndCalorieTVC = tableView.dequeueReusableCell(withIdentifier: "StepsAndCalorieTVC", for: indexPath) as! StepsAndCalorieTVC
        
        let data = activityArray[indexPath.row]
        
        var imageView = UIImage()
        
        switch data.activity_type{
        case "Walking":
            imageView = #imageLiteral(resourceName: "relaxing-walk")
        case "Running":
            imageView = #imageLiteral(resourceName: "man-sprinting")
        case "Stairs Climbing":
            imageView = #imageLiteral(resourceName: "man-climbing-stairs")
        case "Cycling":
            imageView = #imageLiteral(resourceName: "cyclist")
        case "Hiking":
            imageView = #imageLiteral(resourceName: "hiking")
        case "Swimming":
            imageView = #imageLiteral(resourceName: "swimming-figure")
        case "Aerobic":
            imageView = #imageLiteral(resourceName: "exercising-silhouette")
        case "Indoor Running":
            imageView = #imageLiteral(resourceName: "person-running-on-a-treadmill-silhouette-from-side-view")
        default:
            break
        }
        stepsandCalCell.fill(title: data.activity_type, date: data.start_date, value: isSteps == true ?"\(data.steps) Steps":"\(data.kcal) Calories", image: "", image2: imageView)
        return stepsandCalCell
    }
    
    
}

//
//  StepsAndCalorieTVC.swift
//  Fit MY
//
//  Created by Mahad on 3/24/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class StepsAndCalorieTVC: UITableViewCell {

    @IBOutlet var backView: UIView!
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var valLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backView.layer.cornerRadius = 10
        imgView.contentMode = .center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fill(title: String, date: String, value: String, image: String, image2: UIImage) {
        self.titleLbl.text = title
        self.dateLbl.text = date
        self.valLbl.text = value
        if image.isEmpty {
            self.imgView.image = image2
        } else {
            self.imgView.kf.setImage(with: URL(string: image), options: [.transition(.fade(0.3))])
        }
    }
}

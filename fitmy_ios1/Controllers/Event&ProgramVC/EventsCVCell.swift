//
//  EventsCVCell.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class EventsCVCell: UICollectionViewCell {
    @IBOutlet weak var programImage: UIImageView!
    @IBOutlet weak var programLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
}

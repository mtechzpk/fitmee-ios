//
//  EventRegisterVC.swift
//  Fit MY
//
//  Created by Mahad on 3/11/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class EventRegisterVC: UIViewController {

    
    // MARK: - Properties
    var allevent:allEvents?
    
    // MARK: - IBOutlets
    
    @IBOutlet var backimgView: UIImageView!
    @IBOutlet var frontimgView: UIImageView!
    
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var locationLbl: UILabel!
    
    @IBOutlet var descLbl: UILabel!
    
    @IBOutlet var termsLbl: UILabel!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismisscurrentView(gesture:)))
               slideDown.direction = .right
               view.addGestureRecognizer(slideDown)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let event = allevent {
             backimgView.kf.setImage(with: URL(string: event.filename))
            frontimgView.kf.setImage(with: URL(string: event.filename))
            
            titleLbl.text = event.event_name
            dateLbl.text = event.event_date
            locationLbl.text = event.location
            descLbl.text = event.event_description
            termsLbl.text = event.term_and_conditions
            
        }
    }
    // MARK: - Set up
    
    @objc func dismisscurrentView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - IBActions
    
    @IBAction func registorBtn_Click(_ sender: Any) {
        if let event = allevent {
            
            UIApplication.shared.open(URL(string: event.registration_url)!, options: [:], completionHandler: nil)
            
//            UIApplication.shared.openURL(URL(string: event.registration_url)!)
        }
        
    }
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    // MARK: - Extensions
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  EventAndProgramsVC.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit
import Kingfisher

class EventAndProgramsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Arrays
    var arrayImage:[String] = []
    var arrayCategories = [allCategories]()
    var arrayEvents = [allEvents]()
    
    var isCategoryFilter:Bool = false
    var filterarrayEvents = [allEvents]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCategories()
        getNews()
        searchView.layer.cornerRadius = 7
        addBottomBorderTo(textfield: searchTF)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNews()
        getCategories()
    }
    
    //MARK:- Functions
    func addBottomBorderTo(textfield: UITextField){
        let layer = CALayer()
        layer.backgroundColor = UIColor.gray.cgColor
        layer.frame = CGRect(x: 0.0, y: textfield.frame.size.height - 2.0, width: textfield.frame.size.width, height: 2.0)
        textfield.layer.addSublayer(layer)
    }
    func getCategories(){
        let header:[String:String] = ["Accept":"application/json"]
        let url = "http://172.104.217.178/fitmy/public/api/get_event_categories"
        AFWrapper.requestGETURL(url, params: nil, headers: header, success: { response in
            let json = response.dictionaryValue
            let data = json["data"]?.arrayValue
            self.arrayCategories.removeAll()
            if let responseData = data{
                for i in responseData{
                    let id = i["id"].intValue
                    let name = i["name"].stringValue
                    let filename = i["filename"].stringValue
                    
                    let obj = allCategories(id: id, name: name, filename: filename)
                    self.arrayCategories.append(obj)
                }
                self.collectionView.reloadData()
            }
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func getNews(){
        let header:[String:String] = ["Accept":"application/json"]
        let url = "http://172.104.217.178/fitmy/public/api/get_all_events"
        AFWrapper.requestGETURL(url, params: nil, headers: header, success: { response in
            let json = response.dictionaryValue
            //print(json)
            let data = json["data"]?.arrayValue
            self.arrayEvents.removeAll()
            if let responseData = data{
                for i in responseData{
                    let id = i["id"].intValue
                    let event_name = i["event_name"].stringValue
                    let description = i["description"].stringValue
                    let location = i["location"].stringValue
                    let event_date = i["event_date"].stringValue
                    let filename = i["filename"].stringValue
                    let term_and_conditions = i["term_and_conditions"].stringValue
                    let registration_url = i["registration_url"].stringValue
                    let event_category_id = i["event_category_id"].intValue
                    
                    let obj = allEvents(id: id, event_name: event_name, event_description: description, location: location, event_date: event_date, filename: filename, term_and_conditions: term_and_conditions, registration_url: registration_url, event_category_id: event_category_id)
                    self.arrayEvents.append(obj)
                }
                self.tableView.reloadData()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
     @IBAction func menuBtn_Click(_ sender: Any) {
            
            
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController:SideMenuViewController = storyboard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            
            var frm = initialViewController.view.frame
            frm = self.view.frame
            initialViewController.view.frame = frm
            
            initialViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
    //        self.view.addSubview(initialViewController.view)
            initialViewController.viewController = self
            self.present(initialViewController, animated: false, completion: nil)
    //        self.navigationController?.pushViewController(initialViewController, animated: false)
        }
}


extension EventAndProgramsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCategories.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "EventsCVCell", for: indexPath) as! EventsCVCell
        cell.bgView.layer.cornerRadius = 7
        
        if indexPath.row == 0{
            cell.programLabel.text = "All"
            cell.programImage.image = UIImage(named: "music_menu_icon")
        } else {
            cell.programImage.kf.setImage(with: URL(string: arrayCategories[indexPath.row - 1].filename))
            cell.programLabel.text = arrayCategories[indexPath.row - 1].name
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            isCategoryFilter = false
            tableView.reloadData()
        }
        else
        {
            let catid = arrayCategories[indexPath.row-1].id
            isCategoryFilter = true
            filterarrayEvents = arrayEvents.filter{$0.event_category_id == catid}
            tableView.reloadData()
        }
    }
}


extension EventAndProgramsVC: UITableViewDelegate, UITableViewDataSource{
    
    // MARK: - DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isCategoryFilter
        {
            return filterarrayEvents.count
        }
        return arrayEvents.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventsTBVCell", for: indexPath) as! EventsTBVCell
        
        var event = arrayEvents[indexPath.row]
               
               if isCategoryFilter
               {
                event = filterarrayEvents[indexPath.row]
               }
        
        cell.eventImage.kf.setImage(with: URL(string: event.filename))
        cell.eventName.text = event.event_name
        cell.eventDescription.text = event.event_description
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    // MARK: - Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let eventRegisterVC:EventRegisterVC = storyboard.instantiateViewController(withIdentifier: "EventRegisterVC") as! EventRegisterVC
        
        eventRegisterVC.allevent = isCategoryFilter == true ? filterarrayEvents[indexPath.row] : arrayEvents[indexPath.row]
        self.navigationController?.pushViewController(eventRegisterVC, animated: true)
        
    }
}

//
//  EventAndProgramsModel.swift
//  Fit MY
//
//  Created by apple on 3/6/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import Foundation

struct allCategories{
    var id:Int
    var name:String
    var filename:String
}

struct allEvents{
    var id:Int
    var event_name:String
    var event_description:String
    var location:String
    var event_date:String
    var filename:String
    var term_and_conditions:String
    var registration_url:String
    var event_category_id:Int
}

//
//  ProfileController.swift
//  Fit MY
//
//  Created by Azlan Shah on 30/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol isAbleToReceiveData {
    func pass(data: Bool)  //data: string is an example parameter
}

class ProfileController: UIViewController, isAbleToReceiveData, NVActivityIndicatorViewable {
    func pass(data: Bool) {
        if data {
            verify = data
            verifyBtn.backgroundColor = .clear
            verifyBtn.setTitleColor(.green, for: .normal)
            verifyBtn.setTitle("Verified", for: .normal)
            verifyBtn.isEnabled = false
            verify = true
            
            phoneNumberField.isEnabled = false
        }
    }
    
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var nickNameLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var emelAddressLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var birthYearLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var raceLbl: UILabel!
    @IBOutlet weak var nationalityLbl: UILabel!
    @IBOutlet weak var nricLbl: UILabel!
    
    func localizeLabel() {
        fullNameLbl.text = "Full Name".localized()
        nickNameLbl.text = "Nick Name".localized()
        phoneNumberLbl.text = "Phone Number".localized()
        emelAddressLbl.text = "Email Address".localized()
        heightLbl.text = "Height".localized()
        weightLbl.text = "Weight".localized()
        birthYearLbl.text = "Birth Year".localized()
        genderLbl.text = "Gender".localized()
        raceLbl.text = "Race".localized()
        nationalityLbl.text = "Nationality".localized()
        nricLbl.text = "NRIC".localized()
        verifyBtn.setTitle("Verify".localized(), for: .normal)
        genderBtn.setTitle("Change".localized(), for: .normal)
        raceBtn.setTitle("Change".localized(), for: .normal)
        backBtn.setTitle("Back".localized(), for: .normal)
        updateBtn.setTitle("Update Profile".localized(), for: .normal)
    }
    

    @IBOutlet weak var fullNameField: UITextField!
    @IBOutlet weak var nickNameField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var heightField: UITextField!
    @IBOutlet weak var weightField: UITextField!
    @IBOutlet weak var birthYearField: UITextField!
    @IBOutlet weak var genderField: UITextField!
    @IBOutlet weak var raceField: UITextField!
    @IBOutlet weak var nationalityField: UITextField!
    @IBOutlet weak var nricField: UITextField!
    @IBOutlet weak var verifyBtn: UIButtonX!
    @IBOutlet weak var genderBtn: UIButtonX!
    @IBOutlet weak var raceBtn: UIButtonX!
    @IBOutlet weak var backBtn: UIButtonX!
    @IBOutlet weak var updateBtn: UIButtonX!
    
    var profile = Profile()
    var verify = false
    var otp = Int()
    
    var race = ["Malay".localized(), "Chinese".localized(), "Indian".localized(), "Other".localized()]
    var gender = ["Male".localized(), "Female".localized()]
    
    var currentGender = "M"
    
    var activityIndicatorView: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicatorView = NVActivityIndicatorView(frame: view.frame, type: .ballRotateChase, color: .white, padding: nil)
        
        localizeLabel()
        if let data = UserDefaults.standard.object(forKey: "user_profile") as? Data {
            let decoder = JSONDecoder()
            if let profileData = try? decoder.decode(Profile.self, from: data) {
                profile = profileData
            }
        }
        fullNameField.text = profile.full_name
        nickNameField.text = profile.nick_name
        emailField.text = profile.email
        heightField.text = String(describing: profile.height_cm)
        weightField.text = String(describing: profile.weight_kg)
        birthYearField.text = String(describing: profile.birth_year)
        genderField.text = profile.gender
        raceField.text = profile.race
        UserDefaults.standard.value(forKey: "user_nric")
        if profile.phone_num.count < 32 {
            verifyBtn.backgroundColor = .clear
            verifyBtn.setTitleColor(.green, for: .normal)
            verifyBtn.setTitle("Verified".localized(), for: .normal)
            verifyBtn.isEnabled = false
            verify = true
            
            phoneNumberField.isEnabled = false
            phoneNumberField.text = profile.phone_num
        }
        switch genderField.text {
        case "M": genderField.text = gender[0].localized();currentGender = "M"
        case "F": genderField.text = gender[1].localized();currentGender = "F"
        default: genderField.text = gender[0].localized();currentGender = "M"
        }
        
//        let dropdown : DropDown = UITextField(frame: raceField.frame) as! DropDown
        
//        let  dropDown = DropDown(frame: raceField.frame) // set frame
//
//        // The list of array to display. Can be changed dynamically
//        dropDown.optionArray = ["Malay", "Chinese", "Indian", "Others"]
//        // Its Id Values and its optional
//        dropDown.optionIds = [0,1,2,3]
//
//        // The the Closure returns Selected Index and String
//        dropDown.didSelect{(selectedText , index ,id) in
//            self.raceField.text = "\(selectedText)"
//        }
//        raceField.isEnabled = false
//        raceField.addSubview(dropDown)
    }
    
    @IBAction func verifyPhone(_ sender: Any) {
        startAnimating()
        getOTP()
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateProfile(_ sender: UIButtonX) {
        if verify {
            startAnimating()
            updateProfile()
        } else {
            showAlert(title: "Verification Alert".localized(), message: "Please verify phone number to update your profile".localized())
        }
    }
    
    @IBAction func changeRace(_ sender: Any) {
        switch raceField.text {
        case "": raceField.text = race[0]
        case "Malay".localized(): raceField.text = race[1]
        case "Chinese".localized(): raceField.text = race[2]
        case "Indian".localized(): raceField.text = race[3]
        case "Other".localized(): raceField.text = race[0]
        default: raceField.text = race[0]
        }
    }
    
    @IBAction func changeGender(_ sender: Any) {
        switch genderField.text {
        case "Male".localized(): genderField.text = gender[1].localized();currentGender = "F"
        case "Female".localized(): genderField.text = gender[0].localized();currentGender = "M"
        default: genderField.text = gender[0].localized();currentGender = "M"
        }
        print(currentGender, genderField.text)
    }
    
    func saveToLocal() {
        profile.full_name = fullNameField.text ?? profile.full_name
        profile.nick_name = nickNameField.text ?? profile.nick_name
        profile.phone_num = phoneNumberField.text ?? profile.phone_num
        profile.email = emailField.text ?? profile.email
        if let height = Double(heightField.text!) {
            profile.height_cm = height
        }
        if let weight = Double(weightField.text!) {
            profile.weight_kg = weight
        }
        if let year = Int(birthYearField.text!) {
            profile.birth_year = year
        }
        profile.gender = genderField.text ?? profile.gender
        profile.race = raceField.text ?? profile.race
        profile.nationality = nationalityField.text ?? profile.nationality
        UserDefaults.standard.setValue(nricField.text, forKey: "user_nric")
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(profile) {
            UserDefaults.standard.set(encoded, forKey: "user_profile")
        }
    }
    
    func gotoLogin(otp : Int) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Verify") as? VerifyController {
            viewController.phone_num = phoneNumberField.text!
            viewController.otp = otp
            viewController.delegate = self
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verify_user" {
            if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Verify") as? VerifyController {
                viewController.phone_num = phoneNumberField.text!
                viewController.otp = self.otp
                viewController.delegate = self
                self.present(viewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}


extension ProfileController {
    func getOTP() {
        let url = "https://api.fitmyapp.asia/api/user/profile/verify_phone"
        var header = [String:String]()
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        var body = [String:String]()
        if let phone_num = phoneNumberField.text {
            body["phone_num"] = phone_num
        }
        body["user_password"] = ""
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            print("OTP:", data["user_password"]!.intValue)
                                            self.gotoLogin(otp: data["user_password"]!.intValue)
                                        } else {
                                            print("Get OTP:", JSON)
                                        }
                                        self.stopAnimating()
                                    }
        },
                                 failure: { (error) in
                                    print(error)
                                    self.stopAnimating()
                                    self.showAlert(title: "Invalid Number", message: "The phone number inserted is either invalid or already registered with FitMY")
        })
        
    }
    
    func updateProfile() {
        let url = "https://api.fitmyapp.asia/api/user/profile/update_profile"
        var header = [String:String]()
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        var body = [String:String]()
        if let token = UserDefaults.standard.value(forKey: "total_points") as? String {
            body["total_points"] = token
        }
        body["birth_year"] = birthYearField.text
        body["full_name"] = fullNameField.text
        body["nick_name"] = nickNameField.text
        body["height_cm"] = heightField.text
        body["weight_kg"] = weightField.text
        body["nationality"] = nationalityField.text
        body["race"] = raceField.text
        body["gender"] = currentGender
        body["email"] = emailField.text
        if let token = UserDefaults.standard.value(forKey: "total_steps") as? String{
            body["total_steps"] = token
        }
        if let token = UserDefaults.standard.value(forKey: "birth_date") as? String {
            body["birth_date"] = token
        }
        if let token = UserDefaults.standard.value(forKey: "photo") as? String {
            body["photo"] = token
        }
        if let token = UserDefaults.standard.value(forKey: "id") as? String {
            body["id"] = token
        }
        if let token = UserDefaults.standard.value(forKey: "used_points") as? String {
            body["used_points"] = token
        }
        body["phone_num"] = phoneNumberField.text
        if let token = UserDefaults.standard.value(forKey: "user_password") as? String {
            body["user_password"] = token
        }
        if let token = UserDefaults.standard.value(forKey: "app_password") as? String {
            body["app_password"] = token

        }
        if let token = UserDefaults.standard.value(forKey: "date_created") as? String {
            body["date_created"] = token
        }
        
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        self.saveToLocal()
                                        print(JSON)
                                        let data = JSON.dictionaryValue
                                        if data.count > 0 {
                                            print("Profile Response:", data)
                                            self.showAlert(title: "Update Success", message: "Profile successfully updated. ")
                                        } else {
                                            print("Update Profile:", JSON)
                                        }
                                        self.stopAnimating()
                                        self.dismiss(animated: true, completion: nil)
                                    }
        },
                                 failure: { (error) in
                                    self.stopAnimating()
                                    print(error)
                                    
        })
        
    }
    
}



extension ProfileController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTag = textField.tag + 1
        // Try to find next responder
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder?
        
        if nextResponder != nil {
            // Found next responder, so set it
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        
        return false
    }
}

//
//  BookingConfirmController.swift
//  Fit MY
//
//  Created by Azlan Shah on 12/12/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class BookingConfirmController: UIViewController {

    @IBOutlet weak var facilityItemStackView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func done(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    

}

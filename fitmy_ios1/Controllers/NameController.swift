//
//  NameController.swift
//  Fit MY
//
//  Created by Azlan Shah on 26/09/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit

class NameController: UIViewController {

    @IBOutlet weak var profileLbl: UILabel!
    @IBOutlet weak var introduceLbl: UILabel!
    @IBOutlet weak var fullNameLbl: UILabel!
    @IBOutlet weak var nickNameLbl: UILabel!
    @IBOutlet weak var continueBtn: UIButtonX!
    @IBOutlet weak var backBtn: UIButtonX!
    
    func localizeLabel() {
        profileLbl.text = "PROFILE".localized()
        introduceLbl.text = "Introduce Yourself".localized()
        fullNameLbl.text = "FULL NAME".localized()
        nickNameLbl.text = "NICK NAME".localized()
        continueBtn.setTitle("CONTINUE".localized(), for: .normal)
        backBtn.setTitle("BACK".localized(), for: .normal)
    }
    
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        localizeLabel()
    }
    
    @IBAction func next(_ sender: Any) {
        if checkField() {
            performSegue(withIdentifier: "to_bmi", sender: self)
        } else {
            print("first n last name cannot be empty")
            showAlert(title: "Incomplete Form", message: "Please fill in full name and nick name")
        }
        
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkField() -> Bool {
        if !firstNameField.text.isBlank && !lastNameField.text.isBlank {
            return true
        } else {
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "to_bmi" {
            if let nextViewController = segue.destination as? RegisterController {
                nextViewController.fullName = firstNameField.text!
                nextViewController.nickName = lastNameField.text!
            }
        }
    }

    // MARK: Helper Functions
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension String {
    var isBlank: Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
}
extension Optional where Wrapped == String {
    var isBlank: Bool {
        if let unwrapped = self {
            return unwrapped.isBlank
        } else {
            return true
        }
    }
}


extension NameController: UITextFieldDelegate {
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let nextTag = textField.tag + 1
        // Try to find next responder
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder?
        
        if nextResponder != nil {
            // Found next responder, so set it
            nextResponder?.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard
            textField.resignFirstResponder()
        }
        
        return false
    }
}

//
//  selectLanguageVC.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit
import DropDown
import Localize_Swift
class selectLanguageVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var languageTF: UITextField!
    
    //MARK:- Arrays
    var arrayBGImages:[String] = ["bg_04","bg_05"]
        
    //MARK:- Variables
    var currentIndex:Int = 0
    var timer: Timer?
    var languageDropDown = DropDown()
    
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view.
            startTimer()
            languageDDFunc()
        }
        //MARK:- Actions
    @IBAction func continueBtnClicked(_ sender: UIButton) {
        
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        timer?.invalidate()
        var currentLanguage = ""
        if self.languageTF.text == "English"{
            currentLanguage = "en"
        }
        else
        {
            currentLanguage = "ms"
        }
        UserDefaults.standard.set(currentLanguage, forKey: "AppLanguage")
        UserDefaults.standard.synchronize()
        Localize.setCurrentLanguage(currentLanguage)
        let vc:LoginOrSignupVC = storyboard1.instantiateViewController(withIdentifier: "LoginOrSignupVC") as! LoginOrSignupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
        //MARK:- Functions
        func startTimer(){
            timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
        @objc func timerAction(){
            let desiredScrollPosition = (currentIndex < arrayBGImages.count - 1) ? currentIndex + 1 : 0
            collectionView.scrollToItem(at: IndexPath(item: desiredScrollPosition, section: 0), at: .centeredHorizontally, animated: true)
        }

    }
    extension selectLanguageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return arrayBGImages.count
        }
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectLanguageCVCell", for: indexPath) as! selectLanguageCVCell
            cell.image = UIImage(named: arrayBGImages[indexPath.row])
            return cell
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return collectionView.frame.size
        }
        
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            currentIndex = Int(scrollView.contentOffset.x / collectionView.frame.size.width)
//            print(currentIndex)
        }
    }
extension selectLanguageVC{
    func languageDDFunc(){
        let tapState = UITapGestureRecognizer(target: self, action: #selector(self.handleLanguageUnitDD(_:)))
        languageTF.addGestureRecognizer(tapState)
        languageDropDown.anchorView = languageTF
        languageDropDown.dataSource = ["English", "Bahasa Malaysia"]
    }
    @objc func handleLanguageUnitDD(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        languageDropDown.show()
        languageDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            var currentLanguage = ""
            if(item == "English")
            {
                currentLanguage = "en"
            }
            else
            {
                currentLanguage = "ms"
            }
            UserDefaults.standard.set(currentLanguage, forKey: "AppLanguage")
            Localize.setCurrentLanguage(currentLanguage)
            self.languageTF.text = item
        }
    }
}

//
//  dashboardVC.swift
//  Fit MY
//
//  Created by apple on 3/6/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class dashboardVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var totalSteps: UILabel!
    @IBOutlet weak var totalKilometers: UILabel!
    @IBOutlet weak var totalCalories: UILabel!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var heartLabel: UILabel!
    @IBOutlet weak var sleepLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        getFitnessDiary()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getFitnessDiary()
    }
    
    //MARK:- Fucntions
    func getFitnessDiary(){
        let param:Parameters = ["user_id":UserDefaults.standard.string(forKey: "user_id")]
        let header:HTTPHeaders = ["Accept":"application/json"]
        Alamofire.request("http://172.104.217.178/fitmy/public/api/fitness_diary", method: .post, parameters: param, headers: header).responseData { response in
            switch response.result{
            case.success(let value):
                let json = JSON(value)
                let data = json["data"].dictionaryValue
                if let total_steps = data["total_steps"]?.stringValue{
                    self.totalSteps.text = total_steps
                }
                if let avg_distance = data["total_distance"]?.stringValue{
                    self.totalKilometers.text = avg_distance
                }
                if let avg_kcales = data["total_kcals"]?.stringValue{
                    self.totalCalories.text = avg_kcales
                }
                if let avg_bpm = data["average_bpm"]?.stringValue{
                    //self.totalSteps.text = avg_bpm
                }
                if let avg_sleep = data["average_sleep"]?.stringValue{
                    //self.totalSteps.text = avg_sleep
                }
            case.failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

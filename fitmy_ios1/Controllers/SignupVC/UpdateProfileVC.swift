//
//  UpdateProfileVC.swift
//  Fit MY
//
//  Created by Mahad on 3/12/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class UpdateProfileVC: UIViewController {

    
    // MARK: - Properties
    
    let dobDatePicker = UIDatePicker();
    
    // MARK: - IBOutlets
    
    @IBOutlet var fullnameTxt: UITextField!
    
    @IBOutlet var emailTxt: UITextField!
    
    @IBOutlet var phonenoTxt: UITextField!
    @IBOutlet var dobTxt: customizeTF!
    
//    @IBOutlet var citizenshipTxt: UITextField!
//
//    @IBOutlet var nationailtyTxt: UITextField!
    @IBOutlet var heightTxt: UITextField!
    @IBOutlet var weightTxt: UITextField!
    @IBOutlet var languageTxt: UITextField!
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissProfileView(gesture:)))
        slideDown.direction = .right
        view.addGestureRecognizer(slideDown)
        
        dobDatePicker.addTarget(self, action: #selector(self.updateDOBField), for: UIControl.Event.valueChanged)
        dobDatePicker.datePickerMode =  UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy" //format style. Browse online to get a format that fits your needs.
//        self.dobTF.text =
//            dateFormatter.string(from: userModel.address)
        dobDatePicker.date = dateFormatter.date(from: userModel.address) ?? Date()
        dobTxt.inputView = dobDatePicker
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fillFields()
    }
    
    // MARK: - Set up
    
    @objc func updateDOBField(sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy" //format style. Browse online to get a format that fits your needs.
        self.dobTxt.text = dateFormatter.string(from: sender.date)
        //        self.datePickerCheckOut.minimumDate = sender.date
    }
    
    @objc func dismissProfileView(gesture: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func fillFields()  {
        fullnameTxt.text = userModel.full_name
        emailTxt.text = userModel.email
        phonenoTxt.text = userModel.phone
        dobTxt.text = userModel.address
        //citizenshipTxt.text = userModel.citizenship
        //nationailtyTxt.text = userModel.nation
        heightTxt.text = userModel.height
        weightTxt.text = userModel.weight
        languageTxt.text = "English"
    }
    
    // MARK: - IBActions
    
    @IBAction func updateProfileBtn_Click(_ sender: Any) {
        
        UpDateProfile()
    }
    
    
    // MARK: - Navigation
    // MARK: - Network Manager calls
    func UpDateProfile()  {
            
            if fullnameTxt.text == "" {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Full Name is Empty", Sender: self)
                return
            }
            
            if emailTxt.text == "" {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Email is Empty", Sender: self)
                return
            }
            
            if dobTxt.text == "" {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Date of Birth is Empty", Sender: self)
                return
            }
            
//            if nationailtyTxt.text == "" {
//                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Nation is Empty", Sender: self)
//                return
//            }
            
//            if citizenshipTxt.text == "" {
//                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Citizenship is Empty", Sender: self)
//                return
//            }
            if phonenoTxt.text == "" {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Phone Number is Empty", Sender: self)
                return
            }
            if heightTxt.text == "" {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Height is Empty", Sender: self)
                return
            }
            if weightTxt.text == "" {
                AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Weight is Empty", Sender: self)
                return
            }
            
            let url = "http://172.104.217.178/fitmy/public/api/update_profile"
            var header = [String:String]()
            header["Accept"] = "application/json"
            var body = [String:Any]()
            body["full_name"] = fullnameTxt.text!
            body["email"] = emailTxt.text!
            body["phone"] = phonenoTxt.text!
            body["nation"] = ""
            body["citizenship"] = ""
            body["height"] = heightTxt.text!
            body["weight"] = weightTxt.text!
            body["address"] = dobTxt.text!
            body["user_id"] = userModel.id
            AFWrapper.requestPOSTURL(url,
                                     params: body as [String : AnyObject],
                                     headers: header,
                                     success: { (JSON) in
                                        DispatchQueue.main.async {
                                            let data = JSON.dictionaryValue
                                            if(data["status"] == 200)
                                            {
                                                if let user = data["data"]?.dictionaryValue
                                                {
                                                    userModel.id = user["id"]!.intValue
                                                    userModel.user_type = user["user_type"]!.stringValue
                                                    userModel.category_type = user["category_type"]!.stringValue
                                                    userModel.full_name = user["full_name"]!.stringValue
                                                    userModel.email = user["email"]!.stringValue
                                                    userModel.email_verified_at = user["email_verified_at"]!.stringValue
                                                    userModel.image = user["image"]!.stringValue
                                                    userModel.phone = user["phone"]!.stringValue
                                                    userModel.gender = user["gender"]!.stringValue
                                                    userModel.height = user["height"]!.stringValue
                                                    userModel.weight = user["weight"]!.stringValue
                                                    userModel.nation = user["nation"]!.stringValue
                                                    userModel.citizenship = user["citizenship"]!.stringValue
                                                    userModel.office_tel_no = user["office_tel_no"]!.stringValue
                                                    userModel.office_fax_no = user["office_fax_no"]!.stringValue
                                                    userModel.address = user["address"]!.stringValue
                                                    userModel.country = user["country"]!.stringValue
                                                    userModel.state = user["state"]!.stringValue
                                                    userModel.city = user["city"]!.stringValue
                                                    userModel.zip_code = user["zip_code"]!.stringValue
                                                    userModel.type = user["type"]!.stringValue
                                                    userModel.fitness_points = user["fitness_points"]!.stringValue
                                                }
                                                
                                                
                                                
                                                AlertHelper.showSuccessAlert(WithTitle: "Success", Message: data["message"]?.stringValue ?? "Profile Updated Successfully!", Sender: self)
//                                                AlertHelper.showSuccessAlert(WithTitle: "Success", Message: "", Sender: <#T##UIViewController#>)
//                                                self.navigationController?.popViewController(animated: true)
                                            }
                                            else
                                            {
                                                AlertHelper.showErrorAlert(WithTitle: "Error", Message: data["message"]?.stringValue ?? "Registration Failed!", Sender: self)
                                            }
                                            
                                            
                                        }
                                        
            },
                                     failure: { (error) in
                                        print(error)
                                        AlertHelper.showErrorAlert(WithTitle: "Error", Message: error.localizedDescription, Sender: self)
                                        return
                                        
            })
        }
    // MARK: - Extensions

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

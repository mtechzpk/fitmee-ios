//
//  SignupVC.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit
import DropDown

class SignupVC: UIViewController {
    
    // MARK: - Properties
    let datePickerCheckIn = UIDatePicker();
    var genderDropDown = DropDown()
    var nationDropDown = DropDown()
    var citizenshipDropDown = DropDown()
    
    
    // MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var numberTF: UITextField!
    @IBOutlet weak var heightTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var comebackBtn: UIButton!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var checkbox: UIButton!
    
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var agreementLabel: UILabel!
    @IBOutlet weak var termAndConditionLabel: UIButton!
    
    
    //MARK:- Arrays
    var arrayBGImages:[String] = ["bg_01","bg_02","bg_03"]
    
    //MARK:- Variables
    var currentIndex:Int = 0
    var timer: Timer?
    var isBtnCheck:Bool = false
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        comebackBtn.layer.cornerRadius = 5
        comebackBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        comebackBtn.layer.borderWidth = 2
        
        signupBtn.layer.cornerRadius = 5
        signupBtn.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        signupBtn.layer.borderWidth = 2
        
        checkbox.layer.cornerRadius = 5
        checkbox.layer.borderColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
        checkbox.layer.borderWidth = 2
        checkbox.backgroundColor = .clear
        startTimer()
        
        datePickerCheckIn.addTarget(self, action: #selector(self.updateCheckInField), for: UIControl.Event.valueChanged)
        datePickerCheckIn.datePickerMode =  UIDatePicker.Mode.date
        dobTF.inputView = datePickerCheckIn
        
        textfieldsDDFunc()
    }
    
    
    
    
    // MARK: - Set up
    // MARK: - IBActions
    
    @IBAction func signupBtn_Click(_ sender: Any) {
        DoSignup()
        
        //        startAnimating()
    }
    
    @IBAction func comeBackBtn_Click(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func checkBoxBtn_Click(_ sender: Any) {
        if isBtnCheck == false{
            checkbox.setImage(UIImage(named: "tick1"), for: .normal)
            checkbox.tintColor = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
            isBtnCheck = true
        } else {
            checkbox.setImage(nil, for: .normal)
            isBtnCheck = false
        }
    }
    
    
    // MARK: - Navigation
    
    
    // MARK: - Helpers
    
    func DoSignup()  {
        
        if nameTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Full Name is Empty", Sender: self)
            return
        }
        
        if passwordTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Password is Empty", Sender: self)
            return
        }
        
        if confirmPasswordTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Confirm Password is Empty", Sender: self)
            return
        }
        if confirmPasswordTF.text != passwordTF.text {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Password not Matched", Sender: self)
            return
        }
        
        if emailTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Email is Empty", Sender: self)
            return
        }
        
        if dobTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Date of Birth is Empty", Sender: self)
            return
        }
        
        if genderTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Gender is Empty", Sender: self)
            return
        }
        
        if numberTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Phone Number is Empty", Sender: self)
            return
        }
        if heightTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Height is Empty", Sender: self)
            return
        }
        if weightTF.text == "" {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "Weight is Empty", Sender: self)
            return
        }
        
        if isBtnCheck == false {
            AlertHelper.showErrorAlert(WithTitle: "Error", Message: "You must agree to terms and conditions", Sender: self)
            return
        }
        
        let url = "http://172.104.217.178/fitmy/public/api/register"
        var header = [String:String]()
        header["Accept"] = "application/json"
        var body = [String:Any]()
        body["full_name"] = nameTF.text!
        body["email"] = emailTF.text!
        body["phone"] = numberTF.text!
        body["gender"] = genderTF.text!
        body["nation"] = ""
        body["citizenship"] = ""
        body["password"] = passwordTF.text!
        body["password_confirmation"] = confirmPasswordTF.text!
        body["height"] = heightTF.text!
        body["weight"] = weightTF.text!
        body["address"] = dobTF.text!
        body["user_type"] = "1"
        body["office_tel_no"] = ""
        //office_fax_no
        body["country"] = ""
        body["state"] = ""
        body["city"] = ""
        body["zip_code"] = ""
        body["office_fax_no"] = ""
        AFWrapper.requestPOSTURL(url,
                                 params: body as [String : AnyObject],
                                 headers: header,
                                 success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.dictionaryValue
                                        if(data["status"] == 200)
                                        {
                                            AlertHelper.showSuccessAlert(WithTitle: "Success", Message: data["message"]?.stringValue ?? "You Registered Successfully!", Sender: self)
//                                            self.navigationController?.popViewController(animated: true)
                                        }
                                        else
                                        {
                                            AlertHelper.showErrorAlert(WithTitle: "Error", Message: data["message"]?.stringValue ?? "Registration Failed!", Sender: self)
                                        }
                                        
                                        
                                    }
                                    
        },
                                 failure: { (error) in
                                    print(error)
                                    AlertHelper.showErrorAlert(WithTitle: "Error", Message: error.localizedDescription, Sender: self)
                                    return
                                    
        })
    }
    
    @objc func updateCheckInField(sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy" //format style. Browse online to get a format that fits your needs.
        self.dobTF.text = dateFormatter.string(from: sender.date)
        //        self.datePickerCheckOut.minimumDate = sender.date
    }
    
    // MARK: - Network Manager calls
    // MARK: - Extensions
    
    
    //MARK:- Functions
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    @objc func timerAction(){
        let desiredScrollPosition = (currentIndex < arrayBGImages.count - 1) ? currentIndex + 1 : 0
        collectionView.scrollToItem(at: IndexPath(item: desiredScrollPosition, section: 0), at: .centeredHorizontally, animated: true)
    }
    
}
extension SignupVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayBGImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imagesCVCell", for: indexPath) as! imagesCVCell
        cell.image = UIImage(named: arrayBGImages[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentIndex = Int(scrollView.contentOffset.x / collectionView.frame.size.width)
        print(currentIndex)
    }
}


extension SignupVC{
    
    func textfieldsDDFunc(){
        let tapState = UITapGestureRecognizer(target: self, action: #selector(self.selectGender(_:)))
        genderTF.addGestureRecognizer(tapState)
        genderDropDown.anchorView = genderTF
        genderDropDown.dataSource = ["Male","Female"]
    }
    @objc func selectGender(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        genderDropDown.show()
        genderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            //print("Selected item: \(item) at index: \(index)")
            self.genderTF.text = item
        }
    }
}

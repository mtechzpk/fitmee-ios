//
//  imagesCVCell.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class imagesCVCell: UICollectionViewCell {
    
    //MARK:- Outlets
    @IBOutlet weak var bgImages: UIImageView!
    
    //MARK:- Variables
    var image:UIImage!{
        didSet{
            bgImages.image = image
        }
    }
}

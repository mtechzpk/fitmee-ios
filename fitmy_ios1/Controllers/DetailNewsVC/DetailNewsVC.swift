//
//  DetailNewsVC.swift
//  Fit MY
//
//  Created by apple on 3/19/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class DetailNewsVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var frontImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var decriptionLabel: UILabel!
    
    //MARK:- Variables
    var newsModel:NewsModel?
    
    //MARK:- Arrays
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if newsModel != nil{
            titleLabel.text = newsModel?.title
            decriptionLabel.text = newsModel?.news_description
            self.frontImage.kf.setImage(with: URL(string:newsModel!.filename))
        } else {
            print("Empty")
        }
        
    }
    
    //MARK:- Actions
    @IBAction func backBtnClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

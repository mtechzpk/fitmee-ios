//
//  LoginOrSignupVC.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class LoginOrSignupVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var signupBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    //MARK:- Arrays
    var arrayBGImages:[String] = ["bg_01","bg_02","bg_03","bg_04", "bg_04"]
    var arrayTopImages:[String] = ["ic_fma_logo", "introduction_page2_01", "introduction_page3_01", "introduction_page4_01", "introduction_page5_01"]
    var arrayTopText:[String] = ["WELCOME TO", "LIVE HEALTHIER", "TRACK YOUR EXERCISE", "COLLECT AND REDEEM REWARDS", "BOOK FACILITIES"]
    var arrayBottomText:[String] = ["FIT MALAYSIA APP","Performing exercise such as walking, running and cycling for healthy body and mind","Keep track of your daily activities easily and accurately. Improve your lifestyle by reaching your daily and weekly goals","Collect FitPoint and redeem exciting gifts and vouchers","Book Sports facilities using your Fit Points"]
    var arrayBottomImages:[String] = ["ic_kbs_logo_01"]
    
    //MARK:- Variables
    var currentIndex:Int = 0
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        signupBtn.layer.cornerRadius = 5
        loginBtn.layer.cornerRadius = 5
        pageControl.numberOfPages = arrayBGImages.count
        //startTimer()
    }
    //MARK:- Actions
    @IBAction func signupBtnClicked(_ sender: UIButton) {
        let storyboard1 = UIStoryboard(name: "Main", bundle: nil)
        let vc:SignupVC = storyboard1.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoginScreen", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginVC")
        self.navigationController?.pushViewController(initialViewController, animated: true)
    }
    
    //MARK:- Functions
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    @objc func timerAction(){
        let desiredScrollPosition = (currentIndex < arrayBGImages.count - 1) ? currentIndex + 1 : 0
        collectionView.scrollToItem(at: IndexPath(item: desiredScrollPosition, section: 0), at: .centeredHorizontally, animated: true)
    }
}

extension LoginOrSignupVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayBGImages.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoginOrSignupCVCell", for: indexPath) as! LoginOrSignupCVCell
        cell.topImages.image = UIImage(named: arrayTopImages[indexPath.row])
        cell.topText.text = arrayTopText[indexPath.row]
        cell.bottomText.text = arrayBottomText[indexPath.row]
        cell.image = UIImage(named: arrayBGImages[indexPath.row])
        cell.bottomImage.isHidden = indexPath.row == 0 ? false : true
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        currentIndex = Int(scrollView.contentOffset.x / collectionView.frame.size.width)
        pageControl.currentPage = currentIndex
    }
}

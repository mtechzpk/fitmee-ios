//
//  LoginOrSignupCVCell.swift
//  Fit MY
//
//  Created by apple on 3/5/20.
//  Copyright © 2020 Fit Malaysia. All rights reserved.
//

import UIKit

class LoginOrSignupCVCell: UICollectionViewCell {
    //MARK:- Outlets
    @IBOutlet weak var bgImages: UIImageView!
    @IBOutlet weak var topImages: UIImageView!
    @IBOutlet weak var topText: UILabel!
    @IBOutlet weak var bottomText: UILabel!
    @IBOutlet weak var bottomImage: UIImageView!
    
    //MARK:- Variables
    var image:UIImage!{
        didSet{
            bgImages.image = image
        }
    }
}

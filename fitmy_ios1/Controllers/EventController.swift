//
//  EventController.swift
//  Fit MY
//
//  Created by Azlan Shah on 25/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import ZKCarousel

struct Event {
    var  id =  String()
    var  name =  String()
    var  cover_image =  String()
    var  event_datetime =  String()
    var  venue =  String()
    var  about_desc =  String()
    var  about_image =  String()
    var  reward_desc =  String()
    var  reward_image =  String()
    var  eo_id =  String()
    var  eo_name =  String()
}

class EventController: UIViewController {

    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    @IBOutlet weak var searchBtn: UIButton!
    
    func localizeLabel() {
        searchField.placeholder = "Search Program..".localized()
        searchBtn.setTitle("Search".localized(), for: .normal)
    }
    
    var carousel = ZKCarousel()
    var category = [Category]()
    var events = [Event]()
    var currentEvent = [Event]()
    var cat1 = [Event]()
    var cat2 = [Event]()
    var cat3 = [Event]()
    var history = [Event]()
    
    var currentCategory = 6
    
    
    var image1 = UIImageView()
    var image2 = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tableView.register(cellType: EventCell.self)
        tableView.backgroundColor = .clear
        
        category.append(Category(img: #imageLiteral(resourceName: "measuring-tape"), btn: 0, lbl: "ESport".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "man-sprinting"), btn: 1, lbl: "Fun Run".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "sneaker"), btn: 2, lbl: "Futsal".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "search"), btn: 3, lbl: "Badminton".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "man-climbing-stairs"), btn: 4, lbl: "Takraw".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "swimming-figure"), btn: 5, lbl: "Swimming".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "sea-knot"), btn: 6, lbl: "Others".localized()))
        category.append(Category(img: #imageLiteral(resourceName: "calendar"), btn: 7, lbl: "History".localized()))
        
        
        carousel = ZKCarousel(frame: sliderView.frame)
        
        // Create as many slides as you'd like to show in the carousel
        // Add the slides to the carousel
        let slide = ZKCarouselSlide(image: #imageLiteral(resourceName: "subaru"), title: "", description: "")
        let slide1 = ZKCarouselSlide(image: #imageLiteral(resourceName: "baju"), title: "", description: "")
        carousel.slides = [slide, slide1]
        carousel.backgroundColor = .black
        sliderView.addSubview(carousel)
        carousel.interval = 3.0
        carousel.start()
        
        sliderView.layoutIfNeeded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        localizeLabel()
        getAllEvent()
        tableView.reloadSections([0], with: .none)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func doSearch(_ sender: Any) {
        let search = searchField!.text!
        currentEvent = events.filter{$0.name.lowercased().contains(search.lowercased())}
        if search != "" {
            tableView.reloadData()
        } else {
            currentEvent = events
            tableView.reloadData()
        }
        print(currentEvent)
    }
    
    func updateBanner() {
        var slides = [ZKCarouselSlide]()
        for event in currentEvent {
            let img = UIImageView()
            img.kf.setImage(with: URL(string: event.cover_image), options: [.transition(.fade(0.3))])
            {
                result in
                switch result {
                case .success(let value):
                    print("Task done for: \(value.source.url?.absoluteString ?? "")")
                case .failure(let error):
                    print("Job failed: \(error.localizedDescription)")
                    if Bool.random() {
                        img.image = #imageLiteral(resourceName: "subaru")
                    } else {
                        img.image = #imageLiteral(resourceName: "baju")
                    }
                }
            }
            if img.image != nil {
                slides.append(ZKCarouselSlide(image: img.image!, title: "", description: ""))
            }
        }
        carousel.slides = slides
    }
    
    func selectCategory(information: Any?) {
        print("select category: ", information!)
        let tag = information as! Int
        switch tag {
        case 0:
            currentEvent =  events.filter{$0.name.lowercased() == "sport"}
            currentCategory = 0
        case 1:
            currentEvent =  events.filter{$0.name.lowercased() == "run"}
            currentCategory = 1
        case 2:
            currentEvent = events
            currentCategory = 2
        case 3:
            currentEvent = events
            currentCategory = 3
        case 4:
            currentEvent = events
            currentCategory = 4
        case 5:
            currentEvent = events
            currentCategory = 5
        case 6:
            currentEvent = events
            currentCategory = 6
        case 7:
            getAllEventHistory()
            currentCategory = 7
        default:
            currentEvent = events
            currentCategory = 6
        }
        
        tableView.reloadData()
    }
    
    func allReward() {
        selectCategory(information: 6)
    }
    
    func historyReward() {
        selectCategory(information: 7)
    }
    
    func nearbyReward() {
        selectCategory(information: 6)
    }
}

extension EventController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return EventCategoryView.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame = CGRect(
            x: 0,
            y: 0,
            width: tableView.bounds.size.width,
            height: self.tableView(tableView, heightForHeaderInSection: section)
        )
        let view = EventCategoryView(frame: frame) as! EventCategoryView
        view.fill(categories: category)
        view.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.9490196078, blue: 0.9647058824, alpha: 1)
        view.events.listenTo(eventName: "category", action: selectCategory)
        view.events.listenTo(eventName: "allReward", action: allReward)
        view.events.listenTo(eventName: "historyReward", action: historyReward)
        view.events.listenTo(eventName: "nearbyReward", action: nearbyReward)
        view.updateBG(tag: currentCategory)
        view.allRewardBtn.setTitle("All Events", for: .normal)
        view.allRewardBtn.setTitleColor(#colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.2352941176, alpha: 1), for: .normal)
        view.allRewardBtn.setTitleColor(#colorLiteral(red: 0.4705882353, green: 0.3764705882, blue: 0.9098039216, alpha: 1), for: .selected)
        view.historyBtn.setTitleColor(#colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.2352941176, alpha: 1), for: .normal)
        view.historyBtn.setTitleColor(#colorLiteral(red: 0.4705882353, green: 0.3764705882, blue: 0.9098039216, alpha: 1), for: .selected)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentEvent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as EventCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.fill(title: currentEvent[indexPath.row].name, detail: currentEvent[indexPath.row].about_desc, price: currentEvent[indexPath.row].eo_name, image: currentEvent[indexPath.row].cover_image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Event_Detail") as? EventDetailController {
            viewController.thisTitle = currentEvent[indexPath.row].name
            viewController.thisDetail = currentEvent[indexPath.row].about_desc
            viewController.thisImg = currentEvent[indexPath.row].cover_image
            viewController.thisLocation = currentEvent[indexPath.row].venue
            viewController.thisRewardImg = currentEvent[indexPath.row].reward_image
            viewController.thisID = currentEvent[indexPath.row].id
            
            present(viewController, animated: true, completion: nil)
        }
    }
}


extension EventController {
    func getAllEvent() {
        let url = "https://api.fitmyapp.asia/api/event/inventory/get_events"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            self.events.removeAll()
                                            for (index, value) in data.enumerated() {
                                                self.events.append(Event(id: value["id"].stringValue, name: value["name"].stringValue,
                                                                         cover_image: value["cover_image"].stringValue,
                                                                         event_datetime: value["event_datetime"].stringValue,
                                                                         venue: value["venue"].stringValue,
                                                                         about_desc: value["about_desc"].stringValue,
                                                                         about_image: value["about_image"].stringValue,
                                                                         reward_desc: value["reward_desc"].stringValue,
                                                                         reward_image: value["reward_image"].stringValue,
                                                                         eo_id: value["eo_id"].stringValue,
                                                                         eo_name: value["eo_name"].stringValue))
                                                self.currentEvent = self.events
                                            }
                                            
                                            self.tableView.reloadData()
                                            self.updateBanner()
                                            print(data)
                                        } else {
                                            print("Invalid response")
                                        }
                                    }
        },
                                failure: { (error) in
                                    print(error)
                                    
        })
        
    }
    
    func getAllEventHistory() {
        let url = "https://api.fitmyapp.asia/api/event/interest/get_interest"
        var header = [String:String]()
        var body = [String:String]()
        
        if let token = UserDefaults.standard.value(forKey: "api_token") {
            header["Authorization"] = "Bearer \(token)"
        }
        
        AFWrapper.requestGETURL(url,
                                params: body as [String : AnyObject],
                                headers: header,
                                success: { (JSON) in
                                    DispatchQueue.main.async {
                                        let data = JSON.arrayValue
                                        if data.count > 0 {
                                            self.history.removeAll()
                                            for (index, value) in data.enumerated() {
                                                self.history.append(Event(id: value["id"].stringValue, name: value["name"].stringValue,
                                                                         cover_image: value["cover_image"].stringValue,
                                                                         event_datetime: value["event_datetime"].stringValue,
                                                                         venue: value["venue"].stringValue,
                                                                         about_desc: value["about_desc"].stringValue,
                                                                         about_image: value["about_image"].stringValue,
                                                                         reward_desc: value["reward_desc"].stringValue,
                                                                         reward_image: value["reward_image"].stringValue,
                                                                         eo_id: value["eo_id"].stringValue,
                                                                         eo_name: value["eo_name"].stringValue))
                                                self.currentEvent = self.history
                                            }
                                            
                                            self.tableView.reloadData()
//                                            self.updateBanner()
                                            print(data)
                                        } else {
                                            print("No history:", data)
                                            self.currentEvent = self.history
                                            self.tableView.reloadData()
                                        }
                                    }
        },
                                failure: { (error) in
                                    print(error)
                                    
        })
        
    }
}


extension EventController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1 { //search textfield
            currentEvent = events
            if textField.text!.count > 0 {
                doSearch(UIButton())
            } else {
                currentEvent = events
                tableView.reloadData()
            }
        }
    }
}

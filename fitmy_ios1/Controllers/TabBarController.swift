//
//  TabBarController.swift
//  Fit MY
//
//  Created by Azlan Shah on 13/09/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import SwipeableTabBarController
import UIKit

class TabBarController: SwipeableTabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let viewControllers = viewControllers {
            selectedViewController = viewControllers[0]
            
        }
        /// Set the animation type for swipe
        swipeAnimatedTransitioning?.animationType = SwipeAnimationType.sideBySide
        
        /// Set the animation type for tap
//        tapAnimatedTransitioning?.animationType = SwipeAnimationType.push
        
        /// if you want cycling switch tab, set true 'isCyclingEnabled'
        isCyclingEnabled = false
        
        /// Disable custom transition on tap.
        //tapAnimatedTransitioning = nil
        
        /// Set swipe to only work when strictly horizontal.
        //        diagonalSwipeEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let items = self.tabBar.items {
            items[0].title = "Home".localized()
            items[1].title = "Fitness".localized()
            items[2].title = "Reward".localized()
            items[3].title = "Event".localized()
            items[4].title = "Booking".localized()
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        // Handle didSelect viewController method here
    }
}

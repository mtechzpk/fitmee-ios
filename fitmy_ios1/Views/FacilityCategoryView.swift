//
//  CategoryView.swift
//  Fit MY
//
//  Created by Azlan Shah on 23/10/2019.
//  Copyright © 2019 Fit Malaysia. All rights reserved.
//

import UIKit
import Reusable


class FacilityCategoryView: UIView, NibOwnerLoadable {
    
    static let height: CGFloat = 173
    @IBOutlet weak var categoryCell: UIView!
    @IBOutlet weak var categoryStackView: UIStackView!
    @IBOutlet weak var allRewardBtn: UIButton!
    @IBOutlet weak var historyBtn: UIButton!
    @IBOutlet weak var nearbyBtn: UIButton!
    
    
    let events = EventManager()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.loadNibContent()
        categoryStackView.layoutIfNeeded()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        categoryStackView.layoutIfNeeded()
    }
    @IBAction func getAllReward(_ sender: Any) {
        print("Get all reward")
        events.trigger(eventName: "allReward")
    }
    
    @IBAction func getHistory(_ sender: Any) {
        print("Get reward history")
        events.trigger(eventName: "historyReward")
    }
    
    @IBAction func getNearby(_ sender: Any) {
        print("Get nearby reward")
        events.trigger(eventName: "nearbyReward")
    }
    
    @objc func selectCategory(sender: UIButton) {
        print("select category: ", sender.tag)
        events.trigger(eventName: "category", information: sender.tag)
    }
    
    func updateBG(tag: Int) {
        let btn = categoryStackView.subviews[tag] as! UIViewX
        btn.backgroundColor = #colorLiteral(red: 1, green: 0.9478865266, blue: 0.9467069507, alpha: 1)
    }
    
    func fill(categories:[Category]) {
        categoryStackView.layoutIfNeeded()
        let cell = categoryCell.copyView()
        for subview in categoryStackView.subviews {
            subview.removeFromSuperview()
        }
        print(categoryStackView.subviews.count)
        for (index, category) in categories.enumerated() {
            let cell2 = cell.copyView() as! UIViewX
            let img = cell2.subviews[0] as! UIImageView
            let lbl = cell2.subviews[1] as! UILabel
            let btn = cell2.subviews[2] as! UIButton
            btn.tag = index
            btn.addTarget(self, action: #selector(selectCategory), for: .touchDown)
            img.image = category.img
            lbl.text = category.lbl
            cell2.cornerRadius = 8
            cell2.shadowColor = .black
            cell2.shadowRadius = 4
            cell2.shadowOpacity = 0.1
            cell2.shadowOffsetY = 1
            categoryStackView.addArrangedSubview(cell2)
            print(categoryStackView.subviews.count)
        }
        categoryStackView.layoutIfNeeded()
    }
    
}
